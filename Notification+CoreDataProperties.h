//
//  Notification+CoreDataProperties.h
//  
//
//  Created by Sonia Mane on 06/02/17.
//
//  This file was automatically generated and should not be edited.
//

#import "Notification+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Notification (CoreDataProperties)

+ (NSFetchRequest<Notification *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *dateTime;
@property (nullable, nonatomic, copy) NSString *message;
@property (nonatomic) BOOL readStatus;
@property (nullable, nonatomic, copy) NSString *notificationkey;

@end

NS_ASSUME_NONNULL_END
