//
//  AviationPressIAPHelper.m
//  AviationPress
//
//  Created by Sonia Mane on 20/04/17.
//  Copyright © 2017 Aptara. All rights reserved.
//

#import "AviationPressIAPHelper.h"
#import "APConstants.h"
#import <QuartzCore/QuartzCore.h>
#import "BookshelfDatasource.h"

@implementation AviationPressIAPHelper

+ (AviationPressIAPHelper *)sharedInstance {
    static dispatch_once_t once;
    static AviationPressIAPHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      IAP_PRODUCT_EEPP_12,
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}

//Auto Login
+(void)saveAutoLoginOnRememberMePressed:(BOOL)shouldSave
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    [mpUserDefault setBool:shouldSave forKey:@"autoLoginUD"];
    [mpUserDefault synchronize];
}

+(BOOL)checkAutoLogin
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    return [mpUserDefault boolForKey:@"autoLoginUD"];
}
+(NSString *)getJWTToken
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    return [mpUserDefault stringForKey:@"jwtToken"];
    
}
+(void)setJWTToken:(NSString *)token
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    [mpUserDefault setObject:token forKey:@"jwtToken"];
    [mpUserDefault synchronize];
}
//Book Purchased
+(BOOL)checkIfBookPurchased:(Book *)bookObj
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    NSString *val =  [mpUserDefault stringForKey:[NSString stringWithFormat:@"bookPurchase-%@-%@", bookObj.bookEditionNo, bookObj.bookId.stringValue]];
    if (val != nil)
    {
        if (val.length > 0)
        {
            return YES;
        }
    }
    
    return NO;
    
}

+(void)setBookPurchased:(NSString *)valueSTR :(Book *)bookObj
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    [mpUserDefault setValue:valueSTR forKey:[NSString stringWithFormat:@"bookPurchase-%@-%@", bookObj.bookEditionNo, valueSTR]];
    [mpUserDefault synchronize];
}
+(void)removeBookPurchased:(Book *)bookObj
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    [mpUserDefault removeObjectForKey:[NSString stringWithFormat:@"bookPurchase-%@-%@", bookObj.bookDesc, [AviationPressIAPHelper getBookIDFromBookName:bookObj.bookName]]];
    [mpUserDefault synchronize];
}

+(void)saveBookIDWithBookName:(NSString *)bookName andBookID:(NSString *)bookID
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    [mpUserDefault setValue:bookID forKey:[NSString stringWithFormat:@"%@", bookName]];
    [mpUserDefault synchronize];
}
+(NSString *)getBookIDFromBookName:(NSString *)bookName
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    return [mpUserDefault stringForKey:bookName];
}
//Cover Page, Passphrase, pList and SALT
+(NSString *)getCoverPage:(Book *)bookObj
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    return [mpUserDefault stringForKey:[NSString stringWithFormat:@"coverPage-%@", bookObj.bookDesc != nil ? bookObj.bookDesc : bookObj.bookEditionNumber]];
}

+(void)setCoverPage:(NSString *)valueSTR :(Book *)bookObj
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    [mpUserDefault setValue:valueSTR forKey:[NSString stringWithFormat:@"coverPage-%@", bookObj.bookEditionNumber]];
    [mpUserDefault synchronize];
}

+(NSString *)getpassPhrase:(Book *)bookObj
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    return [mpUserDefault stringForKey:[NSString stringWithFormat:@"passphrase-%@", bookObj.bookDesc]];
    
}
+(void)setpassPhrase:(NSString *)valueSTR :(Book *)bookObj
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    [mpUserDefault setValue:valueSTR forKey:[NSString stringWithFormat:@"passphrase-%@", bookObj.bookEditionNo]];
    [mpUserDefault synchronize];
}

+(NSDictionary *)getpListPathDictionary:(NSString  *)bookEditionNumber
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    return [NSDictionary dictionaryWithContentsOfFile:[mpUserDefault stringForKey:[NSString stringWithFormat:@"plistpath-%@", bookEditionNumber]]];
}

+(NSString *)getpListPath:(Book *)bookObj
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    return [mpUserDefault stringForKey:[NSString stringWithFormat:@"plistpath-%@", bookObj.bookDesc]];
}

+(void)setpListPath:(NSString *)valueSTR :(Book *)bookObj
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    [mpUserDefault setValue:valueSTR forKey:[NSString stringWithFormat:@"plistpath-%@", bookObj.bookEditionNo]];
    [mpUserDefault synchronize];
}


+(NSString *)getSalt:(Book *)bookObj
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    return [mpUserDefault stringForKey:[NSString stringWithFormat:@"salt-%@", bookObj.bookDesc]];
    
}
+(void)setSalt:(NSString *)valueSTR :(Book *)bookObj
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    [mpUserDefault setValue:valueSTR forKey:[NSString stringWithFormat:@"salt-%@", bookObj.bookEditionNo]];
    [mpUserDefault synchronize];
}


+(NSString *)getUSERID
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    return [mpUserDefault stringForKey:@"email"];
    
}
+(void)setUSERID:(NSString *)userID
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    [mpUserDefault setObject:userID forKey:@"email"];
    [mpUserDefault synchronize];
}

+(NSDictionary *)getUserDetails
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    return [mpUserDefault dictionaryForKey:@"userdetails"];
    
}
+(void)setUserDetails:(NSDictionary *)userDetails
{
    NSUserDefaults *mpUserDefault = [NSUserDefaults standardUserDefaults];
    [mpUserDefault setObject:userDetails forKey:@"userdetails"];
    [mpUserDefault synchronize];
}

+ (void) addLineToView:(UIView *)view atPosition:(LINE_POSITION)position withColor:(UIColor *)color lineWitdh:(CGFloat)width {
    // Add line
    UIView *lineView = [[UIView alloc] init];
    [lineView setBackgroundColor:color];
    [lineView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [view addSubview:lineView];
    
    NSDictionary *metrics = @{@"width" : [NSNumber numberWithFloat:width]};
    NSDictionary *views = @{@"lineView" : lineView};
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[lineView]|" options: 0 metrics:metrics views:views]];
    
    switch (position) {
        case LINE_POSITION_TOP:
            [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[lineView(width)]" options: 0 metrics:metrics views:views]];
            break;
            
        case LINE_POSITION_BOTTOM:
            [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lineView(width)]|" options: 0 metrics:metrics views:views]];
            break;
        default: break;
    }
}
+(void)giveBorderToView:(UIView *)mView withColor:(UIColor *)color andBorderWidth:(CGFloat )width
{
    mView.layer.borderColor = color.CGColor;
    mView.layer.borderWidth = width;
}

+(void)makeViewRound:(UIView *)mView roundVal:(CGFloat )width
{
    mView.layer.cornerRadius = width;
    mView.layer.masksToBounds = true;
}

+(void)addImageViewOnTextFielf:(UITextField *)mTextField withColor:(UIImage *)image isRight:(BOOL)isRight
{
    UIImageView *imgforLeft=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)]; // Set frame as per space required around icon
    [imgforLeft setImage:image];
    
    [imgforLeft setContentMode:UIViewContentModeCenter];// Set content mode centre or fit
    
    if (isRight == YES)
    {
        mTextField.rightView=imgforLeft;
        mTextField.rightViewMode=UITextFieldViewModeAlways;
    }else
    {
        mTextField.leftView=imgforLeft;
        mTextField.leftViewMode=UITextFieldViewModeAlways;
    }
}

/*
 UIImageView *imgforLeft=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)]; // Set frame as per space required around icon
 [imgforLeft setImage:[UIImage imageNamed:@"yourImagename.png"]];
 
 [imgforLeft setContentMode:UIViewContentModeCenter];// Set content mode centre or fit
 
 self. yourTextfield.leftView=imgforLeft;
 self. yourTextfield.leftViewMode=UITextFieldViewModeAlways;
 
 */
+ (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

+(void)showAlertOnVC:(UIViewController *)target withTitleText:(NSString *)titleText andWithSubTitle:(NSString *)subTitleText withCompletion:(void (^)(BOOL success))block
{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:titleText
                                 message:subTitleText
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * _Nonnull action)
    {
                                   block(YES);
                               }];
    
    
    //Add your buttons to alert controller
    
    [alert addAction:okButton];
    
    [target presentViewController:alert animated:YES completion:nil];
}

+(void)showAlertOnVC:(UIViewController *)target withTitleText:(NSString *)titleText andWithSubTitle:(NSString *)subTitleText
{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:titleText
                                 message:subTitleText
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:nil];
    
    
    //Add your buttons to alert controller
    
    [alert addAction:okButton];
    
    [target presentViewController:alert animated:YES completion:nil];
}
//+(FeSpinnerTenDot *)standardLoaderConfig:(UIView *)targetView
//{
//    FeSpinnerTenDot *spinnerTenDot = [[FeSpinnerTenDot alloc] initWithView:targetView withBlur:YES];
//    spinnerTenDot.titleLabelText = @"LOADING";
//    spinnerTenDot.fontTitleLabel = [UIFont fontWithName:@"Neou-Thin" size:22];
//
//    return  spinnerTenDot;
//}

+(void)showLoaderWithText:(NSString *)loaderString
{
    [SVProgressHUD showWithStatus:loaderString];
}
+(void)hideLoaderText
{
    [SVProgressHUD dismiss];
}
+(void)performPostonTarget:(UIViewController *)target forAPI:(NSString *)apiName withParameter:(NSMutableDictionary *)params withCompletion:(void (^)(id responseObject, long statusCode))block loaderText:(NSString *)loaderString
{
    NSString *finalURL = [NSString stringWithFormat:@"%@%@", BASE_SERVER, apiName];
    NSLog(@"Final URL is - %@", finalURL);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    //Loader
    //Authorization
    NSString *headerValue = [NSString stringWithFormat:@"Bearer %@" , [AviationPressIAPHelper getJWTToken]];
    [manager.requestSerializer  setValue:headerValue forHTTPHeaderField:@"Authorization"];

    //Loader
//    if (![loaderString isEqualToString:@""])
//    {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [SVProgressHUD showWithStatus:loaderString];
//        });
//    }
    [manager POST:finalURL parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
//         if (![loaderString isEqualToString:@""])
//         {
//             dispatch_async(dispatch_get_main_queue(), ^{
//                 [SVProgressHUD dismiss];
//             });
//         }
         
         NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
         NSInteger statusCode = [response statusCode];
         NSLog(@"RESPONSE CODE: %li", (long)statusCode);

         NSLog(@"JSON: %@", responseObject);
         block(responseObject, statusCode);
     } failure:^(NSURLSessionTask *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         NSHTTPURLResponse *response = (NSHTTPURLResponse *) [operation response];
         NSInteger statusCode = [response statusCode];
         NSLog(@"RESPONSE CODE: %li", (long)statusCode);

//         if (![loaderString isEqualToString:@""])
//         {
//             dispatch_async(dispatch_get_main_queue(), ^{
//                 [SVProgressHUD dismiss];
//             });
//         }
         block(error, statusCode);
     }];
}

+(void)performGetOnTarget:(UIViewController *)target forAPI:(NSString *)apiName withParameter:(NSMutableDictionary *)params withCompletion:(void (^)(id responseObject))block loaderText:(NSString *)loaderString
{
    NSString *finalURL = [NSString stringWithFormat:@"%@%@", BASE_SERVER, apiName];
    NSLog(@"Final URL is - %@", finalURL);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/x-www-form-urlencoded", @"application/json", @"text/json", nil];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];

    //Authorization
    NSString *headerValue = [NSString stringWithFormat:@"Bearer %@" , [AviationPressIAPHelper getJWTToken]];
    [manager.requestSerializer  setValue:headerValue forHTTPHeaderField:@"Authorization"];
    //Loader
//    if (![loaderString isEqualToString:@""])
//    {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [SVProgressHUD showWithStatus:loaderString];
//        });
//    }

    [manager GET:finalURL parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
//         if (![loaderString isEqualToString:@""])
//         {
//             dispatch_async(dispatch_get_main_queue(), ^{
//                 [SVProgressHUD dismiss];
//             });
//         }
         NSLog(@"JSON: %@", responseObject);
         block(responseObject);
     } failure:^(NSURLSessionTask *operation, NSError *error)
    {
//        if (![loaderString isEqualToString:@""])
//        {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [SVProgressHUD dismiss];
//            });
//        }
         NSLog(@"Error: %@", error);
         block(error);
     }];
}
+(void)performGetOnTargetWithDirectURL:(UIViewController *)target forAPI:(NSString *)apiName withParameter:(NSString *)urlSTRAppend withCompletion:(void (^)(id responseObject))block loaderText:(NSString *)loaderString
{
    NSString *finalURL = [NSString stringWithFormat:@"%@%@%@", BASE_SERVER, apiName, urlSTRAppend];
    NSLog(@"Final URL is - %@", finalURL);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

    NSString *headerValue = [NSString stringWithFormat:@"Bearer %@" , [AviationPressIAPHelper getJWTToken]];
    [manager.requestSerializer  setValue:headerValue forHTTPHeaderField:@"Authorization"];
    //Loader
//    if (![loaderString isEqualToString:@""])
//    {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [SVProgressHUD showWithStatus:loaderString];
//        });
//    }

    [manager GET:finalURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
//         if (![loaderString isEqualToString:@""])
//         {
//             dispatch_async(dispatch_get_main_queue(), ^{
//                 [SVProgressHUD dismiss];
//             });
//         }
         NSLog(@"JSON: %@", responseObject);
         block(responseObject);
     } failure:^(NSURLSessionTask *operation, NSError *error) {
//         if (![loaderString isEqualToString:@""])
//         {
//             dispatch_async(dispatch_get_main_queue(), ^{
//                 [SVProgressHUD dismiss];
//             });
//         }
         
         NSLog(@"Error: %@", error);
         block(error);
     }];
}

+(void)downloadFileAndSaveItToDocDir:(NSString *)urlSTR  withCompletion:(void (^)(NSURL * savedAtULR))block loaderText:(NSString *)loaderString
{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:urlSTR];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    //Loader
//    if (![loaderString isEqualToString:@""])
//    {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [SVProgressHUD showWithStatus:loaderString];
//        });
//    }

    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response)
    {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error)
    {
        NSLog(@"File downloaded to: %@", filePath);
        dispatch_async(dispatch_get_main_queue(), ^{
//            if (![loaderString isEqualToString:@""])
//            {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [SVProgressHUD dismiss];
//                });
//            }
        });
        if (error)
        {
            block(nil);
        }else
        {
            block(filePath);
        }
    }];
    [downloadTask resume];
}

#pragma mark - Restore Book
+(void)downloadBook:(Book *)bookObj withTarget:(UIViewController *)target withCompletion:(void (^)(id responseObject))block loaderText:(NSString *)loaderString
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];
    [paramDic setValue:bookObj.bookId.stringValue forKey:@"bookId"];
    [paramDic setValue:[AviationPressIAPHelper getUSERID] forKey:@"UserID"];
    //Loader
//    if (![loaderString isEqualToString:@""])
//    {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [SVProgressHUD showWithStatus:loaderString];
//        });
//    }
    
    [AviationPressIAPHelper performGetOnTarget:target forAPI:DOWNLOADBOOK withParameter:paramDic withCompletion:^(id responseObject)
     {
         if (![responseObject isKindOfClass:[NSError class]]) // Response
         {
             if([responseObject isKindOfClass:[NSDictionary class]])
             {
                 NSDictionary *allDic = (NSDictionary *)responseObject;
                 if ([allDic.allKeys containsObject:@"Message"])
                 {
                     NSString *message = [allDic valueForKey:@"Message"];
                     if ([message isEqualToString:@"File not found"])
                     {
                         block(nil);
                     }
                 }
                 
                 NSString *bookURL = [allDic valueForKey:@"BookURL"];
                 NSString *passphrase = [allDic valueForKey:@"PassPhrase"];
                 NSString *salt = [allDic valueForKey:@"Salt"];
                 NSString *plist = [allDic valueForKey:@"Plist"];
                 NSString *plistXMLFilePath = [allDic valueForKey:@"PlistXMLFilePath"];
                 
                 
                 if ([plist isEqualToString:@"EEPP_13"])
                 {
                     bookObj.pListName = EEPP_13_PLIST;
                     bookObj.bookDesc = EEPP_13_PLIST;
                     bookObj.coverImageName = @"EEPP_13";
                 }else if ([plist isEqualToString:@"EEPP_12"])
                 {
                     bookObj.pListName = EEPP_12_PLIST;
                     bookObj.bookDesc = EEPP_12_PLIST;
                     bookObj.coverImageName = @"EEPP_12";
                 }else if ([plist isEqualToString:@"EEPP_11"])
                 {
                     bookObj.pListName = EEPP_11_PLIST;
                     bookObj.bookDesc = EEPP_11_PLIST;
                     bookObj.coverImageName = @"EEPP_11";
                 }
                 bookObj.passphrase = passphrase;
                 bookObj.salt = salt;
                 
                 [AviationPressIAPHelper setpassPhrase:passphrase :bookObj];
                 [AviationPressIAPHelper setSalt:salt :bookObj];
                 
                 //Download Code
                 [AviationPressIAPHelper downloadFileAndSaveItToDocDir:bookObj.coverPageURL.absoluteString withCompletion:^(NSURL *savedAtULR)
                  {
                      [AviationPressIAPHelper setCoverPage:savedAtULR.absoluteString :bookObj]; //Saving the Cover Page
                      
                      [AviationPressIAPHelper downloadFileAndSaveItToDocDir:plistXMLFilePath withCompletion:^(NSURL *savedAtULR)
                       {
                           //First Save document directory path to user defaults
                           [AviationPressIAPHelper setpListPath:savedAtULR.absoluteString :bookObj];
                           
                           [AviationPressIAPHelper downloadFileAndSaveItToDocDir:[NSString stringWithFormat:@"%@", bookURL] withCompletion:^(NSURL *savedAtULR)
                            {
                                
                                if (savedAtULR != nil)
                                {
                                    bookObj.documentsURL = savedAtULR;
                                    [[BookshelfDatasource sharedInstance] saveProperBookToCoreData:bookObj];[AviationPressIAPHelper setBookPurchased:bookObj.bookId.stringValue :bookObj];
                                        block(bookObj);
                                }else
                                {
                                    block(nil);
                                }
                            } loaderText:@""];
                       } loaderText:@""]; //Saving the pList File
                  } loaderText:@""];
             }
             
         }else //Error
         {
                      block(nil);
         }
         
     } loaderText:@""];
}

#pragma mark - Core Data and Books
//+(void)CheckAndAddBookToCoreData
//{
//    if (![[NSUserDefaults standardUserDefaults] boolForKey:FIRST_LAUNCH]) {
//        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:FIRST_LAUNCH];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        [AviationPressIAPHelper addInitialBookAndCategory:[Book new]];
//    } else {
//        // check if already added
//        //        if (![[NSUserDefaults standardUserDefaults] boolForKey:CORE_DATA_CRASH_FIX]) {
//        NSError *error;
//        NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
//        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"AviationBook"];
//        NSArray *aviationBooks = [[managedObjectContext executeFetchRequest:fetchRequest error:&error] mutableCopy];
//        
//        if (aviationBooks.count > 0) {
//            // do nothings
//            // Has book - now assign it to only All Editions
//            [AviationPressIAPHelper assignBookToAllEditions:[aviationBooks lastObject]];
//        } else {
//            // else now add for core data crash fix
//            [self addInitialBookAndCategory];
//        }
//    }
//}
//
////Assign To All Addition
//+ (void) assignBookToAllEditions:(AviationBook *) aviationBook {
//    NSError *error;
//    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
//    
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Edition"];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name contains[cd] %@", @"All Editions"];
//    [fetchRequest setPredicate:predicate];
//    NSArray *allEditions = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
//    
//    if (allEditions.count > 0) {
//        // do nothings
//        /// update hasBOOK
//        for (Edition *edi in allEditions) {
//            [edi addHasBook:[NSSet setWithObjects:aviationBook, nil]];
//        }
//        if (![self.managedObjectContext save:&error]) {
//            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
//        }
//    } else {
//        Edition *edi1 = [NSEntityDescription insertNewObjectForEntityForName:@"Edition" inManagedObjectContext:self.managedObjectContext];
//        edi1.name = @"All Editions";
//        edi1.selected = YES;
//        [edi1 addHasBook:[NSSet setWithObjects:aviationBook, nil]];
//        
//        if (![self.managedObjectContext save:&error]) {
//            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
//        }
//    }
//}
//
//+ (void) addInitialBookAndCategory:(Book *)bookObj
//{
//    NSError *error;
//    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
//    
//    //Book
//    NSURL *const encryptedPDFURL = [[[NSBundle bundleForClass:[self class]] resourceURL] URLByAppendingPathComponent:EEPP_11_AES];
//    NSURL *documentSamplesURL = PSCCopyFileURLToDocumentFolderAndOverride(encryptedPDFURL, NO);
//    
//    AviationBook *bookObject = [NSEntityDescription insertNewObjectForEntityForName:@"AviationBook"
//                                                             inManagedObjectContext:self.managedObjectContext];
//    [bookObject setValue:EEPP_11_BOOK_NAME forKey:@"bookName"];
//    [bookObject setValue:[NSNumber numberWithBool:NO] forKey:@"isBookSynced"];
//    [bookObject setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
//    [bookObject setValue:documentSamplesURL.lastPathComponent forKey:@"bookDocumentsURL"];
//    [bookObject setValue:EEPP_11_EDITION_NAME forKey:@"bookEditionNumber"];
//    
//    NSURL *coverPageURL = [[NSBundle bundleForClass:[self class]] URLForResource:EEPP_11_COVER_IMAGE_NAME withExtension:@"png"];
//    [bookObject setValue:coverPageURL.lastPathComponent forKey:@"bookCoverImageName"];
//    
//    NSLog(@"SONIAS CHECK  *** 1st Launch encryptedPDFURL - %@", documentSamplesURL);
//    NSLog(@"SONIAS CHECK  *** 1st Launch coverPageURL - %@", coverPageURL);
//    
//    
//    if (![self.managedObjectContext save:&error]) {
//        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
//    }
//    
//#warning EDITION
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Edition"];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name contains[cd] %@", @"All Editions"];
//    [fetchRequest setPredicate:predicate];
//    NSArray *allEditions = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
//    
//    if (allEditions.count > 0) {
//        // do nothings
//        /// update hasBOOK
//        for (Edition *edi in allEditions) {
//            [edi addHasBook:[NSSet setWithObjects:bookObject, nil]];
//        }
//        if (![self.managedObjectContext save:&error]) {
//            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
//        }
//    } else {
//        Edition *edi1 = [NSEntityDescription insertNewObjectForEntityForName:@"Edition" inManagedObjectContext:self.managedObjectContext];
//        edi1.name = @"All Editions";
//        edi1.selected = YES;
//        [edi1 addHasBook:[NSSet setWithObjects:bookObject, nil]];
//        
//        if (![self.managedObjectContext save:&error]) {
//            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
//        }
//    }
//    
//    
//    NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc] initWithEntityName:@"Edition"];
//    NSArray *allEditions1 = [[managedObjectContext executeFetchRequest:fetchRequest1 error:nil] mutableCopy];
//    
//    for (Edition *ediCD in allEditions1) {
//        NSSet<AviationBook *> *books = ediCD.hasBook;
//        for (AviationBook *bk in books) {
//            NSLog(@"$$$$$$$$$ %@ --- %@", ediCD.name, bk.bookEditionNumber);
//        }
//    }
//    
//    NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:@"AviationBook"];
//    NSArray *allBooks = [managedObjectContext executeFetchRequest:req error:nil];
//    for (AviationBook *bk in allBooks) {
//        NSSet<Edition *> *belongsToEdi = bk.belongsTo;
//        for (Edition *ed in belongsToEdi) {
//            NSLog(@"&&&&&&&& %@ ---- %@", bk.bookName, ed.name);
//        }
//    }
//}

@end
