//
//  IAPHelper.m
//  AviationPress
//
//  Created by Sonia Mane on 02/03/17.
//  Copyright © 2017 Aptara. All rights reserved.
//

#import "IAPHelper.h"
#import "VerificationController.h"
#import "APConstants.h"
#import "BookshelfDatasource.h"

@interface IAPHelper () <SKProductsRequestDelegate, SKPaymentTransactionObserver>
@property (nonatomic, strong) SKProductsRequest * productsRequest;

@end

@implementation IAPHelper {
    RequestProductsCompletionHandler _productRequestCompletionHandler;
    NSSet * _productIdentifiers;
    NSMutableSet * _purchasedProductIdentifiers;
}

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers {
    
    if (self = [super init]) {
        
        // Store product identifiers
        _productIdentifiers = productIdentifiers;
        
        // Check for previously purchased products
        _purchasedProductIdentifiers = [NSMutableSet set];
        for (NSString * productIdentifier in _productIdentifiers) {
            BOOL productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:productIdentifier];
            if (productPurchased) {
                [_purchasedProductIdentifiers addObject:productIdentifier];
                NSLog(@"Previously purchased: %@", productIdentifier);
            } else {
                NSLog(@"Not purchased: %@", productIdentifier);
            }
        }
        // Add self as transaction observer
            [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    }
    return self;
}

- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler {
#warning here
//    self.productsRequest.delegate = nil;
//    [self.productsRequest cancel];
//    self.productsRequest = nil;

    // 1
    _productRequestCompletionHandler = [completionHandler copy];
    
    // 2
    _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:_productIdentifiers];
    _productsRequest.delegate = self;
    [_productsRequest start];
    
}

- (BOOL)productPurchased:(NSString *)productIdentifier {
    return [_purchasedProductIdentifiers containsObject:productIdentifier];
}

- (void)buyProduct:(SKProduct *)product {
    
    NSLog(@"Buying %@...", product.productIdentifier);
    
    SKPayment * payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    
}

#warning here

- (BOOL) canMakePayments {
    return [SKPaymentQueue canMakePayments];
}

- (void) restorePurchases {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

#pragma mark - SKProductsRequestDelegate

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    
    NSLog(@"Loaded list of products...");
    
    NSArray * skProducts = response.products;

    @try
    {
        if (_productRequestCompletionHandler)
        {
            _productRequestCompletionHandler(YES, skProducts);

        }
    } @catch (NSException *exception)
    {
        NSLog(@"%@", exception);
    } @finally
    {
        [self clearRequestAndHandler];
        
        for (SKProduct * skProduct in skProducts) {
            NSLog(@"Found product: %@ %@ %0.2f",
                  skProduct.productIdentifier,
                  skProduct.localizedTitle,
                  skProduct.price.floatValue);
        }

    }
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    NSLog(@"Failed to load list of products with error = %@", error);
    _productRequestCompletionHandler(NO, nil);
    [self clearRequestAndHandler];
}

- (void) clearRequestAndHandler {
    _productsRequest = nil;
    _productRequestCompletionHandler = nil;
}

#pragma mark SKPaymentTransactionOBserver

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction * transaction in transactions) {
        switch (transaction.transactionState) {
                case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
                
                case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
                
                case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                
            default:
                break;
        }
    };
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"completeTransaction...");
    
    [self validateReceiptForTransaction:transaction];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"restoreTransaction...");
    
    [self validateReceiptForTransaction:transaction];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"failedTransaction...");
    if (transaction.error.code != SKErrorPaymentCancelled) {
        NSLog(@"Transaction error: %@", transaction.error.localizedDescription);
    }
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void)restoreCompletedTransactions {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)validateReceiptForTransaction:(SKPaymentTransaction *)transaction {
    [self provideContentForProductIdentifier:transaction.payment.productIdentifier];

//    VerificationController * verifier = [VerificationController sharedInstance];
//    [verifier verifyPurchase:transaction completionHandler:^(BOOL success) {
//        if (success) {
//            NSLog(@"Successfully verified receipt!");
//        } else {
//            NSLog(@"Failed to validate receipt.");
//            [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
//        }
//    }];
}

- (void)provideContentForProductIdentifier:(NSString *)productIdentifier {
    
//    if ([productIdentifier isEqualToString:IAP_PRODUCT_EEPP_12]) {
//        int currentValue = [[NSUserDefaults standardUserDefaults] integerForKey:IAP_PRODUCT_EEPP_12];
//        currentValue += 5;
//        [[NSUserDefaults standardUserDefaults] setInteger:currentValue forKey:IAP_PRODUCT_EEPP_12];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//    } else
    {
        [_purchasedProductIdentifiers addObject:productIdentifier];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:productIdentifier];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotification object:productIdentifier userInfo:nil];
    
    if ([[BookshelfDatasource sharedInstance] save_12th_EditionBookToCoreData]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductSavedToiCloudNCoreDataNotification object:productIdentifier userInfo:nil];
    }   
}
@end
