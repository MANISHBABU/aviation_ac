//
//  PSCFileHelper.m
//  PSPDFCatalog
//
//  Copyright (c) 2012-2016 PSPDFKit GmbH. All rights reserved.
//
//  The PSPDFKit Sample applications are licensed with a modified BSD license.
//  Please see License for details. This notice may not be removed from this file.
//

#import "PSCFileHelper.h"

NSURL *PSCTempFileURLWithPathExtension(NSString *prefix, NSString *pathExtension) {
    if (pathExtension && ![pathExtension hasPrefix:@"."]) pathExtension = [NSString stringWithFormat:@".%@", pathExtension];
    if (!pathExtension) pathExtension = @"";

    NSString *UDIDString = NSUUID.UUID.UUIDString;
    if (prefix) {
        UDIDString = [NSString stringWithFormat:@"_%@", UDIDString];
    }

    NSURL *tempURL = [[NSURL fileURLWithPath:NSTemporaryDirectory()] URLByAppendingPathComponent:[NSString stringWithFormat:@"%@%@%@", prefix, UDIDString, pathExtension] isDirectory:NO];
    return tempURL;
}

NSURL *PSCCopyFileURLToDocumentFolderAndOverride(NSURL *documentURL, BOOL override) {
    NSCAssert([documentURL isKindOfClass:NSURL.class], @"documentURL must be of type NSURL");

    // copy file from the bundle to a location where we can write on it.
    NSString *docsFolder = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *newPath = [docsFolder stringByAppendingPathComponent:(NSString *)documentURL.lastPathComponent];
    NSURL *newURL = [NSURL fileURLWithPath:newPath];
    const BOOL needsCopy = ![NSFileManager.defaultManager fileExistsAtPath:newPath];
    
    if (needsCopy) {
        NSLog(@"---- Needs copy to documents directory");
    } else {
        NSLog(@"---- File already exists in the documents directory");
    }
    
    // Override paarmter always sent as NO
    if (override) {
        [NSFileManager.defaultManager removeItemAtURL:newURL error:NULL];
    }

    NSError *error;
    if ((needsCopy || override) && ![NSFileManager.defaultManager copyItemAtURL:documentURL toURL:newURL error:&error]) {
        NSLog(@"Error while copying %@: %@", documentURL.path, error.localizedDescription);
    } else {
        NSLog(@"FILE COPIED");
    }

    return newURL;
}
