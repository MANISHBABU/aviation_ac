//
//  AviationPressIAPHelper.h
//  AviationPress
//
//  Created by Sonia Mane on 20/04/17.
//  Copyright © 2017 Aptara. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IAPHelper.h"
#import "AFNetworking.h"
#import "Edition+CoreDataClass.h"
#import "AviationBook+CoreDataClass.h"
#import "Book.h"
#import "SVProgressHUD.h"

typedef enum : NSUInteger {
    LINE_POSITION_TOP,
    LINE_POSITION_BOTTOM
} LINE_POSITION;


@interface AviationPressIAPHelper : IAPHelper
+ (AviationPressIAPHelper *)sharedInstance;

//User Defaults
+(void)saveAutoLoginOnRememberMePressed:(BOOL)shouldSave;
+(BOOL)checkAutoLogin;
+(NSString *)getJWTToken;
+(void)setJWTToken:(NSString *)token;
//Book Purchase
+(NSString *)getBookIDFromBookName:(NSString *)bookName;
+(void)saveBookIDWithBookName:(NSString *)bookName andBookID:(NSString *)bookID;
+(void)setBookPurchased:(NSString *)valueSTR :(Book *)bookObj;
+(BOOL)checkIfBookPurchased:(Book *)bookObj;
+(void)removeBookPurchased:(Book *)bookObj;
//Passphrase
+(void)setCoverPage:(NSString *)valueSTR :(Book *)bookObj;
+(NSString *)getCoverPage:(Book *)bookObj;
+(void)setSalt:(NSString *)valueSTR :(Book *)bookObj;
+(NSString *)getSalt:(Book *)bookObj;
+(void)setpListPath:(NSString *)valueSTR :(Book *)bookObj;
+(NSDictionary *)getpListPathDictionary:(NSString  *)bookEditionNumber;
+(NSString *)getpListPath:(Book *)bookObj;
+(void)setpassPhrase:(NSString *)valueSTR :(Book *)bookObj;
+(NSString *)getpassPhrase:(Book *)bookObj;
//Salt Closed
+(void)setUSERID:(NSString *)userID;
+(NSString *)getUSERID;
+(NSDictionary *)getUserDetails;
+(void)setUserDetails:(NSDictionary *)userDetails;


+ (void) addLineToView:(UIView *)view atPosition:(LINE_POSITION)position withColor:(UIColor *)color lineWitdh:(CGFloat)width;
+(void)giveBorderToView:(UIView *)mView withColor:(UIColor *)color andBorderWidth:(CGFloat )width;
+(void)makeViewRound:(UIView *)mView roundVal:(CGFloat )width;
+(void)addImageViewOnTextFielf:(UITextField *)mTextField withColor:(UIImage *)image isRight:(BOOL)isRight;
+(BOOL) validateEmail: (NSString *) candidate;
+(void)showAlertOnVC:(UIViewController *)target withTitleText:(NSString *)titleText andWithSubTitle:(NSString *)subTitleText withCompletion:(void (^)(BOOL success))block;
+(void)showAlertOnVC:(UIViewController *)target withTitleText:(NSString *)titleText andWithSubTitle:(NSString *)subTitleText;
+(void)performPostonTarget:(UIViewController *)target forAPI:(NSString *)apiName withParameter:(NSMutableDictionary *)params withCompletion:(void (^)(id responseObject, long statusCode))block loaderText:(NSString *)loaderString;
+(void)performGetOnTarget:(UIViewController *)target forAPI:(NSString *)apiName withParameter:(NSMutableDictionary *)params withCompletion:(void (^)(id responseObject))block loaderText:(NSString *)loaderString;
+(void)performGetOnTargetWithDirectURL:(UIViewController *)target forAPI:(NSString *)apiName withParameter:(NSString *)urlSTRAppend withCompletion:(void (^)(id responseObject))block loaderText:(NSString *)loaderString;
+(void)downloadFileAndSaveItToDocDir:(NSString *)urlSTR  withCompletion:(void (^)(NSURL * savedAtULR))block loaderText:(NSString *)loaderString;
+(void)downloadBook:(Book *)bookObj withTarget:(UIViewController *)target withCompletion:(void (^)(id responseObject))block loaderText:(NSString *)loaderString;
+(void)hideLoaderText;
+(void)showLoaderWithText:(NSString *)loaderString;
@end
