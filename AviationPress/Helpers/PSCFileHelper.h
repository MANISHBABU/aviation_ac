//
//  PSCFileHelper.h
//  PSPDFCatalog
//
//  Copyright (c) 2012-2016 PSPDFKit GmbH. All rights reserved.
//
//  The PSPDFKit Sample applications are licensed with a modified BSD license.
//  Please see License for details. This notice may not be removed from this file.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// Creates a temp URL.
FOUNDATION_EXTERN NSURL *PSCTempFileURLWithPathExtension(NSString *prefix, NSString *pathExtension);

/// Copies a file to the documents directory.
FOUNDATION_EXTERN NSURL *PSCCopyFileURLToDocumentFolderAndOverride(NSURL *documentURL, BOOL override);

NS_ASSUME_NONNULL_END
