//
//  APConstants.m
//  AviationPress
//
//  Created by Sonia Mane on 06/12/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import "APConstants.h"

@implementation APConstants
// First launch
NSString *const CORE_DATA_CRASH_FIX = @"CoreDataCrashFix";
NSString *const FIRST_LAUNCH = @"HasLaunchedOnce";
NSString *const FIRST_LAUNCH_VERSION_3 = @"HasLaunchedOncev3";
NSString *const FIRST_LAUNCH_BACKUP_ALERT = @"DidShowBackUpAlert";

// Notification
NSString *const NOTIFICATION_PUSH_RECEIVED = @"NOTIFICATION_PUSH_RECEIVED";
NSString *const NOTIF_HANDLE_FOREGROUND_PUSH = @"NOTIF_HANDLE_FOREGROUND_PUSH";
NSString *const NOTIF_DID_TAP_ON_NOTIFICATION = @"NOTIF_DID_TAP_ON_NOTIFICATION";
NSString *const NOTIF_SAVED_TO_DB = @"NOTIF_SAVED_TO_DB";

// CLoud Column names
NSString *const CLOUD_RECORD_TYPE = @"EEPP_Editions";
NSString *const CLOUD_RECORD_EDITION_DATE = @"EditionDate";
NSString *const CLOUD_RECORD_EDITION_TIME_INTERVAL = @"EditionTimeInterval";
NSString *const CLOUD_RECORD_EDITION_PDF = @"EditionPDF";
NSString *const CLOUD_RECORD_EDITION_COVER_IMAGE = @"EditionCoverImage";
NSString *const CLOUD_RECORD_EDITION_NUMBER = @"EditionNumber";
NSString *const CLOUD_RECORD_EDITION_NAME = @"EditionName";
NSString *const CLOUD_RECORD_PDF_AES_NAME = @"PDF_AES_Name";
NSString *const ALL_EDITIONS_SELECTED = @"AllEditionsSelected";

// Notification URLs
NSString *const SAVE_TOKEN_PROD = @"http://notifications.aviation-press.com/index.php/Tokenizier/savetoken?token=";
NSString *const RESET_MSG_PROD = @"http://notifications.aviation-press.com/index.php/Tokenizier/resetmsgtoken?token=";
NSString *const GET_UNREAD_MSG_PROD = @"http://notifications.aviation-press.com/index.php/Tokenizier/getunreadmesage?token=";

//NSString *const SAVE_TOKEN_PROD = @"http://pun-devctsl.aptaracorp.com/pushnotification//index.php/Tokenizier/savetoken?token=";
//NSString *const RESET_MSG_PROD = @"http://pun-devctsl.aptaracorp.com/pushnotification/index.php/Tokenizier/resetmsgtoken?token=";
//NSString *const GET_UNREAD_MSG_PROD = @"http://pun-devctsl.aptaracorp.com/pushnotification/index.php/Tokenizier/getunreadmesage?token=";

NSString *const SAVE_TOKEN_DEV = @"http://pun-devctsl.aptaracorp.com/pushnotification_dev//index.php/Tokenizier/savetoken?token=";
NSString *const RESET_MSG_DEV = @"http://pun-devctsl.aptaracorp.com/pushnotification_dev/index.php/Tokenizier/resetmsgtoken?token=";
NSString *const GET_UNREAD_MSG_DEV = @"http://pun-devctsl.aptaracorp.com/pushnotification_dev/index.php/Tokenizier/getunreadmesage?token=";


// IAP product name
NSString *const IAP_PRODUCT_EEPP_12 = @"EEPP_12";
NSString *const IAPHelperProductPurchasedNotification = @"IAPHelperProductPurchasedNotification";
NSString *const IAPHelperProductSavedToiCloudNCoreDataNotification = @"IAPHelperProductSavedToiCloudNCoreDataNotification";

// PDF AES names, salt, passphrase
// 11th Edition
NSString *const EEPP_11_AES = @"every_11.pdf.aes";
NSString *const EEPP_11_EDITION_NAME = @"11th Edition";
NSString *const EEPP_11_BOOK_NAME = @"Everything Explained for the Professional Pilot";
NSString *const EEPP_11_COVER_IMAGE_NAME = @"EEPP";
NSString *const EEPP_11_PLIST = @"EEPP_11";
NSString *const EEPP_11_PASSPHRASE = @"aBfMjDTJVjUf3FApA6gtim0e61LeSGWV9sTxBdsfadsHGFHGbasjdjaDSNKCUHGYGghgsguGDHDiuHSDIUHASchsid";
NSString *const EEPP_11_SALT = @"öhapvuenröaoeruhföaeiruaerubYTFYTFVbjncJKcnijsdhcuHcIUTUYGDBHJsaSUHAUIJBvodsijvireiowrewBUYEuuasdas";

// 12th Edition Samle
NSString *const EEPP_12_SAMPLE_AES = @"Everything_Explained_for_the_Professional_Pilot_12th_Edition_Sample.aes";
NSString *const EEPP_12_SAMPLE_EDITION_NAME = @"12th Edition Sample";
NSString *const EEPP_12_SAMPLE_BOOK_NAME = @"Everything Explained for the Professional Pilot";
NSString *const EEPP_12_SAMPLE_COVER_IMAGE_NAME = @"EEPP_12_Sample";
NSString *const EEPP_12_SAMPLE_PLIST = @"EEPP_12_Sample";
NSString *const EEPP_12_SAMPLE_PASSPHRASE = @"xBdsfadsHGFHDaBfMjDTJVjUf3FApASchsidbasjdjaDSNKCUHGYGghgsAHGguGD6gtim0e61LeSGWV9sTiuHSDIUH";
NSString *const EEPP_12_SAMPLE_SALT = @"aAUIJUHöhapvuenröaoeruhföijvireiowrewBUYEuuasdaseiruaerubYTFYTFVBvodsDBHJsaSbjncJKcnijsdhcuHcIUTUYG";

// 12th Edition
NSString *const EEPP_12_AES = @"Everything_Explained_for_the_Professional_Pilot_12th_Edition.aes";
NSString *const EEPP_12_EDITION_NAME = @"12th Edition";
NSString *const EEPP_12_BOOK_NAME = @"Everything Explained for the Professional Pilot";
NSString *const EEPP_12_COVER_IMAGE_NAME = @"EEPP_12";
NSString *const EEPP_12_PLIST = @"EEPP_12";
NSString *const EEPP_12_PASSPHRASE = @"djaDSNKidaBfMjDTGbasjJCUHGYGgdsfadsHGFHVjUf3FApA6gtim0e61LhgsguGDHDiuHSDIUHASchseSGWV9sTxB";
NSString *const EEPP_12_SALT = @"vireiowreuuenröaYDJKcnijTFYTFVbjncHcIUTUYGBvodsijsdhcuBHJsaSUHAUIJwBUYEuoeruhföaeiruaerubasdasöhapv";

// 13th Edition
NSString *const EEPP_13_AES = @"Everything_Explained_for_the_Professional_Pilot_13th_Edition.aes";
NSString *const EEPP_13_EDITION_NAME = @"13th Edition";
NSString *const EEPP_13_BOOK_NAME = @"Everything Explained for the Professional Pilot";
NSString *const EEPP_13_COVER_IMAGE_NAME = @"EEPP_13";
NSString *const EEPP_13_PLIST = @"EEPP_13";
NSString *const EEPP_13_PASSPHRASE = @"6PAD5pQXEhaem6VfmUyn7F9796DfZQCtwPEmujbRUh8Ljh5SVFn4ynrYPCTj4NCQqz2qVPKhMPF7u286atgHY2pchS";//@"djaDSNKidaBfMjDTGbasjJCUHGYGgdsfadsHGFHVjUf3FApA6gtim0e61LhgsguGDHDiuHSDIUHASchseSGWV9sTxB";

NSString *const EEPP_13_SALT = @"6PAD5pQXEhaem6VfmUyn7F9796DfZQCtwPEmujbRUh8Ljh5SVFn4ynrYPCTj4NCQqz2qVPKhMPF7u286atgHY2pchS";// @"vireiowreuuenröaYDJKcnijTFYTFVbjncHcIUTUYGBvodsijsdhcuBHJsaSUHAUIJwBUYEuoeruhföaeiruaerubasdasöhapv";//


//API's
NSString *const BASE_SERVER             = @"http://18.217.204.87:8001/API/";
//@"http://18.217.204.87/API/"; // Old
NSString *const REGISTERAPI             = @"user/RegisterUser";
NSString *const LOGINAPI                = @"user/UserLogin";
NSString *const RESETPASSWORD           = @"user/ResetPassword";
NSString *const ALLBOOKSFROMSTORE       = @"BookDetails/GetbookDetail";
NSString *const BOOKFROMBOOKID          = @"BookDetails/GetBookDetailsById";
NSString *const ADDPURCHASEDBOOK        = @"BookDetails/UserBookDetails";
NSString *const PURCHASEINFO            = @"User/UpdatePurchaseInfo";
NSString *const ALLBOOKSFORUSER         = @"BookDetails/UserBookDetails";
NSString *const COUPONAPPLY             = @"ConsumeCoupon/validateCoupon";
NSString *const DOWNLOADBOOK            = @"BookDetails/DownloadBook";
NSString *const PURCHASEBOOK            = @"BookDetails/InsertUserBook";


@end
