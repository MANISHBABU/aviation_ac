//
//  APConstants.h
//  AviationPress
//
//  Created by Sonia Mane on 06/12/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APConstants : NSObject
extern NSString *const CORE_DATA_CRASH_FIX;
extern NSString *const FIRST_LAUNCH;
extern NSString *const FIRST_LAUNCH_VERSION_3;
extern NSString *const FIRST_LAUNCH_BACKUP_ALERT;
extern NSString *const CLOUD_RECORD_TYPE;
extern NSString *const CLOUD_RECORD_EDITION_TIME_INTERVAL;
extern NSString *const CLOUD_RECORD_EDITION_DATE;
extern NSString *const CLOUD_RECORD_EDITION_PDF;
extern NSString *const CLOUD_RECORD_EDITION_COVER_IMAGE;
extern NSString *const CLOUD_RECORD_EDITION_NUMBER;
extern NSString *const CLOUD_RECORD_EDITION_NAME;
extern NSString *const CLOUD_RECORD_PDF_AES_NAME;
extern NSString *const ALL_EDITIONS_SELECTED;
extern NSString *const NOTIFICATION_PUSH_RECEIVED;
extern NSString *const NOTIF_HANDLE_FOREGROUND_PUSH;
extern NSString *const NOTIF_DID_TAP_ON_NOTIFICATION;
extern NSString *const NOTIF_SAVED_TO_DB;

extern NSString *const SAVE_TOKEN_PROD;
extern NSString *const RESET_MSG_PROD;
extern NSString *const GET_UNREAD_MSG_PROD;

extern NSString *const SAVE_TOKEN_DEV;
extern NSString *const RESET_MSG_DEV;
extern NSString *const GET_UNREAD_MSG_DEV;

// IAP product name
extern NSString *const IAP_PRODUCT_EEPP_12;
extern NSString *const IAPHelperProductPurchasedNotification;
extern NSString *const IAPHelperProductSavedToiCloudNCoreDataNotification;

extern NSString *const EEPP_11_AES;
extern NSString *const EEPP_11_EDITION_NAME;
extern NSString *const EEPP_11_BOOK_NAME;
extern NSString *const EEPP_11_COVER_IMAGE_NAME;
extern NSString *const EEPP_11_PLIST;
extern NSString *const EEPP_11_PASSPHRASE;
extern NSString *const EEPP_11_SALT;

extern NSString *const EEPP_12_SAMPLE_AES;
extern NSString *const EEPP_12_SAMPLE_EDITION_NAME;
extern NSString *const EEPP_12_SAMPLE_BOOK_NAME;
extern NSString *const EEPP_12_SAMPLE_COVER_IMAGE_NAME;
extern NSString *const EEPP_12_SAMPLE_PLIST;
extern NSString *const EEPP_12_SAMPLE_PASSPHRASE;
extern NSString *const EEPP_12_SAMPLE_SALT;

extern NSString *const EEPP_12_AES;
extern NSString *const EEPP_12_EDITION_NAME;
extern NSString *const EEPP_12_BOOK_NAME;
extern NSString *const EEPP_12_COVER_IMAGE_NAME;
extern NSString *const EEPP_12_PLIST;
extern NSString *const EEPP_12_PASSPHRASE;
extern NSString *const EEPP_12_SALT;

extern NSString *const EEPP_13_AES;
extern NSString *const EEPP_13_EDITION_NAME;
extern NSString *const EEPP_13_BOOK_NAME;
extern NSString *const EEPP_13_COVER_IMAGE_NAME;
extern NSString *const EEPP_13_PLIST;
extern NSString *const EEPP_13_PASSPHRASE;
extern NSString *const EEPP_13_SALT;

//API's
extern NSString *const BASE_SERVER;
extern NSString *const REGISTERAPI;
extern NSString *const LOGINAPI;
extern NSString *const RESETPASSWORD;
extern NSString *const ALLBOOKSFROMSTORE;
extern NSString *const BOOKFROMBOOKID;
extern NSString *const ADDPURCHASEDBOOK;
extern NSString *const PURCHASEINFO;
extern NSString *const ALLBOOKSFORUSER;
extern NSString *const COUPONAPPLY;
extern NSString *const DOWNLOADBOOK;
extern NSString *const PURCHASEBOOK;

@end
