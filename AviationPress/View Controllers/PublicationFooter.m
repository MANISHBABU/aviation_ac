

//
//  PublicationFooter.m
//  AviationPress
//
//  Created by Sonia Mane on 03/11/16.
//  Copyright © 2016 Sonia Mane. All rights reserved.
//

#import "PublicationFooter.h"

@implementation PublicationFooter

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
