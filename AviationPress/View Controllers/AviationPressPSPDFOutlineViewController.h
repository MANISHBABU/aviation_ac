//
//  AviationPressPSPDFOutlineViewController.h
//  AviationPress
//
//  Created by Sonia Mane on 03/05/17.
//  Copyright © 2017 Aptara. All rights reserved.
//

#import <PSPDFKit/PSPDFKit.h>

@interface AviationPressPSPDFOutlineViewController : PSPDFOutlineViewController

@end
