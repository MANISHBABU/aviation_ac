//
//  ViewController.m
//  AviationPress
//
//  Created by Sonia Mane on 29/08/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import "ViewController.h"
#import "AviationPressLandingViewController.h"
#import "PSCAESCryptoDataProviderExample.h"

@interface ViewController ()

@end

@implementation ViewController

- (instancetype)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        
    }
    return self;
}

- (nullable UIViewController *)currentViewController {
    return self;
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.navigationItem.hidesBackButton = YES;
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.hidesBackButton = YES;
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    PSCAESCryptoDataProviderExample *example = [[PSCAESCryptoDataProviderExample alloc] init];
    AviationPressLandingViewController *controller = (AviationPressLandingViewController *)[example invokeWithDelegate:self];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
