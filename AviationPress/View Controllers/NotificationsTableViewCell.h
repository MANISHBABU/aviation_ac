//
//  NotificationsTableViewCell.h
//  AviationPress
//
//  Created by Sonia Mane on 24/01/17.
//  Copyright © 2017 Aptara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *notifIcon;
@property (weak, nonatomic) IBOutlet UILabel *notificationMessage;
@property (weak, nonatomic) IBOutlet UILabel *notificationTimeStamp;
@end
