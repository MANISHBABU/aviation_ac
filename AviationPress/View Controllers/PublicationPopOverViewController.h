//
//  PublicationPopOverViewController.h
//  AviationPress
//
//  Created by Sonia Mane on 25/11/16.
//  Copyright © 2016 Sonia Mane. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol PublicationPopOverControllerDelegate

-(void)categoryBooks:(NSString *)input;
-(void)moveBooks:(NSString *)input;
- (void)selectedAllEdition:(BOOL) isAllEdition;

@end

@interface PublicationPopOverViewController : UIViewController

@property (nonatomic, weak, readwrite) id <PublicationPopOverControllerDelegate> delegate;

@property (nonatomic) BOOL isNormal;

- (void)dismissMe;

@end
