//
//  PublicationCell.m
//  AviationPress
//
//  Created by Sonia Mane on 03/11/16.
//  Copyright © 2016 Sonia Mane. All rights reserved.
//

#import "PublicationCell.h"

@implementation PublicationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    // The user can only edit the text field when in editing mode.
    [super setEditing:editing animated:animated];
    //self.publicationTextField.enabled = editing;
}

@end
