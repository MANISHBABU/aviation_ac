//
//  PublicationFooter.h
//  AviationPress
//
//  Created by Sonia Mane on 03/11/16.
//  Copyright © 2016 Sonia Mane. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PublicationFooter : UITableViewCell

@property (strong, nonatomic) IBOutlet UIBarButtonItem *addRowBarButtonItem;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *editBarButtonItem;

@end
