//
//  BookshelfViewController.m
//  AviationPress
//
//  Created by Sonia Mane on 24/11/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import "BookshelfViewController.h"
#import "BookshelfDatasource.h"
#import "SearchViewController.h"
#import "PublicationPopOverViewController.h"
#import "BookShelfGridLayout.h"
#import "BookShelfListLayout.h"
#import "Book.h"
#import "BookShelfCell.h"
#import "BookShelfTableCell.h"
#import "AFHTTPSessionManager.h"
#import "AviationPressLandingViewController.h"
#import <CloudKit/CloudKit.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "APConstants.h"
#import "Reachability.h"
#import "AviationBook+CoreDataClass.h"
#import "APCircularLabel.h"
#import "NotificationsViewController.h"
#import "NotificationManager.h"
#import "DataUpdater.h"
#import "Edition+CoreDataClass.h"
#import "IAPHelper.h"
#import "InAppPurchaseViewController.h"
#import "FAQViewController.h"

#define AP_FONT_S [UIFont fontWithName:@"Helvetica-Bold" size:12]

@interface BookshelfViewController () <UIPopoverPresentationControllerDelegate, SearchViewControllerDelegate, PublicationPopOverControllerDelegate, UITextFieldDelegate>
{
    UIBarButtonItem *_editBarButton;
    UIBarButtonItem *_cancleBarButton;
    UIBarButtonItem *_trashBarButton;
    UIBarButtonItem *_addBarButton;
    UIBarButtonItem *allEditionsBarButton;
    UIBarButtonItem *searchBarButton;
    UIBarButtonItem *switchViewBarButton;
    UIBarButtonItem *iCloudBarButton;
    UIBarButtonItem *purchaseButton, *faqsButton, *purchaseInfoButton;
    
    UIBarButtonItem *moveBarButton ,*deleteBarButton, * doneBarButton,*selectAllBarButton, *notificationBarButton;
    UILabel *notificationsCountLabel;
    
    BOOL _editMode;
    PublicationPopOverViewController *publicationcontroller;
    SearchViewController *searchcontroller;
    InAppPurchaseViewController *inAppController;
    
    UIDatePicker *datePicker;
    UITextField *purchaseDate, *purchaseAmount;
}
@property (nonatomic) Reachability *hostReachability;
@property (nonatomic) Reachability *internetReachability;

@property (nonatomic) BOOL isDeletionModeActive;
@property (nonatomic) BOOL isGrid;
@property (nonatomic) BOOL isSelectAll;

@property (strong, nonatomic) NSMutableArray *fetchedBooks;
@property (strong, nonatomic) NSMutableArray *finalBooks;

@property (nonatomic, strong) BookShelfGridLayout *gridLayout;
@property (nonatomic, strong) BookShelfListLayout *listLayout;

@property (nonatomic, strong) UIImageView *movingCell;
@property (nonatomic, strong) UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic, strong) NSArray *unreadNotifications;
@end

@implementation BookshelfViewController

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"BookShelf";
    [self configureReachability];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enteredForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
    

    [self configureTableViewAndCollectionView];
    [self initBarButtons];
    [self switchToNormalMode];
    [self categoryBooks:@"All Editions"];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    self.navigationController.navigationBar.tintColor = nil;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
//    [self.navigationController.navigationBar setShadowImage:nil];
    self.unreadNotifications = [[NotificationManager sharedInstance] getUnreadNotifications];
    [self updateUnreadNotificationsCount];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navigateToNotificationsViewController:) name:NOTIF_DID_TAP_ON_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSavedNotifCount:) name:NOTIF_SAVED_TO_DB object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(purchased_N_Saved_12thEdition:) name:IAPHelperProductSavedToiCloudNCoreDataNotification object:nil];
    [self initializeBookshelfDataWithAlert:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIF_DID_TAP_ON_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIF_SAVED_TO_DB object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:IAPHelperProductSavedToiCloudNCoreDataNotification object:nil];
}

#pragma mark - InApp Purchase bought

- (void)purchased_N_Saved_12thEdition:(NSNotification *)notification {
    NSString * productIdentifier = notification.object;
    if ([productIdentifier isEqualToString:IAP_PRODUCT_EEPP_12]) {
        [self refreshBookShelfData];
    }
}

- (void)configureReachability {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    //Change the host name here to change the server you want to monitor.
    NSString *remoteHostName = @"www.apple.com";
    self.hostReachability = [Reachability reachabilityWithHostName:remoteHostName];
    [self.hostReachability startNotifier];
    [self updateInterfaceWithReachability:self.hostReachability];
    
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    [self updateInterfaceWithReachability:self.internetReachability];
}

- (void)enteredForeground {
    NSLog(@"Entered Foreground ---------");
    [self refreshBookShelfData];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
}

- (void)refreshSavedNotifCount:(NSNotification *)nsnotification {
    NSLog(@"Notification received = %@",nsnotification);
    [self updateUnreadNotificationsCount];
}

- (void)navigateToNotificationsViewController:(NSNotification *)nsnotification {
    [self notificationButtonTapped:nil];
}

- (void)refreshBookShelfData {
    [self proceedWithOfflineRendering];
    
    /*[[CKContainer defaultContainer] accountStatusWithCompletionHandler:^(CKAccountStatus accountStatus, NSError *error) {
        if (accountStatus == CKAccountStatusNoAccount) {
            [self proceedWithOfflineRendering];
        } else if (accountStatus == CKAccountStatusAvailable) {
            [self proceedWithOnlineRendering];
        }
    }];*/
}

- (void)configureTableViewAndCollectionView {
    self.tableView.hidden = YES;
    self.collectionView.hidden = NO;
    
    self.gridLayout = [[BookShelfGridLayout alloc] init];
    [self.collectionView setCollectionViewLayout:self.gridLayout animated:YES];
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGestureRecognized:)];
    [self.collectionView addGestureRecognizer:longPress];
    self.isGrid = YES;
    self.isSelectAll = NO;
}

- (void)initializeBookshelfDataWithAlert:(BOOL) syncAlert
{
//    [SVProgressHUD showWithStatus:@"Syncing with iCloud..." maskType:SVProgressHUDMaskTypeClear];
    [[CKContainer defaultContainer] accountStatusWithCompletionHandler:^(CKAccountStatus accountStatus, NSError *error) {
        if (accountStatus == CKAccountStatusNoAccount) {
            // 1. First launch - NO ACCOUNT
            if (![[NSUserDefaults standardUserDefaults] boolForKey:FIRST_LAUNCH_BACKUP_ALERT]) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:FIRST_LAUNCH_BACKUP_ALERT];
                [[NSUserDefaults standardUserDefaults] synchronize];
                /*UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Sign in to iCloud"
                                                                               message:@"We recommend storing a copy of this book in your personal iCloud Storage so that it can be restored if deleted. On the Home screen, launch Settings, tap iCloud, and enter your Apple ID. Turn on iCloud Drive. The book will backup automatically. If you don't have an iCloud account, tap Create a new Apple ID. Return to the app and press Sync."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * _Nonnull action) {
                                                            if ([action.title isEqualToString:@"OK"]) {
                                                                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                            }
                                                        }]];
                [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    if ([action.title isEqualToString:@"Cancel"]) {
                        [self proceedWithOfflineRendering];
                    }
                }]];
                
                [self presentViewController:alert animated:YES completion:^{
                    [SVProgressHUD dismiss];
                }]; */
                
                [SVProgressHUD dismiss];
                [self proceedWithOfflineRendering];
            }
            // 2. Further launches - NO ACCOUNT
            else {
                // 3. Further launches - NO ACCOUNT - SYNC Button clicked
                if (syncAlert) {
                    /*UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Sign in to iCloud"
                                                                                   message:@"Sign in to iCloud to restore the last saved copy of this book. On the Home screen, launch Settings, tap iCloud, and enter your Apple ID. Turn on iCloud Drive. Return to the app and press Sync."
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * _Nonnull action) {
                                                                if ([action.title isEqualToString:@"OK"]) {
                                                                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                                }
                                                            }]];
                     */
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Sign in to iCloud"
                                                                                   message:@"Want to backup your bookmarks and annotations? On your home screen, go to Settings, tap iCloud, and enter your Apple ID. Select iCloud Drive ON. Return to the app and press Sync."
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * _Nonnull action) {
                                                                if ([action.title isEqualToString:@"OK"]) {
                                                                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                                }
                                                            }]];
                    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        if ([action.title isEqualToString:@"Cancel"]) {
                            [self proceedWithOfflineRendering];
                        }
                    }]];
                    
                    [self presentViewController:alert animated:YES completion:^{
                        [SVProgressHUD dismiss];
                    }];
                }
                // 4. Further launches - NO ACCOUNT - Default local copy
                else {
                    [self proceedWithOfflineRendering];
                }
            }
        } else if (accountStatus == CKAccountStatusAvailable) {
            // 5. FIRST launch - ACCOUNT Available - iCloud copy
            if (![[NSUserDefaults standardUserDefaults] boolForKey:FIRST_LAUNCH_BACKUP_ALERT]) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:FIRST_LAUNCH_BACKUP_ALERT];
                [[NSUserDefaults standardUserDefaults] synchronize];
                /*UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Store your book to iCloud"
                                                                               message:@"We recommend leaving iCloud Drive turned on so that a copy of this book can be automatically saved, which can be restored if deleted."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * _Nonnull action) {
                                                            [self proceedWithOnlineRendering];
                                                        }]];
                [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    if ([action.title isEqualToString:@"Cancel"]) {
                        [self proceedWithOfflineRendering];
                    }
                }]];
                
                [self presentViewController:alert animated:YES completion:^{
                    [SVProgressHUD dismiss];
                }];*/
                
                [SVProgressHUD dismiss];
                [self proceedWithOfflineRendering];
                
            } else {
                // 6. Further launches - ACCOUNT Available - Default local copy
                if (syncAlert) {
                    if ([self.hostReachability currentReachabilityStatus] == ReachableViaWiFi || [self.hostReachability currentReachabilityStatus] == ReachableViaWWAN) {
                        [self proceedWithOnlineRendering];
                    } else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [SVProgressHUD showWithStatus:@"Check your internet connection and try again. Loading local copy..." maskType:SVProgressHUDMaskTypeClear];
                        });
                        
                        [[BookshelfDatasource sharedInstance] fetchEditionsFromLocalStorageWithCompletionBlock:^(NSArray *editions, NSError *error) {
                            
                            self.finalBooks = [editions mutableCopy];
                            self.fetchedBooks = self.finalBooks.mutableCopy;
                            for (Book *ab in self.fetchedBooks) {
                                Book *book = [[Book alloc] init];
                                NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.bookName contains[cd] %@",book.bookName];
                                NSArray   *filteredArray = [self.fetchedBooks filteredArrayUsingPredicate:bPredicate];
                                
                                if (filteredArray.count != 0)
                                {
                                    [self.fetchedBooks removeObject:ab];
                                }
                                
                            }

                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (error) {
                                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"AP_STR_ERROR_FETCHING_ICLOUD_COPY", nil)];
                                } else {
                                    if (self.fetchedBooks.count) {
//                                        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"AP_STR_FETCHING_LOCAL_EDITIONS", nil)];
                                    } else {
                                        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"AP_STR_ZERO_LOCAL_EDITIONS", nil)];
                                    }
                                    [self.collectionView reloadData];
                                    [self.tableView reloadData];

                                }
                            });
                        }];
                    }
                } else {
                    [self proceedWithOfflineRendering];
                }
            }
        }
    }];
}

- (void)proceedWithOfflineRendering {
    dispatch_async(dispatch_get_main_queue(), ^{
//        [SVProgressHUD showWithStatus:NSLocalizedString(@"AP_STR_LOADING_LOCAL", nil) maskType:SVProgressHUDMaskTypeClear];
    });
    [[BookshelfDatasource sharedInstance] fetchEditionsFromLocalStorageWithCompletionBlock:^(NSArray *editions, NSError *error) {
        
        self.finalBooks = [editions mutableCopy];
        self.fetchedBooks = self.finalBooks.mutableCopy;
        for (Book *ab in self.fetchedBooks) {
            Book *book = [[Book alloc] init];
            NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.bookName contains[cd] %@",book.bookName];
            NSArray   *filteredArray = [self.fetchedBooks filteredArrayUsingPredicate:bPredicate];
            
            if (filteredArray.count != 0)
            {
                [self.fetchedBooks removeObject:ab];
            }
            
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"AP_STR_ERROR_FETCHING_LOCAL_COPY", nil)];
            } else {
                if (self.fetchedBooks.count) {
//                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"AP_STR_FETCHING_LOCAL_EDITIONS", nil)];
                } else {
                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"AP_STR_ZERO_LOCAL_EDITIONS", nil)];
                }
                [self.collectionView reloadData];
                [self.tableView reloadData];
            }
        });
    }];
}

- (void)proceedWithOnlineRendering {
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD showWithStatus:NSLocalizedString(@"AP_STR_LOADING", nil) maskType:SVProgressHUDMaskTypeClear];
    });
    
    if ([self.hostReachability currentReachabilityStatus] == ReachableViaWiFi || [self.hostReachability currentReachabilityStatus] == ReachableViaWWAN) {
        [[BookshelfDatasource sharedInstance] fetchAllEditionsFromiCloudWithCompletionBlock:^(NSArray *editions, NSError *error) {
            if (error || editions.count == 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
//                    [SVProgressHUD showErrorWithStatus:error.localizedDescription];
                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"AP_STR_ERROR_FETCHING_ICLOUD_COPY", nil)];
                });
            } else {
                [self setFlagForSyncedToiCloudForEditions];
                self.finalBooks = [editions mutableCopy];
                self.fetchedBooks = self.finalBooks.mutableCopy;
                for (Book *ab in self.fetchedBooks) {
                    Book *book = [[Book alloc] init];
                    NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.bookName contains[cd] %@",book.bookName];
                    NSArray   *filteredArray = [self.fetchedBooks filteredArrayUsingPredicate:bPredicate];
                    
                    if (filteredArray.count != 0)
                    {
                        [self.fetchedBooks removeObject:ab];
                    }
                    
                }

                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"AP_STR_FETCHING_EDITIONS", nil)];
                    [self.collectionView reloadData];
                    [self.tableView reloadData];
                });
            }
        } withError:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"AP_STR_ERROR_FETCHING_ICLOUD_COPY", nil)];
            });
        }];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showWithStatus:@"Check your internet connection and try again. Loading local copy..." maskType:SVProgressHUDMaskTypeClear];
        });

        [[BookshelfDatasource sharedInstance] fetchEditionsFromLocalStorageWithCompletionBlock:^(NSArray *editions, NSError *error) {
            
            self.finalBooks = [editions mutableCopy];
            self.fetchedBooks = self.finalBooks.mutableCopy;
            for (Book *ab in self.fetchedBooks) {
                Book *book = [[Book alloc] init];
                NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.bookName contains[cd] %@",book.bookName];
                NSArray   *filteredArray = [self.fetchedBooks filteredArrayUsingPredicate:bPredicate];
                
                if (filteredArray.count != 0)
                {
                    [self.fetchedBooks removeObject:ab];
                }
                
            }

            dispatch_async(dispatch_get_main_queue(), ^{
                if (error) {
                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"AP_STR_ERROR_FETCHING_LOCAL_COPY", nil)];
                } else {
                    if (self.fetchedBooks.count) {
//                        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"AP_STR_FETCHING_LOCAL_EDITIONS", nil)];
                    } else {
                        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"AP_STR_ZERO_LOCAL_EDITIONS", nil)];
                    }
                    [self.collectionView reloadData];
                    [self.tableView reloadData];
                }
            });
        }];
    }
}

- (void)setFlagForSyncedToiCloudForEditions {
    
    NSManagedObjectContext *context = [self managedObjectContext];
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"AviationBook"];
        NSError *error = nil;
        NSArray *tempAviationBooks = [context executeFetchRequest:request error:&error];
        for (AviationBook *ab in tempAviationBooks) {
            [ab setValue:[NSNumber numberWithBool:YES] forKey:@"isBookSynced"];
            if (![context save:&error]) {
                NSLog(@"Can't Delete! %@ %@", error, [error localizedDescription]);
                return;
            } else {
                NSLog(@"DELETION DONE");
            }
        }
}

- (void)initBarButtons {
    _editBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(editButtonClicked:)];
    allEditionsBarButton = [[UIBarButtonItem alloc] initWithTitle:@"All Editions" style:UIBarButtonItemStylePlain target:self action:@selector(allEditionsButtonClicked:)];
    _cancleBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancleButtonClicked:)];
//    searchBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchTapped:)];
    
    _trashBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Delete" style:UIBarButtonItemStylePlain target:self action:@selector(trashButtonClicked:)];
    
    UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
  //  buyButton.frame = CGRectMake(0, 0, 90, 20);
    [buyButton setImage:[[UIImage imageNamed:@"storeneww"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    
    [buyButton addTarget:self action:@selector(addButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    purchaseButton = [[UIBarButtonItem alloc] initWithCustomView:buyButton];
//    purchaseButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"store1.jpg"] style:UIBarButtonItemStylePlain target:self action:@selector(addButtonClicked:)];

    
    // Edit buttons
    moveBarButton =[[UIBarButtonItem alloc] initWithTitle:@"Move" style:UIBarButtonItemStylePlain target:self action:@selector(moveButtonClicked:)];
    doneBarButton =[[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain   target:self action:@selector(doneButtonClicked:)];
    selectAllBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Select All" style:UIBarButtonItemStylePlain target:self action:@selector(selectAllTapped:)];
    [selectAllBarButton setTag:0];
    iCloudBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"sync"] style:UIBarButtonItemStylePlain target:self action:@selector(iCloudSyncBarButtonAction:)];
    
    //FAQ's
    UIButton *faqButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
   // faqButton.frame = CGRectMake(0, 0, 22, 22);
//    [faqButton setTitle:@"FAQ's" forState:UIControlStateNormal];

    [faqButton setImage:[[UIImage imageNamed:@"faqneww"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    
    [faqButton addTarget:self action:@selector(faqButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    faqsButton = [[UIBarButtonItem alloc] initWithCustomView:faqButton];
//purchaseinfo
    UIButton *purchaseButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
   // purchaseButton.frame = CGRectMake(0, 0, 20, 20);
    //    [faqButton setTitle:@"FAQ's" forState:UIControlStateNormal];
    
    [purchaseButton setImage:[[UIImage imageNamed:@"shoppinginfoneww"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    
    [purchaseButton addTarget:self action:@selector(purchaseInfoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    purchaseInfoButton = [[UIBarButtonItem alloc] initWithCustomView:purchaseButton];

    
    [self configureNotificationBarButton];
//    [self switchSegmentControl];
}

- (void) configureNotificationBarButton {
    
    UIImage* notificationImage = [UIImage imageNamed:@"icn_notification.png"];
    CGRect frameimg = CGRectMake(0, 0, notificationImage.size.width/1.0, notificationImage.size.height/1.0);
    
    UIView *notificationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,  notificationImage.size.width,  notificationImage.size.height)];
    
    UIButton *notificationButton = [[UIButton alloc] initWithFrame:frameimg];
    [notificationButton setBackgroundImage:notificationImage forState:UIControlStateNormal];
    notificationButton.tintColor = self.view.tintColor;
    [notificationButton addTarget:self action:@selector(notificationButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    notificationsCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, -1,  notificationImage.size.width,  notificationImage.size.height - 5)];
    notificationsCountLabel.backgroundColor = [UIColor clearColor];
    notificationsCountLabel.text = [NSString stringWithFormat:@"%ld", (unsigned long)[[NotificationManager sharedInstance] unreadCount]];
    notificationsCountLabel.textAlignment = NSTextAlignmentCenter;
    notificationsCountLabel.font = AP_FONT_S;
    notificationsCountLabel.textColor = [UIColor redColor];
    
    [notificationView addSubview:notificationButton];
    [notificationView addSubview:notificationsCountLabel];
    notificationView.tintColor = self.view.tintColor;
//    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, notificationView.frame.size.width, notificationView.frame.size.height)];
//    [button addTarget:self action:@selector(notificationButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [button setBackgroundColor:[UIColor clearColor]];
//    [button setTitle:@"" forState:UIControlStateNormal];
//    [notificationView addSubview:button];
    UIBarButtonItem *notifBarButton = [[UIBarButtonItem alloc] initWithCustomView:notificationView];
    notificationBarButton = notifBarButton;
}

- (void) updateUnreadNotificationsCount {
    NSUInteger unreadCount = [[NotificationManager sharedInstance] unreadCount];
    NSLog(@"UNREAD COUNT = %ld", (unsigned long)unreadCount);
    if (unreadCount == 0) {
        notificationsCountLabel.textColor = [UIColor blackColor];
        notificationsCountLabel.text = @"0";
    } else {
        notificationsCountLabel.textColor = [UIColor redColor];
        notificationsCountLabel.text = [NSString stringWithFormat:@"%ld", (unsigned long)[[NotificationManager sharedInstance] unreadCount]];
    }
    
}

- (IBAction)notificationButtonTapped:(id)sender {
    NotificationsViewController *notificationsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationsViewController"];
    [self.navigationController pushViewController:notificationsViewController animated:YES];
}

-(void)switchSegmentControl {
    NSArray *itemArray = [NSArray arrayWithObjects:
                          [UIImage imageNamed:@"Thumbs.png"],
                          [UIImage imageNamed:@"list_icon_w.png"],
                          nil];
    UISegmentedControl * switcher = [[UISegmentedControl alloc] initWithItems: itemArray];
    if (self.isGrid) {
        [switcher setSelectedSegmentIndex:0];
    }
    else {
        [switcher setSelectedSegmentIndex:1];
    }
    [switcher addTarget: self
                 action: @selector(onSegmentChange:)
       forControlEvents: UIControlEventValueChanged];
    
    switchViewBarButton  = [[UIBarButtonItem alloc] initWithCustomView:switcher];
    
}

- (void)switchToNormalMode {
    if (self.isGrid) {
        _editMode = NO;
    }
    else {
        _editMode = NO;
        [self.tableView setEditing:NO animated:NO];
    }
    [self switchSegmentControl];
    [[self navigationItem] setLeftBarButtonItems:[NSArray arrayWithObjects:allEditionsBarButton, nil]];
    [[self navigationItem] setRightBarButtonItems:[NSArray arrayWithObjects:purchaseInfoButton, faqsButton, notificationBarButton, purchaseButton, iCloudBarButton, _editBarButton, switchViewBarButton,  nil]];
}

- (void)switchToEditMode {
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
    if (self.isGrid) {
        _editMode = YES;
        _trashBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Delete" style:UIBarButtonItemStylePlain target:self action:@selector(trashButtonClicked:)];
        [self setTrashButtonToMode:NO];

        moveBarButton =[[UIBarButtonItem alloc] initWithTitle:@"Move" style:UIBarButtonItemStylePlain target:self action:@selector(moveButtonClicked:)];
        doneBarButton =[[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain   target:self action:@selector(doneButtonClicked:)];
        selectAllBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Select All" style:UIBarButtonItemStylePlain target:self action:@selector(selectAllTapped:)];
        
        self.navigationItem.rightBarButtonItems = @[doneBarButton,selectAllBarButton];
        self.navigationItem.leftBarButtonItems = @[moveBarButton,_trashBarButton];
    }
    else {
        [self.tableView setEditing:YES animated:YES];
        [self.tableView reloadData];
        if (self.tableView.editing) {
            _editMode = YES;
            _trashBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Delete" style:UIBarButtonItemStylePlain target:self action:@selector(trashButtonClicked:)];
            [self setTrashButtonToMode:YES];
            
            moveBarButton =[[UIBarButtonItem alloc] initWithTitle:@"Move" style:UIBarButtonItemStylePlain target:self action:@selector(moveButtonClicked:)];
            
            doneBarButton =[[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain   target:self action:@selector(doneButtonClicked:)];
            selectAllBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Select All" style:UIBarButtonItemStylePlain target:self action:@selector(selectAllTapped:)];
            
            self.navigationItem.rightBarButtonItems = @[doneBarButton,selectAllBarButton];
            self.navigationItem.leftBarButtonItems = @[moveBarButton,_trashBarButton];
        }
        else {
        }
    }
}

- (void)dismissPopovers {
    if (searchcontroller) {
        [searchcontroller dismissMe];
    }
    
    if (publicationcontroller) {
        [publicationcontroller dismissMe];
    }
    
    if (inAppController) {
        [inAppController dismissViewControllerAnimated:YES
                                            completion:^{
                                                
                                            }];
    }
}

#pragma mark Actions

- (IBAction)onSegmentChange:(UISegmentedControl*)sender {
    [self dismissPopovers];
    
    if (sender.selectedSegmentIndex == 0) {
        self.tableView.hidden = YES;
        self.collectionView.hidden = NO;
        self.isGrid = YES;
    }
    else {
        self.collectionView.hidden = YES;
        self.tableView.hidden = NO;
        self.isGrid = NO;
    }
    
}

- (void)iCloudSyncBarButtonAction:(UIBarButtonItem *)sender
{
    [self initializeBookshelfDataWithAlert:YES];
  //  [self apiCallToGetPurchasedBook];
}
-(void)apiCallToGetPurchasedBook
{
    NSMutableDictionary *userIDDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[AviationPressIAPHelper getUSERID], @"UserID", nil];
    [AviationPressIAPHelper showLoaderWithText:@"Getting all purchased books..."];
    [AviationPressIAPHelper performGetOnTarget:self forAPI:ADDPURCHASEDBOOK withParameter:userIDDic withCompletion:^(id responseObject)
     {
         [AviationPressIAPHelper hideLoaderText];
         if (![responseObject isKindOfClass:[NSError class]]) // Response
         {
//             UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//
//             UINavigationController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"BookshelfNavigationViewController"];
//             rootViewController.navigationBar.tintColor = nil;
//             rootViewController.navigationBar.backIndicatorImage = nil;
//             [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
             [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Updated" andWithSubTitle:@"Bookshelf is updated now." withCompletion:^(BOOL success) {
                 
             }];
             
         }else //Error
         {
             //            NSError *error = (NSError *)responseObject;
             [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"No Books Found" andWithSubTitle:@"No books found for this user at this time" withCompletion:^(BOOL success)
              {
                  dispatch_async(dispatch_get_main_queue(), ^
                                 {
                                     
//                                     UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                                     UINavigationController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"BookshelfNavigationViewController"];
//                                     rootViewController.navigationBar.tintColor = nil;
//                                     rootViewController.navigationBar.backIndicatorImage = nil;
//                                     [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
//
                                });
              }];
         }
     } loaderText:@"Getting all purchased books..."];
}

- (void)editButtonClicked:(id)sender {
    [self dismissPopovers];
    [self switchToEditMode];
}

- (void)moveButtonClicked:(id)sender {
    [self dismissPopovers];
    
    publicationcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"PublicationPopOverViewController"];
    publicationcontroller.delegate = self;
    publicationcontroller.isNormal = NO;
    publicationcontroller.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:publicationcontroller animated:YES completion:nil];
    
    // configure the Popover presentation controller
    UIPopoverPresentationController *popController = [publicationcontroller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    popController.delegate = self;
    popController.barButtonItem = (UIBarButtonItem*)sender;
}

- (void)selectAllTapped:(id)sender {
    [self dismissPopovers];
    
    if (self.isSelectAll == NO) {
        self.isSelectAll = YES;
        selectAllBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Select None" style:UIBarButtonItemStylePlain target:self action:@selector(selectAllTapped:)];
        self.navigationItem.rightBarButtonItems = @[doneBarButton,selectAllBarButton];
        
        [self setTrashButtonToMode:YES];
        for (Book *obj in self.fetchedBooks) {
            obj.isSelected = [NSNumber numberWithBool:YES];
            if ([obj.bookDesc isEqualToString:EEPP_11_EDITION_NAME]) {
                [self setTrashButtonToMode:NO];
            }
        }
        
        for (NSInteger i = 0; i < [self.fetchedBooks count]; i++){
            Book *obj = [self.fetchedBooks objectAtIndex:i];
            if ([obj.bookDesc isEqualToString:EEPP_11_EDITION_NAME]) {
                [self setTrashButtonToMode:NO];
            }
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
    }
    else {
        self.isSelectAll = NO;
        selectAllBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Select All" style:UIBarButtonItemStylePlain target:self action:@selector(selectAllTapped:)];
        self.navigationItem.rightBarButtonItems = @[doneBarButton,selectAllBarButton];
        
        [self setTrashButtonToMode:NO];
        
        for (Book *obj in self.fetchedBooks) {
            obj.isSelected = [NSNumber numberWithBool:NO];
        }
        for (NSInteger i = 0; i < [self.fetchedBooks count]; i++) {
            [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] animated:NO];
        }
    }
    
    [self.collectionView reloadData];
}

- (void)doneButtonClicked:(id)sender {
    [self dismissPopovers];
    self.isSelectAll = YES;
    [self selectAllTapped:nil];
    [self switchToNormalMode];
}

- (void)trashButtonClicked:(id)sender {
    [self dismissPopovers];
    
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    
    for (Book *book in self.fetchedBooks ) {
        if(book.isSelected.integerValue == 0) {
            [temp addObject:book];
        } else if (book.isSelected.integerValue == 1) {
            if ([book.bookDesc isEqualToString:EEPP_11_EDITION_NAME] && [book.category isEqualToString:@"All Editions"]) {
                [temp addObject:book];
            }
            NSManagedObjectContext *context = [self managedObjectContext];
            NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"AviationBook"];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bookEditionNumber == %@", book.bookDesc];
            
            NSCompoundPredicate *compounPredicate;
            
            if ([book.bookDesc isEqualToString:EEPP_11_EDITION_NAME] && ![book.category isEqualToString:@"All Editions"]) {
                compounPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate]];
            } else {
                NSPredicate *predicateNot11 = [NSPredicate predicateWithFormat:@"NOT (bookEditionNumber CONTAINS %@)", EEPP_11_EDITION_NAME];
                compounPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate, predicateNot11]];
            }

            [request setPredicate:compounPredicate];
            NSError *error = nil;
            NSArray *tempAviationBooks = [context executeFetchRequest:request error:&error];
            
            if (tempAviationBooks.count > 0) {
                for (AviationBook *ab in tempAviationBooks) {
                    [context deleteObject:ab];
                    [AviationPressIAPHelper removeBookPurchased:book];
                }
                
                if (![context save:&error]) {
                    NSLog(@"Can't Delete! %@ %@", error, [error localizedDescription]);
                    return;
                } else {
                    NSLog(@"DELETION DONE");
                }
            } else {
                
            }
        }
    }
    self.fetchedBooks = nil;
    self.isSelectAll = NO;
    self.fetchedBooks = temp;
    [self.collectionView reloadData];
    [self.tableView reloadData];
    
}

- (void)allEditionsButtonClicked:(id)sender {
    [self dismissPopovers];
    
    publicationcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"PublicationPopOverViewController"];
    publicationcontroller.delegate = self;
    publicationcontroller.isNormal = YES;
    publicationcontroller.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:publicationcontroller animated:YES completion:nil];
    
    // configure the Popover presentation controller
    UIPopoverPresentationController *popController = [publicationcontroller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    popController.delegate = self;
    popController.barButtonItem = (UIBarButtonItem*)sender;
}

- (void)cancleButtonClicked:(id)sender {
    [self dismissPopovers];
}
- (void)faqButtonClicked:(id)sender {
    
    FAQViewController *faqVCObj = [self.storyboard instantiateViewControllerWithIdentifier:@"faqVCID"];
    [self.navigationController pushViewController:faqVCObj animated:YES];

}

#pragma mark:- Purchase Info
- (void)purchaseInfoButtonClicked:(id)sender
{
    //Load Purchase Amount Purchase Date Popup
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Purchase Information"
                                                                              message: @"Please enter the purchase information"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
        textField.placeholder = @"Purchase Date";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        purchaseDate = textField;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
        textField.placeholder = @"Purchase Amount";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        purchaseAmount = textField;
    }];

    [alertController addAction:[UIAlertAction actionWithTitle:@"Submit" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                {
                                    NSArray * textfields = alertController.textFields;
                                    UITextField * purchaseDateTF = textfields[0];
                                    UITextField * purchaseAmountTF = textfields[1];
                                    
                                    NSLog(@"%@",purchaseDateTF.text);
                                    NSLog(@"%@",purchaseAmountTF.text);
                                    //Call Web Service
                                    NSMutableDictionary *userIDDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[AviationPressIAPHelper getUSERID], @"userid", purchaseAmount.text, @"PurchaseAmount", purchaseDate.text, @"purchaseDate", nil];
                                    [AviationPressIAPHelper showLoaderWithText:@"Updating purchase info..."];
                                    [AviationPressIAPHelper performPostonTarget:self forAPI:PURCHASEINFO withParameter:userIDDic withCompletion:^(id responseObject, long statusCode)
                                    {
                                        [AviationPressIAPHelper hideLoaderText];
                                        if (![responseObject isKindOfClass:[NSError class]]) // Response
                                        {
                                            [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Success" andWithSubTitle:@"Purchase information updated." withCompletion:^(BOOL success)
                                             {

                                             }];
                                        }else //Error
                                        {
                                            [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Failed" andWithSubTitle:@"Purchase information update failed" withCompletion:^(BOOL success)
                                             {
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     
                                                     
                                                 });
                                             }];
                                        }
                                    } loaderText:@"Updating purchase info..."];
                                }]];
    [self presentViewController:alertController animated:YES completion:nil];
    [self instantiateDatePicker];
    purchaseAmount.delegate = self;

}
-(void)instantiateDatePicker
{
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    [datePicker addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    purchaseDate.inputView = datePicker;
    
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(ShowSelectedDate)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
    
    [purchaseDate setInputAccessoryView:toolBar];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    //    [comps setYear:30];
    //    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    [comps setYear:-5];
    NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    
    [datePicker setMaximumDate:currentDate];
    [datePicker setMinimumDate:minDate];
    
    
}
-(void)ShowSelectedDate
{
    [purchaseDate resignFirstResponder];
    if (purchaseAmount.text.length < 1)
    {
        [purchaseAmount becomeFirstResponder];
    }
}
- (void)onDatePickerValueChanged:(UIDatePicker *)datePicker
{
    NSDateFormatter * df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MM-dd-yyyy"];
    purchaseDate.text = [df stringFromDate:datePicker.date];
}

- (void)addButtonClicked:(id)sender {
    inAppController = [self.storyboard instantiateViewControllerWithIdentifier:@"InAppPurchaseViewController"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:inAppController];
    [self.navigationController pushViewController:inAppController animated:YES];
    
//    [self presentViewController:navController animated:YES completion:^{
//    }];
}

- (void)searchTapped:(id)sender {
    [self dismissPopovers];
    
    searchcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
    searchcontroller.delegate = self;
    searchcontroller.preferredContentSize = CGSizeMake(400, 80);
    
    searchcontroller.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:searchcontroller animated:YES completion:nil];
    
    // configure the Popover presentation controller
    UIPopoverPresentationController *popController = [searchcontroller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    popController.delegate = self;
    popController.barButtonItem = (UIBarButtonItem*)sender;
}

- (void)setTrashButtonToMode:(BOOL) mode {
    if (mode == NO) {
        _trashBarButton.enabled = NO;
    } else {
        _trashBarButton.enabled = YES;
    }
}

#pragma mark - UICollectionView Delegate & DataSource Methods
//------------------------------------
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return self.fetchedBooks.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    BookShelfCell *cell = (BookShelfCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"BookShelfCell" forIndexPath:indexPath];
    Book *book = [self.fetchedBooks objectAtIndex:indexPath.row];
    [cell setBook:book];
    [cell configCell];
    
    if (self.isSelectAll || book.isSelected.integerValue == 1 ) {
            cell.deleteImage.hidden = NO;
            cell.bookImage.alpha = 0.8;
    }
    else {
        cell.bookImage.alpha = 1.0;
    }
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Book *book = [self.fetchedBooks objectAtIndex:indexPath.row];
    if (_editMode) {
        BookShelfCell *cell = (BookShelfCell*)[collectionView cellForItemAtIndexPath:indexPath];
        if (cell.deleteImage.hidden == YES) {
            // SELECT
            cell.deleteImage.hidden = NO;
            cell.bookImage.alpha = 0.8;
            [self setTrashButtonToMode:YES];
            book.isSelected = [NSNumber numberWithBool:YES];
            if (([book.bookDesc isEqualToString:EEPP_11_EDITION_NAME] && [book.category isEqualToString:@"All Editions"])) {
                cell.deleteImage.hidden = NO;
                cell.bookImage.alpha = 0.8;
                [self setTrashButtonToMode:NO];
                book.isSelected = [NSNumber numberWithBool:YES];
            }
        }
        else {
            // DESELECT
            cell.deleteImage.hidden = YES;
            cell.bookImage.alpha = 1;
            [self setTrashButtonToMode:NO];
            book.isSelected = [NSNumber numberWithBool:NO];
            if (([book.bookDesc isEqualToString:EEPP_11_EDITION_NAME] && [book.category isEqualToString:@"All Editions"])) {
                [self setTrashButtonToMode:NO];
            }
        }
        
        [self handleBarButtonChanges];
        
    } else {
        [SVProgressHUD showWithStatus:@"Initializing..."];
        NSLog(@"Book selected == %@", book.documentsURL);
        BookshelfDatasource*datasource = [BookshelfDatasource sharedInstance];
        AviationPressLandingViewController *controller = [datasource getInitializedVCForURL:book.documentsURL forBookEditionNumber:book.bookDesc andBookObj:book];
        controller.bookEditionName = book.bookDesc;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark - UITableView Datasource methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fetchedBooks.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"shelfTableCell";
    BookShelfTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    //Configure Cell
    Book *obj = [self.fetchedBooks objectAtIndex:indexPath.row];
    [cell setBook:obj];
    [cell configCell];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableview canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    // ... update data source.
    [self.fetchedBooks exchangeObjectAtIndex:fromIndexPath.row withObjectAtIndex:toIndexPath.row];
}

#pragma mark UITableView Editing methods

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    Book *obj = [self.fetchedBooks objectAtIndex:indexPath.row];
    if ([obj.bookDesc isEqualToString:EEPP_11_EDITION_NAME]) {
        return UITableViewCellEditingStyleNone;
    }
    return UITableViewCellEditingStyleDelete;
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark - Table View delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Book *book = [self.fetchedBooks objectAtIndex:indexPath.row];
    
    if (_editMode) {
        book.isSelected = [NSNumber numberWithBool:YES];
        self.isSelectAll = NO;

        if (([book.bookDesc isEqualToString:EEPP_11_EDITION_NAME] && [book.category isEqualToString:@"All Editions"])) {
            [self setTrashButtonToMode:NO];
        } else {
            [self setTrashButtonToMode:YES];
        }
        [self handleBarButtonChanges];
    } else {
        [SVProgressHUD showWithStatus:@"Initializing..."];
        BookshelfDatasource*example = [BookshelfDatasource sharedInstance];
        AviationPressLandingViewController *controller = [example getInitializedVCForURL:book.documentsURL forBookEditionNumber:book.bookDesc andBookObj:book];
        controller.bookEditionName = book.bookDesc;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    Book *book = [self.fetchedBooks objectAtIndex:indexPath.row];
    
    if (_editMode) {
        book.isSelected = [NSNumber numberWithBool:NO];
        self.isSelectAll = NO;
        [self setTrashButtonToMode:NO];
        if ([book.bookDesc isEqualToString:EEPP_11_EDITION_NAME] && [book.category isEqualToString:@"All Editions"]) {
            [self setTrashButtonToMode:NO];
        }
        [self handleBarButtonChanges];
    }
}

#pragma mark - Handle Bar Button changes

- (void)handleBarButtonChanges {
    for (Book *book in self.fetchedBooks) {
        NSLog(@"NOW CHECK ARRAY = %@, %@", book.bookDesc, book.isSelected);
        NSLog(@"CHECK FOR ***CATEGORY*** = %@", book.category);
    }

    NSPredicate *predicateAllSelected = [NSPredicate predicateWithFormat:@"SELF.isSelected == 0"];
    NSArray *filteredArray = [self.fetchedBooks filteredArrayUsingPredicate:predicateAllSelected];
    if (filteredArray && filteredArray.count > 0) {
        // even if one book is DeSelected
        self.isSelectAll = NO;
        selectAllBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Select All" style:UIBarButtonItemStylePlain target:self action:@selector(selectAllTapped:)];
        self.navigationItem.rightBarButtonItems = @[doneBarButton,selectAllBarButton];
        NSPredicate *predicate11thEditionDeSelected = [NSPredicate predicateWithFormat:@"SELF.bookDesc CONTAINS %@", EEPP_11_EDITION_NAME];
        NSArray *filtered11thEditionDeSelected = [filteredArray filteredArrayUsingPredicate:predicate11thEditionDeSelected];
        if (filtered11thEditionDeSelected && filtered11thEditionDeSelected.count > 0) {
            // 11th Edition is Deselected
            [self setTrashButtonToMode:YES];
        } else {
            [self setTrashButtonToMode:NO];
        }
    } else {
        // All books selected
        self.isSelectAll = YES;
        selectAllBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Select None" style:UIBarButtonItemStylePlain target:self action:@selector(selectAllTapped:)];
        self.navigationItem.rightBarButtonItems = @[doneBarButton,selectAllBarButton];
        [self setTrashButtonToMode:YES];
        
        NSPredicate *predicateBookSelection;
        NSPredicate *predicateBookDescription;
        
        predicateBookSelection = [NSPredicate predicateWithFormat:@"SELF.isSelected == 1"];
        predicateBookDescription = [NSPredicate predicateWithFormat:@"SELF.bookDesc contains[cd] %@", EEPP_11_EDITION_NAME];
        
        NSCompoundPredicate *compooundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicateBookSelection, predicateBookDescription]];
        NSArray *filteredSelectedN11Edition = [[self.fetchedBooks filteredArrayUsingPredicate:compooundPredicate] mutableCopy];
        if (filteredSelectedN11Edition && filteredSelectedN11Edition.count > 0) {
            [self setTrashButtonToMode:NO];
        }
    }
}

#pragma - Methods

- (void)handlePanGesture:(UIPanGestureRecognizer *)panRecognizer {
    CGPoint locationPoint = [panRecognizer locationInView:self.collectionView];
    if (panRecognizer.state == UIGestureRecognizerStateBegan) {
        
        NSIndexPath *indexPathOfMovingCell = [self.collectionView indexPathForItemAtPoint:locationPoint];
        UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPathOfMovingCell];
        
        UIGraphicsBeginImageContext(cell.bounds.size);
        [cell.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *cellImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        self.movingCell = [[UIImageView alloc] initWithImage:cellImage];
        [self.movingCell setCenter:locationPoint];
        [self.movingCell setAlpha:0.75f];
        [self.collectionView addSubview:self.movingCell];
        
    }
    
    if (panRecognizer.state == UIGestureRecognizerStateChanged) {
        [self.movingCell setCenter:locationPoint];
    }
    
    if (panRecognizer.state == UIGestureRecognizerStateEnded) {
        [self.movingCell removeFromSuperview];
    }
}

- (IBAction)longPressGestureRecognized:(id)sender {
    
    UILongPressGestureRecognizer *longPress = (UILongPressGestureRecognizer *)sender;
    UIGestureRecognizerState state = longPress.state;
    
    CGPoint location = [longPress locationInView:self.collectionView];
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:location];
    
    static UIView       *snapshot = nil;        ///< A snapshot of the row user is moving.
    static NSIndexPath  *sourceIndexPath = nil; ///< Initial index path, where gesture begins.
    
    switch (state) {
        case UIGestureRecognizerStateBegan: {
            if (indexPath) {
                sourceIndexPath = indexPath;
                
                BookShelfCell *cell = (BookShelfCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
                
                
                // Take a snapshot of the selected row using helper method.
                snapshot = [self customSnapshoFromView:cell];
                
                // Add the snapshot as subview, centered at cell's center...
                __block CGPoint center = cell.center;
                snapshot.center = center;
                snapshot.alpha = 0.0;
                [self.collectionView addSubview:snapshot];
                [UIView animateWithDuration:0.25 animations:^{
                    
                    // Offset for gesture location.
                    center.y = location.y;
                    snapshot.center = center;
                    snapshot.transform = CGAffineTransformMakeScale(1.05, 1.05);
                    snapshot.alpha = 0.98;
                    
                    // Fade out.
                    cell.alpha = 0.0;
                }  completion:^(BOOL finished) {
                    
                    
                }];
            }
            break;
        }
            
        case UIGestureRecognizerStateChanged: {
            CGPoint center = snapshot.center;
            if (self.collectionView.collectionViewLayout != self.listLayout) {
                center.x = location.x;
            }
            center.y = location.y;
            snapshot.center = center;
            
            // Is destination valid and is it different from source?
            if (indexPath && ![indexPath isEqual:sourceIndexPath]) {
                
                // ... update data source.
                [self.fetchedBooks exchangeObjectAtIndex:indexPath.row withObjectAtIndex:sourceIndexPath.row];
                
                // Hide view
                BookShelfCell *cell = (BookShelfCell*)[self.collectionView cellForItemAtIndexPath:sourceIndexPath];
                cell.alpha = 0.0;
                cell.hidden = YES;
                
                // ... move the rows.
                [self.collectionView moveItemAtIndexPath:sourceIndexPath toIndexPath:indexPath];
                
                [self.tableView reloadData];
                
                // ... and update source so it is in sync with UI changes.
                sourceIndexPath = indexPath;
                
            }
            break;
        }
            
        default: {
            //            NSLog(@"Clean Up");
            
            // Clean up.
            BookShelfCell *cell = (BookShelfCell*)[self.collectionView cellForItemAtIndexPath:sourceIndexPath];
            [UIView animateWithDuration:0.25 animations:^{
                
                snapshot.center = cell.center;
                snapshot.transform = CGAffineTransformIdentity;
                snapshot.alpha = 0.0;
                
                // Undo the fade-out effect we did.
                cell.alpha = 1.0;
                cell.hidden = NO;
                
            } completion:^(BOOL finished) {
                
                [snapshot removeFromSuperview];
                snapshot = nil;
                
            }];
            sourceIndexPath = nil;
            break;
        }
    }
}

-(void)bookDeleteTapped:(UIButton *)sender {
    [self.collectionView performBatchUpdates:^{
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:(BookShelfCell *)sender.superview.superview];
        [self.fetchedBooks removeObjectAtIndex:indexPath.row];
        [self.collectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
        
    } completion:nil];
}

#pragma mark - Private Methods

- (BOOL) isDeletionModeActiveForCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout {
    return self.isDeletionModeActive;
}

-(void)addItem {
}

#pragma mark - Helper methods

/** @brief Returns a customized snapshot of a given view. */
- (UIView *)customSnapshoFromView:(UIView *)inputView {
    
    // Make an image from the input view.
    UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, NO, 0);
    [inputView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Create an image view.
    UIView *snapshot = [[UIImageView alloc] initWithImage:image];
    snapshot.layer.masksToBounds = NO;
    snapshot.layer.cornerRadius = 0.0;
    snapshot.layer.shadowOffset = CGSizeMake(-5.0, 0.0);
    snapshot.layer.shadowRadius = 5.0;
    snapshot.layer.shadowOpacity = 0.4;
    
    return snapshot;
}

#pragma Search

-(void)findBooks:(NSString *)input {
    self.fetchedBooks = nil;
    self.fetchedBooks = [[NSMutableArray alloc] init];
    
    if (input.length == 0) {
        for (Book *book in self.finalBooks) {
            [self.fetchedBooks addObject:book];
        }
    }
    else {
        NSPredicate *predicateBookName;
        NSPredicate *predicateBookDescription;
        
        predicateBookName = [NSPredicate predicateWithFormat:@"SELF.bookName contains[cd] %@", input];
        predicateBookDescription = [NSPredicate predicateWithFormat:@"SELF.bookDesc contains[cd] %@", input];
        
        NSCompoundPredicate *compooundPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicateBookName, predicateBookDescription]];
        self.fetchedBooks = [[self.finalBooks filteredArrayUsingPredicate:compooundPredicate] mutableCopy];
        NSMutableSet *names = [NSMutableSet set];
         NSMutableArray *uniquearray = [self.fetchedBooks valueForKeyPath:@"@distinctUnionOfObjects.self"];

        for (Book *ab in self.fetchedBooks) {
            Book *book = [[Book alloc] init];
            NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.bookName contains[cd] %@",book.bookName];
            NSArray   *filteredArray = [self.fetchedBooks filteredArrayUsingPredicate:bPredicate];
            
            if (filteredArray.count != 0)
            {
                [self.fetchedBooks removeObject:ab];
            }
            
        }
//        [_book.editionRecord valueForKey:CLOUD_RECORD_EDITION_NAME];
//        [_book.editionRecord valueForKey:CLOUD_RECORD_EDITION_NUMBER];
//        for (Book *book in self.finalBooks) {
//            NSRange stringRange = [book.bookName rangeOfString:input options:NSCaseInsensitiveSearch];
//            if(stringRange.location != NSNotFound){
//                [self.fetchedBooks addObject:book];
//            }
//        }
    }
    
    [self.collectionView reloadData];
    [self.tableView reloadData];
}

- (void)selectedAllEdition:(BOOL) isAllEdition {
    if (isAllEdition) {
        
    } else {
        
    }
}

-(void)categoryBooks:(NSString *)input {
    
    allEditionsBarButton = [[UIBarButtonItem alloc] initWithTitle:input style:UIBarButtonItemStylePlain target:self action:@selector(allEditionsButtonClicked:)];
    [[self navigationItem] setLeftBarButtonItems:[NSArray arrayWithObjects:allEditionsBarButton, nil]];

    self.fetchedBooks = nil;
    self.fetchedBooks = [[NSMutableArray alloc] init];
    
    self.fetchedBooks = [[[BookshelfDatasource sharedInstance] fetchForCategory:input] mutableCopy];
    for (Book *ab in self.fetchedBooks) {
        Book *book = [[Book alloc] init];
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.bookName contains[cd] %@",book.bookName];
        NSArray   *filteredArray = [self.fetchedBooks filteredArrayUsingPredicate:bPredicate];
        
        if (filteredArray.count != 0)
        {
            [self.fetchedBooks removeObject:ab];
        }
        
    }
    [self.collectionView reloadData];
    [self.tableView reloadData];
    [self switchToNormalMode];
}

-(void)moveBooks:(NSString *)input {
    NSError *error;
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];    
    NSMutableSet *booksSet = [NSMutableSet new];
    for (Book *book in self.fetchedBooks ) {
        if(book.isSelected.integerValue == 1) {
            book.category = input;
            NSFetchRequest *req = [AviationBook fetchRequest];
            [req setPredicate:[NSPredicate predicateWithFormat:@"bookEditionNumber == %@", book.bookDesc]];
            NSArray *avBooks = [managedObjectContext executeFetchRequest:req error:nil];
            if (avBooks && [avBooks lastObject]) {
                AviationBook *bk = [avBooks lastObject];
                [booksSet addObject:bk];
            }
        }
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Edition"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name contains %@", input];
    [fetchRequest setPredicate:predicate];
    NSArray *editions = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if (editions && [editions lastObject]) {
        Edition *editionCD = [editions lastObject];
        [editionCD addHasBook:booksSet];
    }
    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }

    self.isSelectAll = YES;
    [self selectAllTapped:nil];
    [self categoryBooks:input];
}

#pragma mark - Reachability methods

- (void)updateInterfaceWithReachability:(Reachability *)reachability {
    if (reachability == self.hostReachability) {
        NetworkStatus netStatus = [reachability currentReachabilityStatus];
        
        switch (netStatus) {
            case NotReachable: {
//                [SVProgressHUD showErrorWithStatus:@"Internet connection not available"];
            }
                break;
            case ReachableViaWiFi:
            case ReachableViaWWAN:{
//                [SVProgressHUD showSuccessWithStatus:@"Connected"];
            }
                break;
            default:
                break;
        }
    }
}

- (void) reachabilityChanged:(NSNotification *)note {
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self updateInterfaceWithReachability:curReach];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

#pragma mark - PSCExampleRunner

//- (nullable UIViewController *)currentViewController {
////    return [self.navigationController.viewControllers lastObject];
//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark:- UITextView Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
        NSString *regEx = @"0123456789.";
        NSString* val = [[textField text] stringByReplacingCharactersInRange:range withString:string];
        NSCharacterSet *allowedCharacterSet = [NSCharacterSet characterSetWithCharactersInString:regEx];
        if ([[string componentsSeparatedByCharactersInSet:[allowedCharacterSet invertedSet]] count] > 1 || [val length] > 5)
        {
            return NO;
        }
        return YES;
}

@end
