//
//  APNavigationController.m
//  AviationPress
//
//  Created by Sonia Mane on 30/01/17.
//  Copyright © 2017 Aptara. All rights reserved.
//

#import "APNavigationController.h"
#import <CWStatusBarNotification/CWStatusBarNotification.h>
#import "APConstants.h"
#import "NotificationManager.h"
#import "APNotification.h"
#import "NotificationsViewController.h"
#import "DataUpdater.h"

@interface APNavigationController ()
@property (strong, nonatomic) APNotification *notificationObject;
@property (strong, nonatomic) CWStatusBarNotification *cwnotification;
@end

@implementation APNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleForegroundNotifications:) name:NOTIF_HANDLE_FOREGROUND_PUSH object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enteredForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)enteredForeground:(NSNotification *)nsnotification {
    NSLog(@"Entered Foreground ---------");
    [DataUpdater getNotificationsFromServer:^(BOOL didFinish) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_SAVED_TO_DB object:nil];
    }];
}

- (void)handleForegroundNotifications:(NSNotification *)nsnotification {
    NSLog(@"Notification received = %@",nsnotification);
    NSDictionary *notifDictioanry = [nsnotification object];
    
    NSString *notifciationString = notifDictioanry[@"notifciationString"];
    NSString *notificationID = notifDictioanry[@"notificationkey"];
    NotificationManager *notifManager = [NotificationManager sharedInstance];
    if ([notifManager saveNotificationsInDB:notifciationString withNotifId:notificationID] ) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_SAVED_TO_DB object:nil];
    }
    [self setupNotification];
    UIView *view = [[NSBundle mainBundle] loadNibNamed:@"NotificationView" owner:nil options:nil][0];
    
    UILabel *notifLabel = (UILabel *)[view viewWithTag:2];

    notifLabel.text = notifciationString;
    [self.cwnotification displayNotificationWithView:view forDuration:10.0];
        
    self.cwnotification.notificationTappedBlock = ^(void){
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_DID_TAP_ON_NOTIFICATION object:notifciationString];
    };
}

# pragma mark - show notification banner

- (void)setupNotification {
    self.cwnotification = [CWStatusBarNotification new];
    self.cwnotification.notificationAnimationInStyle = CWNotificationAnimationStyleTop;
    self.cwnotification.notificationAnimationOutStyle = CWNotificationAnimationStyleBottom;
    self.cwnotification.notificationStyle = CWNotificationStyleNavigationBarNotification;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
