//
//  NotificationsViewController.h
//  AviationPress
//
//  Created by Sonia Mane on 24/01/17.
//  Copyright © 2017 Aptara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationsViewController : UIViewController
@property (nonatomic, strong) NSMutableArray *notificationsArray;
@end
