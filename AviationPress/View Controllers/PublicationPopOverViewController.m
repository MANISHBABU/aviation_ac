//
//  PublicationPopOverViewController.m
//  AviationPress
//
//  Created by Sonia Mane on 25/11/16.
//  Copyright © 2016 Sonia Mane. All rights reserved.
//

#import "PublicationPopOverViewController.h"
#import "PublicationCell.h"
#import "PublicationFooter.h"
#import "AppDelegate.h"
#import "Edition+CoreDataClass.h"
#import "APConstants.h"

@interface PublicationPopOverViewController ()<UITextFieldDelegate, NSFetchedResultsControllerDelegate>{
        AppDelegate *appDelegate;
        PublicationFooter *footerCell;
        UITextField *textFields;
}

@property (nonatomic) BOOL isEditMode;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *addRowBarButtonItem;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *editBarButtonItem;

@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic, strong) NSMutableArray *editions;

//@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation PublicationPopOverViewController
//@synthesize fetchedResultsController = _fetchedResultsController;

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
//    NSLog(@"View did load  ===== %ld", [[[_fetchedResultsController sections] objectAtIndex:0] numberOfObjects]);
//    NSLog(@"View did load  ===== %ld", [[[_fetchedResultsController sections] objectAtIndex:0] numberOfObjects]);
    self.isEditMode = NO;
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Edition"];
    self.editions = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    [self.tableView reloadData];

    self.preferredContentSize = CGSizeMake(300, 40 + 44 * self.editions.count);
    [self showFooterNav];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidUnload {

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissMe {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)showFooterNav {
    if (self.isEditMode) {
        [self.addRowBarButtonItem setEnabled:NO];
        [self.addRowBarButtonItem setTitle:@"New"];
        [self.editBarButtonItem setTitle:@"Done"];
        [self setEditButtonToMode:YES];
    }
    else {
        [self.addRowBarButtonItem setEnabled:YES];
        [self.addRowBarButtonItem setTitle:@"New"];
        [self.editBarButtonItem setTitle:@"Edit"];
        [self setEditButtonToMode:NO];
    }
}

- (void)setEditButtonToMode:(BOOL) mode {
    if (mode == NO) {
        if ([self.editions count] == 1) {
            _editBarButtonItem.enabled = NO;
        } else {
            _editBarButtonItem.enabled = YES;
        }
        
    } else {
        _editBarButtonItem.enabled = YES;
    }
}

#pragma Actions

- (IBAction)onAddRow:(UIBarButtonItem *)sender {
    
    self.isEditMode = YES;
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *object = [NSEntityDescription insertNewObjectForEntityForName:@"Edition"
                                                            inManagedObjectContext:context];
    [object setValue:@"" forKey:@"name"];
    [object setValue:[NSNumber numberWithBool:NO] forKey:@"selected"];
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }

    [self.editions addObject:object];
    
    NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:self.editions.count - 1 inSection:0];
    [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self showFooterNav];
    self.preferredContentSize = CGSizeMake(300, 40 + 44 * self.editions.count);
}

- (IBAction)onEditRow:(UIBarButtonItem *)sender {
    
    NSString *title = sender.title;
    
    if ([title isEqualToString:@"Edit"]) {
        self.isEditMode = YES;
        [self.tableView setEditing:YES animated:YES];
        [self.addRowBarButtonItem setEnabled:NO];
        [self.addRowBarButtonItem setTitle:@"New"];
        [self.editBarButtonItem setTitle:@"Done"];
    } else if ([title isEqualToString:@"Done"]) {
        self.isEditMode = NO;
        
        if (textFields) {
            [textFields resignFirstResponder];
            textFields.enabled = NO;
        }
        
        if (self.isNormal) {
            NSLog(@"NORMAL________");
//            [self configureTextField:textFields];
            self.isEditMode = NO;
            [self.tableView setEditing:NO animated:NO];
            [self dismissMe];
        } else {
            NSLog(@"ELSE NORMAL________");
            [self.delegate moveBooks:@"All Editions"];
            [self dismissMe];
        }
    } else if ([title isEqualToString:@"Done "]) {
        self.isEditMode = NO;
        [self.tableView setEditing:NO animated:NO];
        [self showFooterNav];
    } else {
         self.isEditMode = NO;
        [self showFooterNav];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return appDelegate.publicationsArray.count;
    return self.editions.count;
//    id  sectionInfo =
//    [[_fetchedResultsController sections] objectAtIndex:section];
//    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PublicationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"publicationCell" forIndexPath:indexPath];
//    NSDictionary *dic = [appDelegate.publicationsArray objectAtIndex:indexPath.row];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(PublicationCell *)cell atIndexPath:(NSIndexPath *)indexPath {
//    Edition *dic = [_fetchedResultsController objectAtIndexPath:indexPath];

    Edition *dic = [self.editions objectAtIndex:indexPath.row];
    NSString *name = dic.name;//[dic objectForKey:@"name"];

    NSLog(@"NAME === %@", name);
    if (name.length == 0) {
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.publicationTextField.placeholder = @"Enter";
        [cell.publicationTextField becomeFirstResponder];
        cell.publicationTextField.delegate = self;
        textFields = cell.publicationTextField;
    }
    else {
        
        cell.publicationTextField.text = name;
        cell.publicationTextField.enabled = NO;
        
        BOOL val = dic.selected;//[[dic objectForKey:@"selected"] boolValue];
        if (val) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    cell.publicationTextField.tag = indexPath.row;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    footerCell = [tableView dequeueReusableCellWithIdentifier:@"footerCell"];
    [self showFooterNav];
    return footerCell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


#pragma mark -
#pragma mark Editing rows

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0)
        return UITableViewCellEditingStyleNone;
    return UITableViewCellEditingStyleDelete;
}


- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        [appDelegate.publicationsArray removeObjectAtIndex:indexPath.row];
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        NSManagedObjectContext *context = [self managedObjectContext];
        [context deleteObject:[self.editions objectAtIndex:indexPath.row]];
        
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"Can't Delete! %@ %@", error, [error localizedDescription]);
            return;
        } else {
            NSLog(@"DELETION DONE");
        }
        
        [self.editions removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        self.preferredContentSize = CGSizeMake(300, 40 + 44 * self.editions.count);
    }
}


#pragma mark - Table View delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isEditMode) {
//        if (indexPath.row == 0) {
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:ALL_EDITIONS_SELECTED];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            [self.delegate selectedAllEdition:YES];
//            [self dismissMe];
//        }
//        else {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:ALL_EDITIONS_SELECTED];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //        NSDictionary *dic = [appDelegate.publicationsArray objectAtIndex:indexPath.row];
            //        NSString *name = [dic objectForKey:@"name"];
            Edition *dic = [self.editions objectAtIndex:indexPath.row];
            NSString *name = dic.name;//[dic objectForKey:@"name"];
            
            if (name.length == 0) {
                PublicationCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                cell.publicationTextField.delegate = self;
                [cell.publicationTextField becomeFirstResponder];
            }
//        }
    } else {
//        if (indexPath.row == 0) {
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:ALL_EDITIONS_SELECTED];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            [self.delegate selectedAllEdition:YES];
//            [self dismissMe];
//        } else {
            //        for (NSMutableDictionary* dic in appDelegate.publicationsArray) {
            //            [dic setObject:[NSNumber numberWithBool:NO] forKey:@"selected"];
            //        }
            NSManagedObjectContext *context = [self managedObjectContext];
            for (Edition *ed in self.editions) {
                [ed setValue:[NSNumber numberWithBool:NO] forKey:@"selected"];
            }
            NSError *error = nil;
            // Save the object to persistent store
            if (![context save:&error]) {
                NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
            }
            //        NSMutableDictionary *dic = [appDelegate.publicationsArray objectAtIndex:indexPath.row];
            Edition *dic = [self.editions objectAtIndex:indexPath.row];
            [dic setValue:[NSNumber numberWithBool:YES] forKey:@"selected"];
            
            if (![context save:&error]) {
                NSLog(@"Failed to save - error: %@", [error localizedDescription]);
            }
            [self.tableView reloadData];
            
            if (self.isNormal) {
                [self.delegate categoryBooks:dic.name];//[dic objectForKey:@"name"]
            }
            else {
                [self.delegate moveBooks:dic.name];//
            }
            [self dismissMe];
        }
//    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Editing text fields

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self configureTextField:textField];
    [textField resignFirstResponder];
}

- (void)configureTextField:(UITextField *)textField {
    NSString *trimedString = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (trimedString.length == 0) {
        //        NSDictionary *dic = [appDelegate.publicationsArray objectAtIndex:appDelegate.publicationsArray.count - 1];
        //        [appDelegate.publicationsArray removeObject:dic];
        
        NSManagedObjectContext *context = [self managedObjectContext];
        NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:self.editions.count - 1 inSection:0];
        Edition *dic = [self.editions objectAtIndex:newIndexPath.row];
        [context deleteObject:dic];
        
        NSError *deleteError = nil;
        
        if (![context save:&deleteError]) {
            NSLog(@"Unable to save managed object context.");
            NSLog(@"%@, %@", deleteError, deleteError.localizedDescription);
        }
        [self.editions removeObject:dic];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        self.preferredContentSize = CGSizeMake(300, 40 + 44 * self.editions.count);
        [self dismissMe];
    }
    else {
        //        NSMutableDictionary *dic = [appDelegate.publicationsArray objectAtIndex:appDelegate.publicationsArray.count - 1];
        //        [dic setObject:textField.text forKey:@"name"];
        NSManagedObjectContext *context = [self managedObjectContext];
        NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:self.editions.count - 1 inSection:0];
        Edition *dic = [self.editions objectAtIndex:newIndexPath.row];
        [dic setValue:textField.text forKey:@"name"];
        
        NSError *error;
        if (![context save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
        [NSFetchedResultsController deleteCacheWithName:nil];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    //Limit custom category length to 20 character
    NSInteger oldLength = [textField.text length];
    NSInteger newLength = oldLength + [string length] - range.length;
    if(newLength > 20){
        return NO;
    }
    return YES;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
}
@end
