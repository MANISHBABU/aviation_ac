//
//  AviationPressLandingViewController.m
//  AviationPress
//
//  Created by Sonia Mane on 24/08/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import "AviationPressLandingViewController.h"
#import "PSCMetadataViewController.h"
#import "PSCInfoViewController.h"
#import "PSCIndexViewController.h"
#import "PSCTableOfContentsViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "NotificationsViewController.h"
#import "APConstants.h"
#import <PSPDFKit/PSPDFKit.h>
#import "AviationPressPSPDFOutlineViewController.h"
#import "AppContext.h"

#define PSCSettingsChangedNotification @"PSCSettingsChangedNotification"

#define BOXED(val) ({ typeof(val) _tmp_val = (val); [NSValue valueWithBytes:&(_tmp_val) objCType:@encode(typeof(val))]; })

@interface AviationPressLandingViewController () <TOCGotoPageNumber, IndexGotoPageNumber>

@property (nonatomic) UIBarButtonItem *metadataButtonItem;
@property (nonatomic) UIBarButtonItem *indexButtonItem;
@property (nonatomic) UIBarButtonItem *tocButtonItem;

@end

@implementation AviationPressLandingViewController

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navigateToNotificationsViewController:) name:NOTIF_DID_TAP_ON_NOTIFICATION object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // First, enable the sticky header:
    [SVProgressHUD dismiss];
    UICollectionViewLayout *layout = self.thumbnailController.collectionViewLayout;
    if ([layout isKindOfClass:PSPDFThumbnailFlowLayout.class]) {
        ((PSPDFThumbnailFlowLayout *)layout).stickyHeaderEnabled = YES;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSUserDefaults standardUserDefaults] setInteger:self.pageIndex forKey:@"currentPageNumber"];
    
    [[NSUserDefaults standardUserDefaults] setInteger:self.pageIndex forKey:self.bookEditionName];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"Saved page number = %lu", (unsigned long)self.pageIndex);
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIF_DID_TAP_ON_NOTIFICATION object:nil];
}

- (instancetype)initWithDocument:(PSPDFDocument *)document configuration:(PSPDFConfiguration *)configuration withBookEditionName:(NSString *) bookEditionName {
    static dispatch_once_t onceToken;

    if ((self = [super initWithDocument:document configuration:configuration])) {
        self.bookEditionName = bookEditionName;
        self.metadataButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"info"] style:UIBarButtonItemStylePlain target:self action:@selector(metadataButtonPressed:)];
        self.metadataButtonItem.accessibilityLabel = NSLocalizedString(@"Metadata", @"");
        
        self.indexButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"index"] style:UIBarButtonItemStylePlain target:self action:@selector(indexButtonPressed:)];
        
        if ([self.bookEditionName isEqualToString:EEPP_12_SAMPLE_EDITION_NAME]) {
            self.tocButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"book"] style:UIBarButtonItemStylePlain target:self action:@selector(tableOfContentsButtonPressed:)];
        } else {
            if (self.document.documentProviders.firstObject.outlineParser.isOutlineAvailable) {
                self.tocButtonItem = [[UIBarButtonItem alloc] initWithImage:[PSPDFKit imageNamed:@"outline"] style:UIBarButtonItemStylePlain target:self action:@selector(outlineButtonPressed:)];
            }
        }

        self.delegate = self;
        [self globalVarChanged];
        [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(globalVarChanged) name:PSCSettingsChangedNotification object:nil];
        [self updateLeftBarButtonItems];
        [self updateRightBarButtonItems];
        
        dispatch_once(&onceToken, ^{
            PSPDFCollectionReusableFilterView *headerAppearance = [PSPDFCollectionReusableFilterView appearanceWhenContainedInInstancesOfClasses:@[AviationPressLandingViewController.class]];
            
            headerAppearance.backgroundColor = [UIColor whiteColor];
            UIEdgeInsets filterMargin = UIEdgeInsetsZero;
            filterMargin.bottom = filterMargin.top = 2 * PSPDFCollectionReusableFilterViewDefaultMargin;
            headerAppearance.minimumFilterMargin = filterMargin;
            PSPDFThumbnailFilterSegmentedControl *thumbnailSegment = [PSPDFThumbnailFilterSegmentedControl appearance];
            [thumbnailSegment setTintColor:self.view.tintColor];            
        });
    }
    return self;
}

- (void) navigateToNotificationsViewController:(NSNotification *)nsnotification {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NotificationsViewController *notificationsViewController = [storyBoard instantiateViewControllerWithIdentifier:@"NotificationsViewController"];
    [self.navigationController pushViewController:notificationsViewController animated:YES];
}

#pragma mark - Metadata button pressed

- (void) metadataButtonPressed:(nullable id)sender {
    PSCInfoViewController *metadataController = [[PSCInfoViewController alloc] initWithDocument:self.pdfController.document withBookEditionName: self.bookEditionName];
    metadataController.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:metadataController options:@{PSPDFPresentationCloseButtonKey: @YES} animated:YES sender:sender completion:NULL];
}

- (void) outlineButtonPressed:(UIButton *)sender {
    PSPDFOutlineViewController *outlineController = [[PSPDFOutlineViewController alloc] initWithDocument:self.document];
    outlineController.title = @"Chapters";
    outlineController.searchController.searchBar.placeholder = @"Search Chapters";
    outlineController.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:outlineController options:@{PSPDFPresentationCloseButtonKey: @YES, PSPDFPresentationPopoverArrowDirectionsKey: @(UIPopoverArrowDirectionUp)}
                       animated:YES sender:sender completion:NULL];
}

//- (BOOL) outlineController:(PSPDFOutlineViewController *)outlineController didTapAtElement:(PSPDFOutlineElement *)outlineElement {
//
//    return NO;
//}

#pragma mark - Delegate methods

- (void)moveToPage:(NSInteger)page {
    [self dismissViewControllerOfClass:[PSCTableOfContentsViewController class] animated:NO
                            completion:^{
                                if ([self.bookEditionName isEqualToString:EEPP_12_SAMPLE_EDITION_NAME]) {
                                    NSLog(@"DO NOTHING - 12th Edition Sample");
                                } else {
                                    [self setPageIndex:page animated:NO];
                                }
                            }];
}

- (void)moveToIndexPage:(NSInteger)page {
    if ([self dismissViewControllerOfClass:[PSCIndexViewController class] animated:NO
                                completion:NULL] ) {
        if ([self.bookEditionName isEqualToString:EEPP_12_SAMPLE_EDITION_NAME]) {
            NSLog(@"DO NOTHING - 12th Edition Sample");
        } else {
            [self setPageIndex:page animated:NO];
        }
    }
    NSLog(@"--------Page number %ld   -------", (long)page);
}

- (void)tableOfContentsButtonPressed:(nullable id)sender {
    PSCTableOfContentsViewController *tocController = [[PSCTableOfContentsViewController alloc] initWithDocument:self.pdfController.document withBookEditionName: self.bookEditionName];
    tocController.chapters = [[[AppContext sharedInstance] getBookdataFor:self.bookEditionName] objectForKey:@"chapters"];
    tocController.delegate = self;
    tocController.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:tocController options:@{PSPDFPresentationCloseButtonKey: @YES} animated:YES sender:sender completion:NULL];
}

- (void)indexButtonPressed:(nullable id)sender {
    PSCIndexViewController *indexController = [[PSCIndexViewController alloc] initWithDocument:self.pdfController.document withBookEditionName: self.bookEditionName];
    indexController.indexes = [[[AppContext sharedInstance] getBookdataFor:self.bookEditionName] objectForKey:@"index"];
    indexController.delegate = self;
    indexController.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:indexController options:@{PSPDFPresentationCloseButtonKey: @YES} animated:YES sender:sender completion:NULL];
}

- (void)updateLeftBarButtonItems {
    self.navigationItem.leftItemsSupplementBackButton = YES;
    NSMutableArray *leftToolbarItems = [NSMutableArray array];
    if (self.tocButtonItem != nil)
    {
        [leftToolbarItems addObject:self.tocButtonItem];
    }
    if (self.indexButtonItem != nil)
    {
        [leftToolbarItems addObject:self.indexButtonItem];
    }
    if (self.metadataButtonItem != nil)
    {
        [leftToolbarItems addObject:self.metadataButtonItem];
    }
//
//    [leftToolbarItems addObject:self.indexButtonItem];
//    [leftToolbarItems addObject:self.metadataButtonItem];
    self.navigationItem.hidesBackButton = NO;
    [self.navigationItem setLeftBarButtonItems:leftToolbarItems forViewMode:PSPDFViewModeDocument animated:NO];
}

- (void)updateRightBarButtonItems {
    NSMutableArray *rightBarButtonItems = [NSMutableArray array];
    [rightBarButtonItems addObject:self.thumbnailsButtonItem];
    [rightBarButtonItems addObject:self.searchButtonItem];
    [rightBarButtonItems addObject:self.annotationButtonItem];
    [rightBarButtonItems addObject:self.documentEditorButtonItem];
    [rightBarButtonItems addObject:self.bookmarkButtonItem];
//    [rightBarButtonItems addObject:self.outlineButtonItem];


    [self.navigationItem setRightBarButtonItems:rightBarButtonItems forViewMode:PSPDFViewModeDocument animated:NO];
}

- (void) globalVarChanged {
    
    PSPDFViewState *viewState = self.isViewLoaded ? [self captureCurrentViewState] : nil;
    if (!viewState) {
//        NSUInteger pageIdx = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentPageNumber"];
        NSUInteger pageIdx = 0;
        if (self.bookEditionName) {
            pageIdx = [[NSUserDefaults standardUserDefaults] integerForKey:self.bookEditionName];
        }
        self.pageIndex = pageIdx;
        viewState = [[PSPDFViewState alloc] initWithPageIndex:self.pageIndex];
    }

    [self updateConfigurationWithBuilder:^(PSPDFConfigurationBuilder *builder) {
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        
        if (orientation == UIDeviceOrientationLandscapeLeft ||
            orientation == UIDeviceOrientationLandscapeRight ||
            orientation == UIInterfaceOrientationLandscapeLeft ||
            orientation == UIInterfaceOrientationLandscapeRight) {
            builder.pageMode = PSPDFPageModeDouble;
        } else {
            builder.pageMode = PSPDFPageModeSingle;
        }
    }];
    
    if (self.isViewLoaded) {
        [self applyViewState:viewState animateIfPossible:NO];
    }
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Menu Items

- (NSArray *)pdfViewController:(PSPDFViewController *)_pdfController shouldShowMenuItems:(NSArray *)menuItems atSuggestedTargetRect:(CGRect)rect forSelectedText:(NSString *)selectedText inRect:(CGRect)textRect onPageView:(PSPDFPageView *)pageView {
    
    NSMutableArray *newMenuItems = [menuItems mutableCopy];
    
    // add option to google for it.
    PSPDFMenuItem *googleItem = [[PSPDFMenuItem alloc] initWithTitle:NSLocalizedString(@"Google", nil) block:^{
        
        // trim removes stuff like \n or 's.
        NSString *trimmedSearchText = selectedText;
//        NSString *URLString = [NSString stringWithFormat:@"http://www.google.com/search?q=%@", [trimmedSearchText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
         NSString *URLString = [NSString stringWithFormat:@"http://www.google.com/search?q=%@", [trimmedSearchText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: URLString]];
        
        // create browser
        /*PSPDFWebViewController *browser = [[PSPDFWebViewController alloc] initWithURL:[NSURL URLWithString:URLString]];
        browser.delegate = _pdfController;
        browser.preferredContentSize = CGSizeMake(600, 500);
        browser.modalPresentationStyle = UIModalPresentationPopover;

        [_pdfController presentViewController:browser options:@{PSPDFPresentationCloseButtonKey: @YES, PSPDFPresentationRectKey: BOXED(rect)} animated:YES sender:nil completion:NULL]; */
        } identifier:@"Google"];
    for (PSPDFMenuItem *menuItem in menuItems) {
        if ([menuItem isKindOfClass:[PSPDFMenuItem class]] && [menuItem.identifier isEqualToString:@"Define"]) {
            [newMenuItems removeObjectAtIndex:5];
            break;
        }
    }
    [newMenuItems addObject:googleItem];
    return newMenuItems;
    
}

- (void)pdfDocument:(PSPDFDocument *)document didSaveAnnotations:(NSArray *)annotations {
    NSLog(@"---------- ANNOTATIONS SAVED -------------");
}

#pragma mark - Orientation Changes

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    // Code here will execute before the rotation begins.
    // Equivalent to placing it in the deprecated method -[willRotateToInterfaceOrientation:duration:]
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self updateConfigurationWithBuilder:^(PSPDFConfigurationBuilder *builder) {
            UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
            
            if (orientation == UIDeviceOrientationLandscapeLeft ||
                orientation == UIDeviceOrientationLandscapeRight ||
                orientation == UIInterfaceOrientationLandscapeLeft ||
                orientation == UIInterfaceOrientationLandscapeRight) {
                builder.pageMode = PSPDFPageModeDouble;
            } else {
                builder.pageMode = PSPDFPageModeSingle;
            }
        }];
        // Place code here to perform animations during the rotation.
        // You can pass nil or leave this block empty if not necessary.
        
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
        // Code here will execute after the rotation has finished.
        // Equivalent to placing it in the deprecated method -[didRotateFromInterfaceOrientation:]
        
    }];
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

@end
