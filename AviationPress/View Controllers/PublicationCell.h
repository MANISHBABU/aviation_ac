//
//  PublicationCell.h
//  AviationPress
//
//  Created by Sonia Mane on 03/11/16.
//  Copyright © 2016 Sonia Mane. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PublicationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *publicationTextField;

- (void)setEditing:(BOOL)editing animated:(BOOL)animated;

@end
