//
//  InAppPurchaseViewController.m
//  AviationPress
//
//  Created by Sonia Mane on 20/04/17.
//  Copyright © 2017 Aptara. All rights reserved.
//

#import "InAppPurchaseViewController.h"
#import "AviationPressIAPHelper.h"
#import "APConstants.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "BookshelfDatasource.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface InAppPurchaseViewController () {
    NSArray *_products;
    NSMutableArray <Book*> *booksArray;
    NSNumberFormatter * _priceFormatter;
}
@end

@implementation InAppPurchaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"In App Purchase";
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(apiCallToGetAllBook) forControlEvents:UIControlEventValueChanged];
    [self reload];
    [self.refreshControl beginRefreshing];
    
    _priceFormatter = [[NSNumberFormatter alloc] init];
    [_priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [_priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [self setBarButtonImage];
    [self apiCallToGetAllBook];
    [self reload];

}
-(void)setBarButtonImage
{
    UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    buyButton.frame = CGRectMake(0, 0, 96, 24);
    [buyButton setTitle:@" Restore" forState:UIControlStateNormal];
    [buyButton setImage:[UIImage imageNamed:@"restore"] forState:UIControlStateNormal];

    [buyButton addTarget:self action:@selector(restoreTapped:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:buyButton];
}
- (void)restoreTapped:(id)sender
{
    [[AviationPressIAPHelper sharedInstance] restoreCompletedTransactions];
    [self restoreBooksFromServer];
}
-(void)restoreBooksFromServer
{
    NSMutableDictionary *userIDDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[AviationPressIAPHelper getUSERID], @"UserID", nil];
    [AviationPressIAPHelper showLoaderWithText:@"Fetching books..."];
    [AviationPressIAPHelper performGetOnTarget:self forAPI:ADDPURCHASEDBOOK withParameter:userIDDic withCompletion:^(id responseObject)
     {
         [AviationPressIAPHelper hideLoaderText];
         if (![responseObject isKindOfClass:[NSError class]]) // Response
         {
             if ([responseObject isKindOfClass:[NSString  class]])
             {
                 NSString *messg = (NSString *)responseObject;
                 NSLog(@"Response of purchase books - %@", messg);
             }
             if ([responseObject isKindOfClass:[NSArray class]])
             {
                 NSArray *booksResp = (NSArray *)responseObject;
                 [SVProgressHUD showWithStatus:@"Restoring..."];
                 
                 __block dispatch_group_t group = dispatch_group_create();
                 for (NSDictionary *preBook in  booksResp)
                 {
                     Book *bookObj = [Book new];
                     bookObj.bookName = [NSString stringWithFormat:@"%@", [preBook valueForKey:@"BookName"]];
                     bookObj.bookEditionNo = [NSString stringWithFormat:@"%@", [preBook valueForKey:@"BookEditionNumber"]];
                     bookObj.coverPageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", [preBook valueForKey:@"BookCoverImagePath"]]];
                     bookObj.bookEditionNumber = [NSString stringWithFormat:@"%@", [preBook valueForKey:@"BookEditionNumber"]];
                     bookObj.bookId = [NSNumber numberWithInt:[[preBook valueForKey:@"BookId"] intValue]];
                     bookObj.bookPrice = [NSNumber numberWithLong:[[preBook valueForKey:@"BookPrice"] intValue]];
                     
                     [AviationPressIAPHelper setCoverPage:bookObj.coverPageURL.absoluteString :bookObj];
                     [AviationPressIAPHelper saveBookIDWithBookName:bookObj.bookName andBookID:bookObj.bookId.stringValue];
                     [AviationPressIAPHelper showLoaderWithText:@"Restoring Books"];

                     if([AviationPressIAPHelper checkIfBookPurchased:bookObj] == NO)
                     {
                         dispatch_group_enter(group);
                         [AviationPressIAPHelper downloadBook:bookObj withTarget:self withCompletion:^(id responseObject)
                          {
                              dispatch_group_leave(group);
                          } loaderText:@""];
                     }
                 }
                 // Option 1: Get a notification on a block that will be scheduled on the specified queue:
                 dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0),
                                       ^{
                                           
                                           dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(),
                                                          ^{
                                                              [AviationPressIAPHelper hideLoaderText];
                                                          });
                                       });
             }else
             {
             }
         }else //Error
         {
             //            NSError *error = (NSError *)responseObject;
             [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"No Books Found" andWithSubTitle:@"No books found for this user at this time" withCompletion:^(BOOL success)
              {
                  dispatch_async(dispatch_get_main_queue(), ^
                                 {
                                     
                                 });
              }];
         }
     } loaderText:@"Fetching books..."];
}
- (void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    self.navigationController.navigationBar.tintColor = nil;//[UIColor colorWithRed:(247.0f/255.0f) green:(247.0f/255.0f) blue:(247.0f/255.0f) alpha:1];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];

}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - InApp Purchase bought

- (void)productPurchased:(NSNotification *)notification {
    NSString * productIdentifier = notification.object;
    [_products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
            *stop = YES;
        }
    }];
}

- (void)reload {
    _products = nil;
    [[AviationPressIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products)
    {
        if (success) {
            _products = products;
        }
        [self.refreshControl endRefreshing];
    }];
}
-(void)apiCallToGetAllBook
{
    [self reload];
    if (booksArray == nil)
    {
        booksArray = [NSMutableArray array];
    }else
    {
        [booksArray removeAllObjects];
    }
    [AviationPressIAPHelper showLoaderWithText:@"Getting All Books"];
    [AviationPressIAPHelper performGetOnTarget:self forAPI:ALLBOOKSFROMSTORE withParameter:nil withCompletion:^(id responseObject)
     {
         [self.refreshControl endRefreshing];
         [AviationPressIAPHelper hideLoaderText];
         if (![responseObject isKindOfClass:[NSError class]]) // Response
         {
             for (NSDictionary *preBook in  (NSArray *)responseObject)
             {
                 Book *bookObj = [Book new];
                 bookObj.bookName = [NSString stringWithFormat:@"%@", [preBook valueForKey:@"BookName"]];
                 bookObj.bookEditionNo = [NSString stringWithFormat:@"%@", [preBook valueForKey:@"BookEditionNumber"]];
                 bookObj.coverPageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", [preBook valueForKey:@"BookCoverImagePath"]]];
                 bookObj.bookEditionNumber = [NSString stringWithFormat:@"%@", [preBook valueForKey:@"BookEditionNumber"]];
                 bookObj.bookId = [NSNumber numberWithInt:[[preBook valueForKey:@"BookId"] intValue]];
                 bookObj.bookPrice = [NSNumber numberWithLong:[[preBook valueForKey:@"BookPrice"] intValue]];
                 
                 [AviationPressIAPHelper setCoverPage:bookObj.coverPageURL.absoluteString :bookObj];
                 [AviationPressIAPHelper saveBookIDWithBookName:bookObj.bookName andBookID:bookObj.bookId.stringValue];
                 [booksArray addObject:bookObj];
             }
             dispatch_async(dispatch_get_main_queue(), ^{
                 [self.tableView reloadData];
             });
         }else //Error
         {
             NSError *error = (NSError *)responseObject;
             [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Error" andWithSubTitle:[NSString stringWithFormat:@"%@", error.localizedDescription]];
         }
         
     } loaderText:@"Getting All Books"];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return booksArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
//
//    SKProduct * product = (SKProduct *) _products[indexPath.row];
//    cell.textLabel.text = product.localizedTitle;
//    [_priceFormatter setLocale:product.priceLocale];
//    cell.detailTextLabel.text = [_priceFormatter stringFromNumber:product.price];
//
//    if ([product.productIdentifier isEqualToString:IAP_PRODUCT_EEPP_12] &&
//        [[AviationPressIAPHelper sharedInstance] productPurchased:product.productIdentifier]) {
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        cell.accessoryView = nil;
//    } else {
        //New Code begin
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
//  UITableViewCell *  cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];

    Book *bookObj = booksArray[indexPath.row];
    cell.textLabel.text = bookObj.bookName.localizedCapitalizedString;
    cell.detailTextLabel.text = [bookObj.bookPrice stringValue].localizedCapitalizedString;

        //New Code End
        UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        buyButton.frame = CGRectMake(0, 0, 75, 30);
//purchased
    if ([AviationPressIAPHelper checkIfBookPurchased:bookObj])
    {
        [buyButton setImage:[[UIImage imageNamed:@"purchased"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
        [buyButton addTarget:self action:@selector(buyButtonTappedPurchased:) forControlEvents:UIControlEventTouchUpInside];

    }else
    {
        [buyButton setImage:[[UIImage imageNamed:@"buybutton"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
        [buyButton addTarget:self action:@selector(buyButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

    }
        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:[AviationPressIAPHelper getCoverPage:bookObj]]
                           placeholderImage:[UIImage imageNamed:@"defualt_book"]];
        buyButton.tag = indexPath.row;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.accessoryView = buyButton;
//    }
    
    return cell;
}

#pragma MARK:- Purchase
- (void)buyButtonTapped:(id)sender
{
    //First allow discount coupon
    UIButton *buyButton = (UIButton *)sender;
    SKProduct *product = _products[0];
    NSLog(@"Buying %@...", product.productIdentifier);
    [[AviationPressIAPHelper sharedInstance] buyProduct:product];
    [self initiateCouponProcess:booksArray[buyButton.tag]];
}
- (void)buyButtonTappedPurchased:(id)sender
{
    //First allow discount coupon
    [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Purchased" andWithSubTitle:@"This book is already purchased." withCompletion:^(BOOL success)
    {
        
    }];
}

- (void)initiateCouponProcess:(Book *)bookObj
{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Discount Coupon"
                                                                              message: @"Please enter you discount coupon"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Discount Coupon";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];

    [alertController addAction:[UIAlertAction actionWithTitle:@"Proceed with coupon" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * namefield = textfields[0];
        NSLog(@"%@:%@",namefield.text,namefield.textColor);
        [self discountAPI:bookObj andCouponCode:namefield.text shouldProceedWithoutCoupon:NO];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Proceed without coupon" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
    {
        NSArray * textfields = alertController.textFields;
        UITextField * namefield = textfields[0];
        [self discountAPI:bookObj andCouponCode:namefield.text shouldProceedWithoutCoupon:YES];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
    {
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)discountAPI:(Book *)bookObj andCouponCode:(NSString *)coupon shouldProceedWithoutCoupon:(BOOL)withoutCoupon
{
    if (withoutCoupon)
    {
        //[self downloadBook:bookObj];
        [self purchasedBook:bookObj];
    }else
    {
        NSMutableDictionary *couponDicParam = [NSMutableDictionary dictionary];
        [couponDicParam setValue:coupon forKey:@"CouponCode"];
        [couponDicParam setValue:[AviationPressIAPHelper getUSERID] forKey:@"UserId"];
        [AviationPressIAPHelper showLoaderWithText:@"Processing discount..."];
        [AviationPressIAPHelper performPostonTarget:self forAPI:COUPONAPPLY withParameter:couponDicParam withCompletion:^(id responseObject, long statusCode)
        {
            [AviationPressIAPHelper hideLoaderText];
            if (![responseObject isKindOfClass:[NSError class]]) // Response
            {
                NSDictionary *respDic = (NSDictionary *)responseObject;
                if ([respDic.allKeys containsObject:@"Message"])
                {
                    NSString *responseMessage = [NSString stringWithFormat:@"%@", respDic[@"Message"]];
                    [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Invalid" andWithSubTitle:[NSString stringWithFormat:@"%@", responseMessage]];
                }else if([respDic.allKeys containsObject:@"Discount"])
                {
                    int dVal = [[respDic valueForKey:@"Discount"] intValue];
                    
                    [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Coupon Applied" andWithSubTitle:[NSString stringWithFormat:@"You have got %ld%% discount", (long)dVal] withCompletion:^(BOOL success)
                    {
                        float fract = dVal/100.0;
                        float finalPrice = bookObj.bookPrice.intValue * fract;
                        NSInteger finalPriceInt = bookObj.bookPrice.intValue - (NSInteger)finalPrice;
                        
                        bookObj.bookPrice = [NSNumber numberWithInteger:finalPriceInt];
                        [self purchasedBook:bookObj];
                    }];
                }
                else
                {
//                    [self downloadBook:bookObj];
                    [self purchasedBook:bookObj];
                }
            }else //Error
            {
                NSError *error = (NSError *)responseObject;

                [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Error" andWithSubTitle:[NSString stringWithFormat:@"%@", error.localizedDescription]];
            }

        } loaderText:@"Processing discount..."];
    }
}
-(void)purchasedBook:(Book *)bookObj
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];
    [paramDic setValue:bookObj.bookId.stringValue forKey:@"BookId"];
    [paramDic setValue:[AviationPressIAPHelper getUSERID] forKey:@"PurchaseBy"];
    [paramDic setValue:[NSDate date].description forKey:@"PurchaseDate"];
    [paramDic setValue:@"123445" forKey:@"TransactionId"];
    [paramDic setValue:[AviationPressIAPHelper getUSERID] forKey:@"IMIENo"];
    [AviationPressIAPHelper showLoaderWithText:@"Initiating purchase"];
    [AviationPressIAPHelper performPostonTarget:self forAPI:PURCHASEBOOK withParameter:paramDic withCompletion:^(id responseObject, long statusCode)
     {
         [AviationPressIAPHelper hideLoaderText];
         if (![responseObject isKindOfClass:[NSError class]]) // Response
         {
             [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Success" andWithSubTitle:@"Book has been purchased succesfully, now downloading the book..." withCompletion:^(BOOL success)
              {
                  [self downloadBook:bookObj];
              }];
         }else //Error
         {
             [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Failed" andWithSubTitle:@"Puurchasing Book at this time has failed" withCompletion:^(BOOL success)
              {
                  dispatch_async(dispatch_get_main_queue(), ^{
                      
                      
                  });
              }];
         }
     } loaderText:@"Initiating purchase"];
}
-(void)downloadBook:(Book *)bookObj
{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];
//        [paramDic setValue:@"1" forKey:@"bookId"]; //Working
//        [paramDic setValue:@"1" forKey:@"UserID"];
    [paramDic setValue:bookObj.bookId.stringValue forKey:@"bookId"];
    [paramDic setValue:[AviationPressIAPHelper getUSERID] forKey:@"UserID"];
    
    [AviationPressIAPHelper showLoaderWithText:@"Downloading..."];
    [AviationPressIAPHelper performGetOnTarget:self forAPI:DOWNLOADBOOK withParameter:paramDic withCompletion:^(id responseObject)
     {
         [AviationPressIAPHelper hideLoaderText];
         if (![responseObject isKindOfClass:[NSError class]]) // Response
         {
             if([responseObject isKindOfClass:[NSDictionary class]])
             {
                 NSDictionary *allDic = (NSDictionary *)responseObject;
                 if ([allDic.allKeys containsObject:@"Message"])
                 {
                     NSString *message = [allDic valueForKey:@"Message"];
                     if ([message isEqualToString:@"File not found"])
                     {
                         [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Failed" andWithSubTitle:message withCompletion:^(BOOL success)
                          {
                              dispatch_async(dispatch_get_main_queue(), ^
                                             {
                                             });
                          }];
                     }
                 }
                 
                 NSString *bookURL = [allDic valueForKey:@"BookURL"];
                 NSString *passphrase = [allDic valueForKey:@"PassPhrase"];
                 NSString *salt = [allDic valueForKey:@"Salt"];
                 NSString *plist = [allDic valueForKey:@"Plist"];
                 NSString *plistXMLFilePath = [allDic valueForKey:@"PlistXMLFilePath"];

                 
                 if ([plist isEqualToString:@"EEPP_13"])
                 {
                     bookObj.pListName = EEPP_13_PLIST;
                     bookObj.bookDesc = EEPP_13_PLIST;
                     bookObj.coverImageName = @"EEPP_13";
                 }else if ([plist isEqualToString:@"EEPP_12"])
                 {
                     bookObj.pListName = EEPP_12_PLIST;
                     bookObj.bookDesc = EEPP_12_PLIST;
                     bookObj.coverImageName = @"EEPP_12";
                 }else if ([plist isEqualToString:@"EEPP_11"])
                 {
                     bookObj.pListName = EEPP_11_PLIST;
                     bookObj.bookDesc = EEPP_11_PLIST;
                     bookObj.coverImageName = @"EEPP_11";
                 }
                 bookObj.passphrase = passphrase;
                 bookObj.salt = salt;
                 
                 [AviationPressIAPHelper setpassPhrase:passphrase :bookObj];
                 [AviationPressIAPHelper setSalt:salt :bookObj];
                 
                 //Download Code
                 [AviationPressIAPHelper showLoaderWithText:@"Downloading cover image..."];
                 [AviationPressIAPHelper downloadFileAndSaveItToDocDir:bookObj.coverPageURL.absoluteString withCompletion:^(NSURL *savedAtULR)
                 {
                     [AviationPressIAPHelper hideLoaderText];
                     [AviationPressIAPHelper setCoverPage:savedAtULR.absoluteString :bookObj]; //Saving the Cover Page
                     [AviationPressIAPHelper showLoaderWithText:@"Downloading resources..."];
                     [AviationPressIAPHelper downloadFileAndSaveItToDocDir:plistXMLFilePath withCompletion:^(NSURL *savedAtULR)
                      {
                          [AviationPressIAPHelper hideLoaderText];
                          //First Save document directory path to user defaults
                          [AviationPressIAPHelper setpListPath:savedAtULR.absoluteString :bookObj];
                          [AviationPressIAPHelper showLoaderWithText:@"Downloading book..."];
                          [AviationPressIAPHelper downloadFileAndSaveItToDocDir:[NSString stringWithFormat:@"%@", bookURL] withCompletion:^(NSURL *savedAtULR)
                           {
                               [AviationPressIAPHelper hideLoaderText];
                               if (savedAtULR != nil)
                               {
                                   bookObj.documentsURL = savedAtULR;
                                   [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Success" andWithSubTitle:@"Book has been purchased and downloaded succesfully" withCompletion:^(BOOL success)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [[BookshelfDatasource sharedInstance] saveProperBookToCoreData:bookObj];
                                            [AviationPressIAPHelper setBookPurchased:bookObj.bookId.stringValue :bookObj];
                                            [self GetBookAndStoreInDatabase];
                                        });
                                    }];
                               }else
                               {
                                   [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Failed" andWithSubTitle:@"Book download failed" withCompletion:^(BOOL success)
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^
                                                       {
                                                       });
                                    }];
                               }
                           } loaderText:@"Downloading book..."];
                      } loaderText:@"Downloading resources..."]; //Saving the pList File
                 } loaderText:@"Downloading cover image..."];
             }

         }else //Error
         {
             [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Failed" andWithSubTitle:@"Downloading Book at this time failed" withCompletion:^(BOOL success)
              {
                  dispatch_async(dispatch_get_main_queue(), ^{
                      
                      
                  });
              }];
         }
         
     } loaderText:@"Downloading..."];
}
-(void)GetBookAndStoreInDatabase
{
    NSMutableDictionary *userIDDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[AviationPressIAPHelper getUSERID], @"UserID", nil];
    [AviationPressIAPHelper showLoaderWithText:@"Loading..."];
    [AviationPressIAPHelper performGetOnTarget:self forAPI:ADDPURCHASEDBOOK withParameter:userIDDic withCompletion:^(id responseObject)
     {
         [AviationPressIAPHelper hideLoaderText];
         if (![responseObject isKindOfClass:[NSError class]]) // Response
         {
             if ([responseObject isKindOfClass:[NSString  class]])
             {
                 NSString *messg = (NSString *)responseObject;
                 NSLog(@"Response of purchase books - %@", messg);
             }
             if ([responseObject isKindOfClass:[NSArray class]])
             {
                 NSArray *booksResp = (NSArray *)responseObject;
                 for (NSDictionary *preBook in  booksResp)
                 {
                     Book *bookObj = [Book new];
                     bookObj.bookName = [NSString stringWithFormat:@"%@", [preBook valueForKey:@"BookName"]];
                     bookObj.bookEditionNo = [NSString stringWithFormat:@"%@", [preBook valueForKey:@"BookEditionNumber"]];
                     bookObj.coverPageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", [preBook valueForKey:@"BookCoverImagePath"]]];
                     bookObj.bookEditionNumber = [NSString stringWithFormat:@"%@", [preBook valueForKey:@"BookEditionNumber"]];
                     bookObj.bookId = [NSNumber numberWithInt:[[preBook valueForKey:@"BookId"] intValue]];
                     bookObj.bookPrice = [NSNumber numberWithLong:[[preBook valueForKey:@"BookPrice"] intValue]];
                     [AviationPressIAPHelper saveBookIDWithBookName:bookObj.bookName andBookID:bookObj.bookId.stringValue];

                     [AviationPressIAPHelper setBookPurchased:bookObj.bookId.stringValue :bookObj];
                 }
             }
         }else //Error
         {
             //            NSError *error = (NSError *)responseObject;
             [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"No Books Found" andWithSubTitle:@"No books found for this user at this time" withCompletion:^(BOOL success)
              {
                  dispatch_async(dispatch_get_main_queue(), ^{

                      
                  });
              }];
         }
     } loaderText:@"Loading..."];

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row < _products.count) {
        SKProduct * product = (SKProduct *) _products[indexPath.row];
        NSSet * consumableProductIdentifiers = [NSSet setWithObjects:
                                                IAP_PRODUCT_EEPP_12,
                                                nil];
        if ([consumableProductIdentifiers containsObject:product.productIdentifier]) {
            return;
        }
    }
}
@end
