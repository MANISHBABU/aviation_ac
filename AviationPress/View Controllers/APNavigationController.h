//
//  APNavigationController.h
//  AviationPress
//
//  Created by Sonia Mane on 30/01/17.
//  Copyright © 2017 Aptara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APNavigationController : UINavigationController

@end
