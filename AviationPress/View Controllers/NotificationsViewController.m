//
//  NotificationsViewController.m
//  AviationPress
//
//  Created by Sonia Mane on 24/01/17.
//  Copyright © 2017 Aptara. All rights reserved.
//

#import "NotificationsViewController.h"
#import "NotificationsTableViewCell.h"
#import "Notification+CoreDataProperties.h"
#import "NotificationManager.h"
#import "APConstants.h"

@interface NotificationsViewController () {
    NSDateFormatter *dateFormatter;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation NotificationsViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = nil;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"News and Announcements";
    self.tableView.estimatedRowHeight = 400.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM d, yyyy - h:mm a"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [self setBarButtonImage];
    self.notificationsArray = [[[NotificationManager sharedInstance] getAllNotifications] mutableCopy];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSavedNotifCount:) name:NOTIF_SAVED_TO_DB object:nil];
}
-(void)setBarButtonImage
{
    UIButton *clearAllButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    clearAllButton.frame = CGRectMake(0, 0, 96, 24);
    [clearAllButton setTitle:@" Clear All" forState:UIControlStateNormal];
    [clearAllButton setImage:[UIImage imageNamed:@"clean"] forState:UIControlStateNormal];
    
    [clearAllButton addTarget:self action:@selector(clearAllNotifications:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:clearAllButton];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)refreshSavedNotifCount:(NSNotification *)nsnotification {
    self.notificationsArray = [[[NotificationManager sharedInstance] getAllNotifications] mutableCopy];
    [self.tableView reloadData];
}
-(void)clearAllNotifications:(id)sender
{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Clear All Notifications"
                                 message:@"Are You Sure Want to Clear all the notifications and messages!"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    [[NotificationManager sharedInstance] deleteAllNotifications];
                                    [self.notificationsArray removeAllObjects];
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 1];
                                        [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
                                        [[UIApplication sharedApplication] cancelAllLocalNotifications];

                                        [self.tableView reloadData];
                                    });
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];

}
#pragma Mark UITableViewDelegate


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.notificationsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NotificationsTableViewCell *cell = (NotificationsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"notifCell" forIndexPath:indexPath];
    Notification *notif = [self.notificationsArray objectAtIndex:indexPath.row];
    cell.notificationMessage.text = [notif valueForKey:@"message"];
    cell.notifIcon.tintColor = self.view.tintColor;
    NSDate *date = [notif valueForKey:@"dateTime"];
    
    NSString *timestamp = [dateFormatter stringFromDate:date];
    
    cell.notificationTimeStamp.text = timestamp;
    [[NotificationManager sharedInstance] setNotificationAsRead:notif];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
