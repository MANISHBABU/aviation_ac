//
//  SearchViewController.h
//  AviationPress
//
//  Created by Sonia Mane on 02/11/16.
//  Copyright © 2016 Sonia Mane. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchViewControllerDelegate

- (void)findBooks:(NSString*)input;

@end

@interface SearchViewController : UIViewController

@property (nonatomic, weak, readwrite) id <SearchViewControllerDelegate> delegate;

- (void)dismissMe;

@end
