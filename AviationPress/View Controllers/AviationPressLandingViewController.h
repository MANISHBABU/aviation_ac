//
//  AviationPressLandingViewController.h
//  AviationPress
//
//  Created by Sonia Mane on 24/08/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PSPDFKit/PSPDFKit.h>

@interface AviationPressLandingViewController : PSPDFViewController <PSPDFViewControllerDelegate>
- (instancetype)initWithDocument:(PSPDFDocument *)document configuration:(PSPDFConfiguration *)configuration withBookEditionName:(NSString *) bookEditionName;
@property (nonatomic, copy) NSString *bookEditionName;
@end
