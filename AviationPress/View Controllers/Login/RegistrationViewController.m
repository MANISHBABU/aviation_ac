//
//  RegistrationViewController.m
//  AviationPress
//
//  Created by Bhalchandra on 17/11/18.
//  Copyright © 2018 Aptara. All rights reserved.
//

#import "RegistrationViewController.h"
#import "AviationPressIAPHelper.h"
#import "APConstants.h"
#import "AFNetworking.h"
#import "AFHTTPSessionManager.h"
#import <SafariServices/SafariServices.h>

@interface RegistrationViewController ()<UITextFieldDelegate, UITextViewDelegate, SFSafariViewControllerDelegate>
{
    
    IBOutlet UIButton *btnRegistration;
    IBOutlet UIView *whitePopUpView;
    
    IBOutlet UITextField *textFieldFirstName;
    IBOutlet UITextField *textFieldLastName;
    
    IBOutlet UITextField *textFieldEmailID;
    IBOutlet UITextField *textFieldMobileNumber;
    IBOutlet UITextField *purchaseDate;
    IBOutlet UITextField *purchaseAmount;
    IBOutlet UITextField *textFieldPassword;
    IBOutlet UITextField *textFieldReEnterPassword;
    IBOutlet UILabel *helpLabel;
    
    UIDatePicker *datePicker;
    IBOutlet UITextView *noteTextView;
}

@end

@implementation RegistrationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self preUI];
}
- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}
-(void)userTappedOnLink
{
    [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Help" andWithSubTitle:@"For any assistance please contact this email address: lengel414@yahoo.com " withCompletion:^(BOOL success) {
        
    }];
}
-(void)addLinkToTextView
{
    NSMutableAttributedString *mAttrStr = [[NSMutableAttributedString alloc] initWithString:@"Note: Please register your user and purchase information with us for receiving discount coupons on your next purchase with Aviation Press, LLC. We do not share or sell your registration information with any third party or use your information for other solicitation. If you choose not to register your information with us then we will not be able to send you any discount coupons for future purchases with Aviation Press, LLC. To locate the details of your previous purchase of this book, please follow this link to get your purchase history click here.(Purchase history - https://finance-app.itunes.apple.com/connecting-client?targetUrl=https%3A%2F%2Ffinance-app.itunes.apple.com%2Fpurchases )"];
    
    [noteTextView setAttributedText:mAttrStr];
    
    NSString *str = @"Note: Please register your user and purchase information with us for receiving discount coupons on your next purchase with Aviation Press, LLC. We do not share or sell your registration information with any third party or use your information for other solicitation. If you choose not to register your information with us then we will not be able to send you any discount coupons for future purchases with Aviation Press, LLC. To locate the details of your previous purchase of this book, please follow this link to get your purchase history click here.";
    NSMutableAttributedString *aStr = [[NSMutableAttributedString alloc]initWithString:str attributes:nil];
    [aStr addAttribute:NSLinkAttributeName value:@"https://finance-app.itunes.apple.com/connecting-client?targetUrl=https%3A%2F%2Ffinance-app.itunes.apple.com%2Fpurchases" range:[str rangeOfString:@"click here"]];
    [aStr addAttribute:NSLinkAttributeName value:@"https://finance-app.itunes.apple.com/connecting-client?targetUrl=https%3A%2F%2Ffinance-app.itunes.apple.com%2Fpurchases" range:[str rangeOfString:@"click here"]];
    [aStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14.0] range:[str rangeOfString:@"click here"]];
    
    [noteTextView setAttributedText:aStr];
    
}
-(void)preUI
{
    [self addLinkToTextView];
    [self instantiateDatePicker];
    purchaseAmount.delegate = self;
    
    //helpLabel
    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnLink)];
    // if labelView is not set userInteractionEnabled, you must do so
    [helpLabel setUserInteractionEnabled:YES];
    [helpLabel addGestureRecognizer:gesture];
    
    //Making View Curve
    [AviationPressIAPHelper makeViewRound:whitePopUpView roundVal:10.0];
    [AviationPressIAPHelper makeViewRound:btnRegistration roundVal:5.0];
    [AviationPressIAPHelper giveBorderToView:btnRegistration withColor:[UIColor whiteColor] andBorderWidth:1.0f];
    
//    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:strSomeTextWithLinks];

}

- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

-(BOOL)isAllDataValid
{
    BOOL isValid = YES;
    if (textFieldFirstName.text.length == 0)
    {
        //Show alert of blank text field
        [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Validation Failed" andWithSubTitle:@"Please enter fisrt name"];
        isValid = NO;
    }else if(textFieldLastName.text.length == 0)
    {
        //Show alert of blank text field
        [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Validation Failed" andWithSubTitle:@"Please enter last name"];

        isValid = NO;
    }else if(purchaseDate.text.length == 0)
    {
        //Show alert of blank text field
        [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Validation Failed" andWithSubTitle:@"Please select the purchase date"];
        
        isValid = NO;
    }else if(purchaseAmount.text.length == 0)
    {
        //Show alert of blank text field
        [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Validation Failed" andWithSubTitle:@"Please enter the purchase amount"];
        
        isValid = NO;
    }else if(textFieldEmailID.text.length == 0)
    {
        //Show alert of blank text field
        [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Validation Failed" andWithSubTitle:@"Please enter email id"];

        isValid = NO;
    }else if(textFieldReEnterPassword.text.length == 0)
    {
//        else if(textFieldMobileNumber.text.length == 0)
//        {
//            //Show alert of blank text field
//            //  [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Validation Failed" andWithSubTitle:@"Please enter mobile number"];
//            
//            // isValid = NO;
//        }
        //Show alert of blank text field
        [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Validation Failed" andWithSubTitle:@"Please enter password"];

        isValid = NO;
    }else if(textFieldPassword.text.length == 0)
    {
        //Show alert of blank text field
        [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Validation Failed" andWithSubTitle:@"Please enter password"];
        
        isValid = NO;
    }else if(textFieldReEnterPassword.text.length == 0)
    {
        //Show alert of blank text field
        [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Validation Failed" andWithSubTitle:@"Please enter confirm password"];

        isValid = NO;
    }else if(![AviationPressIAPHelper validateEmail:textFieldEmailID.text]) // Check Email Regex
    {
        //Show alert of Invalid Email Address
        [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Validation Failed" andWithSubTitle:@"Please check email id entered"];

        isValid = NO;
    }else if(purchaseDate.text.length == 0) // Check Email Regex
    {
        //Show alert of Invalid Email Address
        [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Validation Failed" andWithSubTitle:@"Please check the purchase date"];
        
        isValid = NO;
    }else if(purchaseAmount.text.length == 0) // Check Email Regex
    {
        //Show alert of Invalid Email Address
        [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Validation Failed" andWithSubTitle:@"Please check the purchase amount"];
        
        isValid = NO;
    }else if(![textFieldReEnterPassword.text isEqualToString:textFieldReEnterPassword.text]) // Check Same Password Validation
    {
        //Show alert of Mismatch Password
        [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Validation Failed" andWithSubTitle:@"Your entered password mismatch"];

        isValid = NO;
    }
    
    return isValid;
}

-(NSMutableDictionary *)registrationPayload
{
    NSMutableDictionary *allData = [NSMutableDictionary dictionary];
    [allData setValue:textFieldFirstName.text forKey:@"FirstName"];
    [allData setValue:textFieldLastName.text forKey:@"LastName"];
    [allData setValue:textFieldEmailID.text forKey:@"EmailId"];
//    [allData setValue:textFieldMobileNumber.text forKey:@"MobileNumber"];
    [allData setValue:@"" forKey:@"MobileNumber"];
    [allData setValue:textFieldPassword.text forKey:@"Password"];
    [allData setValue:purchaseAmount.text forKey:@"PurchaseAmount"];
    [allData setValue:purchaseDate.text forKey:@"purchaseDate"];
    
    return  allData;
}

-(void)apiCallToRegister
{

   // return;
    [AviationPressIAPHelper showLoaderWithText:@"Registering"];
    [AviationPressIAPHelper performPostonTarget:self forAPI:REGISTERAPI withParameter:[self registrationPayload] withCompletion:^(id responseObject, long statusCode)
     {
         [AviationPressIAPHelper hideLoaderText];
         if (![responseObject isKindOfClass:[NSError class]]) // Response
         {
             NSDictionary *respDic = (NSDictionary *)responseObject;
             if([respDic.allKeys containsObject:@"Message"])
             {
                 NSString *messg = [respDic valueForKey:@"Message"];
                 
                 [AviationPressIAPHelper showAlertOnVC:self withTitleText:messg andWithSubTitle:messg withCompletion:^(BOOL success)
                 {
                     [self.navigationController popViewControllerAnimated:YES];
                 }];
                 
             }else
             {
                 [AviationPressIAPHelper setUserDetails:[self registrationPayload]];
                 [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Registered" andWithSubTitle:@"You are successfully register, now you can login" withCompletion:^(BOOL success)
                 {
                     [self.navigationController popViewControllerAnimated:YES];
                 }];
             }
         }else //Error
         {
                 NSError *error = (NSError *)responseObject;
                 [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Error" andWithSubTitle:[NSString stringWithFormat:@"%@", error.localizedDescription]];
         }
     } loaderText:@"Registering"];
}
-(void)instantiateDatePicker
{
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    [datePicker addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    purchaseDate.inputView = datePicker;

    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(ShowSelectedDate)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
    
    [purchaseDate setInputAccessoryView:toolBar];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
//    [comps setYear:30];
//    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    [comps setYear:-5];
    NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    
    [datePicker setMaximumDate:currentDate];
    [datePicker setMinimumDate:minDate];


}
-(void)ShowSelectedDate
{
    NSDateFormatter * df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MM-dd-yyyy"];
    purchaseDate.text = [df stringFromDate:datePicker.date];

    [self.view endEditing:YES];
}
- (void)onDatePickerValueChanged:(UIDatePicker *)datePicker
{
    NSDateFormatter * df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MM-dd-yyyy"];
    purchaseDate.text = [df stringFromDate:datePicker.date];
}

#pragma mark - IBAction
- (IBAction)registrationButtonPressed:(UIButton *)sender
{
    [self.view endEditing:YES];
    if ([self isAllDataValid]) //if all data is valid Registration login process
    {
        [self apiCallToRegister];
    }
}
- (IBAction)backButtonPressed:(UIButton *)sender
{
    if (self.navigationController != nil)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark:- UITextView Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField isEqual:purchaseAmount])
    {
        NSString *regEx = @"0123456789.";
//        NSPredicate *amountText = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regEx];
        //Valid email address
        NSString* val = [[textField text] stringByReplacingCharactersInRange:range withString:string];

        NSCharacterSet *allowedCharacterSet = [NSCharacterSet characterSetWithCharactersInString:regEx];
        if ([[string componentsSeparatedByCharactersInSet:[allowedCharacterSet invertedSet]] count] > 1 || [val length] > 5)
        {
            return NO;
        }
        return YES;
        
    }
    
    return YES;
}

#pragma Mark:- UITextView Delegate
- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange
{
    SFSafariViewController *websiteToOpen = [[SFSafariViewController alloc]initWithURL:URL entersReaderIfAvailable:YES];
    websiteToOpen.delegate = self;
    [self presentViewController:websiteToOpen animated:YES completion:nil];
    return YES;
}
- (void)safariViewControllerDidFinish:(SFSafariViewController *)controller {
    [self dismissViewControllerAnimated:true completion:nil];
}

@end
