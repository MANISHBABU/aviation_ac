//
//  LoginViewController.m
//  AviationPress
//
//  Created by Bhalchandra on 17/11/18.
//  Copyright © 2018 Aptara. All rights reserved.
//

#import "LoginViewController.h"
#import "AviationPressIAPHelper.h"
#import "APConstants.h"
#import "BookshelfDatasource.h"
#import "SVProgressHUD.h"

@interface LoginViewController ()
{
    IBOutlet UITextField *textFieldEmail;
    IBOutlet UITextField *textFieldPassword;
    IBOutlet UIButton *btnLogin;
    IBOutlet UIButton *btnRegister;
    IBOutlet UIView *whitePopupView;
    IBOutlet UIImageView *checkBoxImageView;
    BOOL isChecked;
    IBOutlet UILabel *forgotPasswordLabel;
}

@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self preUI];
    [self addingTapGesture];
}
- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

-(void)preUI
{
    isChecked = NO;
    // Setting Icons in the text field
    [AviationPressIAPHelper addImageViewOnTextFielf:textFieldEmail withColor:[UIImage imageNamed:@"usernameiconview"] isRight:NO];
    [AviationPressIAPHelper addImageViewOnTextFielf:textFieldPassword withColor:[UIImage imageNamed:@"passwordiconview"] isRight:NO];
    
    //Making View round
    [AviationPressIAPHelper makeViewRound:whitePopupView roundVal:10.0];
    [AviationPressIAPHelper makeViewRound:btnLogin roundVal:5.0];
    [AviationPressIAPHelper makeViewRound:btnRegister roundVal:5.0];
    
    // Giving Border's to the view
    [AviationPressIAPHelper giveBorderToView:btnLogin withColor:[UIColor whiteColor] andBorderWidth:1.0f];
    [AviationPressIAPHelper giveBorderToView:btnRegister withColor:[UIColor whiteColor] andBorderWidth:1.0f];
    
    // Adding Line to the view
    [AviationPressIAPHelper addLineToView:textFieldEmail atPosition:LINE_POSITION_BOTTOM withColor:[UIColor whiteColor] lineWitdh:1.0f];
    [AviationPressIAPHelper addLineToView:textFieldPassword atPosition:LINE_POSITION_BOTTOM withColor:[UIColor whiteColor] lineWitdh:1.0f];
    

#ifndef NDEBUG
    /* Debug only code */
//    textFieldEmail.text = @"manish21@gmail.com";
//    textFieldPassword.text = @"123";
#endif

}

-(void)addingTapGesture
{
    checkBoxImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture:)];
    tapGesture1.numberOfTapsRequired = 1;
    [checkBoxImageView addGestureRecognizer:tapGesture1];
    
    forgotPasswordLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture2:)];
    tapGesture2.numberOfTapsRequired = 1;
    [forgotPasswordLabel addGestureRecognizer:tapGesture2];


}
- (void) tapGesture: (id)sender
{
    //handle Tap...
    if (!isChecked)
    {
        isChecked = YES;
    }else
    {
        isChecked = NO;
    }
    checkBoxImageView.image = isChecked == YES ? [UIImage imageNamed:@"checkedimg"] : [UIImage imageNamed:@"uncheckedimg"];
}
- (void) tapGesture2: (id)sender
{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Forgot Password"
                                                                              message: @"Please enter your registered email address"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Email address";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];

    [alertController addAction:[UIAlertAction actionWithTitle:@"Submit" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
    {
        NSArray * textfields = alertController.textFields;
        UITextField * namefield = textfields[0];
        NSLog(@"%@",namefield.text);
        if (![AviationPressIAPHelper validateEmail:namefield.text]) // Check Email Regex
        {
            [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Validation failed" andWithSubTitle:@"Email entered is incorrect." withCompletion:^(BOOL success)
            {
                
            }];
        }else
        {
            [AviationPressIAPHelper showLoaderWithText:@"Forgot password request..."];
            [AviationPressIAPHelper performPostonTarget:self forAPI:RESETPASSWORD withParameter:[NSMutableDictionary dictionaryWithObject:namefield.text forKey:@"email"] withCompletion:^(id responseObject, long statusCode)
             {
                 [AviationPressIAPHelper hideLoaderText];

                 [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Success" andWithSubTitle:@"Please check your email for reset password further process" withCompletion:^(BOOL success) {
                 }];
             } loaderText:@"Forgot password request..."];

        }
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                {
                                }]];

    [self presentViewController:alertController animated:YES completion:nil];

}

-(BOOL)isAllDataValid
{
    [self.view endEditing:YES];

    BOOL isValid = YES;
    if (textFieldEmail.text.length == 0)
    {
        //Show alert of blank text field
        [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Validation Failed" andWithSubTitle:@"Please enter email id"];

        isValid = NO;
    }else if(textFieldPassword.text.length == 0)
    {
        //Show alert of blank text field
        [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Validation Failed" andWithSubTitle:@"Please enter password"];

        isValid = NO;
    }else if(![AviationPressIAPHelper validateEmail:textFieldEmail.text]) // Check Email Regex
    {
        //Show alert of blank text field
        [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Validation Failed" andWithSubTitle:@"Please check email id entered"];

        isValid = NO;
    }
    
    return isValid;
}

-(NSMutableDictionary *)loginPayload
{
    NSMutableDictionary *allData = [NSMutableDictionary dictionary];
    [allData setValue:textFieldEmail.text forKey:@"EmailId"];
    [allData setValue:textFieldPassword.text forKey:@"Password"];
    return  allData;
}
-(void)clearDocumentDirectory
{
    NSString *folderPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSError *error = nil;
    [[BookshelfDatasource sharedInstance] deleteAllEntities:@""];
    for (NSString *file in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderPath error:&error])
    {
        if ([[file pathExtension] isEqualToString:@"aes"] || [[file pathExtension] isEqualToString:@"pdf"])
        {
            //This is Encrypted File with .aes Extension
            [[NSFileManager defaultManager] removeItemAtPath:[folderPath stringByAppendingPathComponent:file] error:&error];
        }
    }
}
-(void)apiCallToLogin
{
    [self clearDocumentDirectory];
    [AviationPressIAPHelper showLoaderWithText:@"Login..."];
    [AviationPressIAPHelper performPostonTarget:self forAPI:LOGINAPI withParameter:[self loginPayload] withCompletion:^(id responseObject, long statusCode)
    {
        [AviationPressIAPHelper hideLoaderText];
        if (![responseObject isKindOfClass:[NSError class]]) // Response
        {
            NSDictionary *respDic = (NSDictionary *)responseObject;
            if([respDic.allKeys containsObject:@"Message"])
            {
                NSString *messg = [respDic valueForKey:@"Message"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                        [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"No user found" andWithSubTitle:messg withCompletion:^(BOOL success) {
                            
                        }];
                    });
            }else
            {
                NSDictionary *jwtTokenDic = (NSDictionary *)responseObject;
                NSString *jwtToken = [NSString stringWithFormat:@"%@", jwtTokenDic[@"Token"]];
                NSString *userID = [NSString stringWithFormat:@"%@", jwtTokenDic[@"UserId"]];
                [AviationPressIAPHelper setJWTToken:jwtToken]; // Saving JWT Token
                [AviationPressIAPHelper setUSERID:userID];
                if (isChecked)
                {
                    [AviationPressIAPHelper saveAutoLoginOnRememberMePressed:YES];  // Saving Auto Login BOOL
                }
                // Get All the Books for the user before loading the Landing Page
                [self apiCallToGetPurchasedBook];
            }
        }else //Error
        {
            NSError *error = (NSError *)responseObject;
            [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"Error" andWithSubTitle:[NSString stringWithFormat:@"%@", error.localizedDescription]];
        }
        
    } loaderText:@"Login..."];
}
-(void)apiCallToGetPurchasedBook
{
    NSMutableDictionary *userIDDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[AviationPressIAPHelper getUSERID], @"UserID", nil];
    [AviationPressIAPHelper showLoaderWithText:@"Fetching books..."];
    [AviationPressIAPHelper performGetOnTarget:self forAPI:ADDPURCHASEDBOOK withParameter:userIDDic withCompletion:^(id responseObject)
    {
        [AviationPressIAPHelper hideLoaderText];
        if (![responseObject isKindOfClass:[NSError class]]) // Response
        {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            UINavigationController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"BookshelfNavigationViewController"];
            rootViewController.navigationBar.tintColor = nil;//[UIColor colorWithRed:(247.0f/255.0f) green:(247.0f/255.0f) blue:(247.0f/255.0f) alpha:1];;;
            rootViewController.navigationBar.backIndicatorImage = nil;
            if ([responseObject isKindOfClass:[NSString  class]])
            {
                NSString *messg = (NSString *)responseObject;
                NSLog(@"Response of purchase books - %@", messg);
            }
            if ([responseObject isKindOfClass:[NSArray class]])
            {
                NSArray *booksResp = (NSArray *)responseObject;
                [SVProgressHUD showWithStatus:@"Restoring..."];

                __block dispatch_group_t group = dispatch_group_create();
                for (NSDictionary *preBook in  booksResp)
                {
                    Book *bookObj = [Book new];
                    bookObj.bookName = [NSString stringWithFormat:@"%@", [preBook valueForKey:@"BookName"]];
                    bookObj.bookEditionNo = [NSString stringWithFormat:@"%@", [preBook valueForKey:@"BookEditionNumber"]];
                    bookObj.coverPageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", [preBook valueForKey:@"BookCoverImagePath"]]];
                    bookObj.bookEditionNumber = [NSString stringWithFormat:@"%@", [preBook valueForKey:@"BookEditionNumber"]];
                    bookObj.bookId = [NSNumber numberWithInt:[[preBook valueForKey:@"BookId"] intValue]];
                    bookObj.bookPrice = [NSNumber numberWithLong:[[preBook valueForKey:@"BookPrice"] intValue]];
                    
                    [AviationPressIAPHelper setCoverPage:bookObj.coverPageURL.absoluteString :bookObj];
                    [AviationPressIAPHelper saveBookIDWithBookName:bookObj.bookName andBookID:bookObj.bookId.stringValue];
                    dispatch_group_enter(group);
                    [AviationPressIAPHelper showLoaderWithText:@"Restoring Books"];
                    [AviationPressIAPHelper downloadBook:bookObj withTarget:self withCompletion:^(id responseObject)
                     {
                         dispatch_group_leave(group);
                     } loaderText:@""];
                }
                // Option 1: Get a notification on a block that will be scheduled on the specified queue:
                dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0),
                ^{
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(),
                                    ^{
                                        [AviationPressIAPHelper hideLoaderText];
                                        [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
                                    });
                });
            }else
            {
                [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
            }
        }else //Error
        {
//            NSError *error = (NSError *)responseObject;
            [AviationPressIAPHelper showAlertOnVC:self withTitleText:@"No Books Found" andWithSubTitle:@"No books found for this user at this time" withCompletion:^(BOOL success)
            {
                dispatch_async(dispatch_get_main_queue(), ^
                {
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UINavigationController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"BookshelfNavigationViewController"];
                    rootViewController.navigationBar.tintColor = nil;//[UIColor colorWithRed:(247.0f/255.0f) green:(247.0f/255.0f) blue:(247.0f/255.0f) alpha:1];;
                    rootViewController.navigationBar.backIndicatorImage = nil;
                    [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];

                });
            }];
        }
    } loaderText:@"Fetching books..."];
}
#pragma mark  - IBAction
- (IBAction)loginButtonPressed:(UIButton *)sender
{
    [self.view endEditing:YES];
    if ([self isAllDataValid]) //if all data is valid perform login process
    {
        [self apiCallToLogin];
    }
}
- (IBAction)registerButtonPressed:(UIButton *)sender
{
    
}

@end
