//
//  APCircularLabel.m
//  AviationPress
//
//  Created by Sonia Mane on 24/01/17.
//  Copyright © 2017 Aptara. All rights reserved.
//

#import "APCircularLabel.h"

@implementation APCircularLabel

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setCustomApperance];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if(self) {
        [self setCustomApperance];
    }
    return self;
}

-(void) setCustomApperance {
    self.clipsToBounds = YES;
    self.layer.cornerRadius = self.frame.size.width/2.0f;
    self.layer.allowsEdgeAntialiasing = YES;
    self.layer.contentsScale = [[UIScreen mainScreen] scale];
}
@end
