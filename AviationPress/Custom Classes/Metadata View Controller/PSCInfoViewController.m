//
//  PSCInfoViewController.m
//  AviationPress
//
//  Created by Sonia Mane on 25/08/16.
//  Copyright (c) 2016 Aptara. All rights reserved.
//

#import "PSCInfoViewController.h"
#import "AppContext.h"

@interface PSCInfoViewController ()
@property (nonatomic, strong) NSDictionary *indexes;
@end

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-designated-initializers"

@implementation PSCInfoViewController

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Lifecycle

- (instancetype)initWithDocument:(nullable PSPDFDocument *)document withBookEditionName:(NSString *) bookEditionName {
    if ((self = [super initWithStyle:UITableViewStylePlain])) {
        _document = document;
        self.indexes = [[[AppContext sharedInstance] getBookdataFor:bookEditionName] objectForKey:@"metadata"];
       
        self.title = @"Everything Explained";
        self.preferredContentSize = CGSizeMake(350.f, (self.indexes.count) * 44.f);
        self.tableView.userInteractionEnabled = NO;
        
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableViewDataSource

- (NSString *)metadataForRow:(NSUInteger)row {
    NSArray *sortedKeys = [[self.indexes allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    return self.indexes[sortedKeys[row]];
}

- (NSString *)metadataKeyForRow:(NSUInteger)row {
    NSArray *sortedKeys = [[self.indexes allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    return [sortedKeys[row] substringFromIndex:3];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.indexes.count > 0 ? 1 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.indexes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"PSCMetadataCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    cell.textLabel.font = [UIFont systemFontOfSize:17.0];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:16.0];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = [self metadataKeyForRow:indexPath.row];
    cell.detailTextLabel.text = [self metadataForRow:indexPath.row];
    return cell;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView*)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath*)indexPath withSender:(id)sender {
    if (action == @selector(copy:)) {
        [UIPasteboard generalPasteboard].string = [self metadataForRow:indexPath.row] ?: @"";
    }
}

- (BOOL)tableView:(UITableView*)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath*)indexPath withSender:(id)sender {
    return action == @selector(copy:);
}

- (BOOL)tableView:(UITableView*)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath*)indexPath {
    return YES;
}

@end

#pragma clang diagnostic pop
