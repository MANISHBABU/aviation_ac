//
//  PSCInfoViewController.h
//  AviationPress
//
//  Created by Sonia Mane on 25/08/16.
//  Copyright (c) 2016 Aptara. All rights reserved.
//

#import <UIKit/UIKit.h>

@import PSPDFKit;

NS_ASSUME_NONNULL_BEGIN

@interface PSCInfoViewController : UITableViewController

- (instancetype)initWithDocument:(nullable PSPDFDocument *)document withBookEditionName:(NSString *) bookEditionName;

NS_DESIGNATED_INITIALIZER;

@property (nonatomic, copy) NSString *bookEdiName;
@property (nonatomic, nullable) PSPDFDocument *document;

@end

NS_ASSUME_NONNULL_END
