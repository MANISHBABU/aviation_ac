//
//  BookhelfDatasource.m
//  AviationPress
//
//  Created by Sonia Mane on 06/12/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import "BookshelfDatasource.h"
#import "Book.h"
#import <PSPDFKit/PSPDFKit.h>
#import "PSCFileHelper.h"
#import "AviationPressLandingViewController.h"
#import "APConstants.h"
#import "AviationBook+CoreDataClass.h"
#import "Edition+CoreDataClass.h"
#import "AviationPressIAPHelper.h"

NSString *kEditionCellID = @"EditionCell";

static NSMutableArray *allEEPPEditions;

@interface BookshelfDatasource ()
@property (nonatomic, strong) NSMutableArray *allEEPPEditions;
@property (nonatomic, strong) CKContainer *privateContainer;
@property (nonatomic, strong) CKDatabase *privateDatabase;
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;

@end

@implementation BookshelfDatasource

+ (instancetype)sharedInstance {
    static BookshelfDatasource *_sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[BookshelfDatasource alloc] init];
    });
    return _sharedInstance;
}

- (instancetype)init {
    if (self = [super init]) {
        self.privateContainer = [CKContainer defaultContainer];
        self.privateDatabase = self.privateContainer.privateCloudDatabase;
    }
    return self;
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}
- (void)deleteAllEntities:(NSString *)nameEntity
{
    if ([nameEntity isEqualToString:@""])
    {
        nameEntity = @"AviationBook";
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:nameEntity];
    [fetchRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];

    NSError *error;
    NSArray *fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject *object in fetchedObjects)
    {
        [managedObjectContext deleteObject:object];
    }
    
    error = nil;
    [managedObjectContext save:&error];
}

- (void)  fetchEditionsFromLocalStorageWithCompletionBlock:(allEditions)editions {
    if (!self.allEEPPEditions) {
        self.allEEPPEditions = [NSMutableArray array];
    }
    [self.allEEPPEditions removeAllObjects];
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"AviationBook"];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"bookEditionNumber" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *aviationBooks = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];

    for (AviationBook *ab in aviationBooks) {
        Book *book = [[Book alloc] init];
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.bookName contains[cd] %@",book.bookName];
         NSArray   *filteredArray = [aviationBooks filteredArrayUsingPredicate:bPredicate];

        if (filteredArray.count == 0)
        {
            NSLog(@"SONIAS CHECK  *** GET SAVED encryptedPDFURL - %@", [NSURL URLWithString:ab.bookDocumentsURL]);
            NSLog(@"SONIAS CHECK  *** GET SAVED coverPageURL - %@", ab.bookCoverImageName);
            
            NSString *docsFolder = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
            NSString *newPath = [docsFolder stringByAppendingPathComponent:(NSString *)ab.bookDocumentsURL];
            
            book.documentsURL = [NSURL fileURLWithPath:newPath];//[NSURL URLWithString:ab.bookDocumentsURL];
            
            book.coverPageURL = [[NSBundle bundleForClass:[self class]] URLForResource:ab.bookCoverImageName withExtension:@"png"];
            
            book.bookDesc = ab.bookEditionNumber;
            book.bookName = ab.bookName;
            [self.allEEPPEditions addObject:book];

        }
    }
    editions(self.allEEPPEditions, nil);
}

- (void) fetchAllEditionsFromiCloudWithCompletionBlock:(allEditions) editions withError: (cloudError) cloudError {
    dispatch_group_t cloudGroup = dispatch_group_create();
    
    __block Book *book_11th_Edi;
    __block Book *book_12th_Edi;
    __block Book *book_12th_Sample_Edi;
    
    dispatch_group_enter(cloudGroup);
    [self fetch_11th_EditionFromiCloudWithCompletionBlock:^(Book *fetchedBook) {
        book_11th_Edi = fetchedBook;
        dispatch_group_leave(cloudGroup);
    } withError:^(NSError *error) {
//        cloudError(error);
        dispatch_group_leave(cloudGroup);
    }];
    
    dispatch_group_enter(cloudGroup);
    [self fetch_12th_Sample_EditionFromLocalStorageWithCompletionBlock:^(Book *fetchedBook) {
        book_12th_Sample_Edi = fetchedBook;
        dispatch_group_leave(cloudGroup);
    } withError:^(NSError *error) {
//        cloudError(error);
        dispatch_group_leave(cloudGroup);
    }];
    
    BOOL productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:IAP_PRODUCT_EEPP_12];
    if (productPurchased) {
        dispatch_group_enter(cloudGroup);
        [self save_12th_EditionToiCloudAndLocallyIfDoesNotExistWithCompletionBlock:^(Book *fetchedBook) {
            book_12th_Edi = fetchedBook;
            dispatch_group_leave(cloudGroup);
        } withError:^(NSError *error) {
//            cloudError(error);
            dispatch_group_leave(cloudGroup);
        }];
    }
    
    dispatch_group_notify(cloudGroup, dispatch_get_main_queue(), ^{
        NSMutableArray *cloudEditions = [NSMutableArray array];
        if (book_11th_Edi) {
            [cloudEditions addObject:book_11th_Edi];
        }
        if (book_12th_Edi) {
            [cloudEditions addObject:book_12th_Edi];
        }
        if (book_12th_Sample_Edi) {
            [cloudEditions addObject:book_12th_Sample_Edi];
        }
        
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"bookDesc" ascending:YES];
        [cloudEditions sortUsingDescriptors:@[sortDescriptor]];
        editions(cloudEditions, nil);
    });
}

- (void) fetch_12th_Sample_EditionFromLocalStorageWithCompletionBlock:(bookViewModel) fetchedBook withError: (cloudError) cloudError {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"AviationBook"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bookEditionNumber == %@", EEPP_12_SAMPLE_EDITION_NAME];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *aviationBooks = [[managedObjectContext executeFetchRequest:fetchRequest error:&error] mutableCopy];
    
    if (aviationBooks.count > 0) {
        Book *book;
        for (AviationBook *ab in aviationBooks) {
            book = [[Book alloc] init];
            NSLog(@"SONIAS CHECK  *** GET SAVED encryptedPDFURL - %@", [NSURL URLWithString:ab.bookDocumentsURL]);
            NSLog(@"SONIAS CHECK  *** GET SAVED coverPageURL - %@", ab.bookCoverImageName);
            
            NSString *docsFolder = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
            NSString *newPath = [docsFolder stringByAppendingPathComponent:(NSString *)ab.bookDocumentsURL];
            book.documentsURL = [NSURL fileURLWithPath:newPath];//[NSURL URLWithString:ab.bookDocumentsURL];
            book.coverPageURL = [[NSBundle bundleForClass:[self class]] URLForResource:ab.bookCoverImageName withExtension:@"png"];
            book.bookDesc = ab.bookEditionNumber;
            book.bookName = ab.bookName;
        }
        if (book) {
            fetchedBook(book);
        } else {
            cloudError(error);
        }
    } else {
        cloudError(error);
    }
}

- (void) fetch_11th_EditionFromiCloudWithCompletionBlock:(bookViewModel) fetchedBook withError: (cloudError) cloudError {
    
    [self fetchRecordfromiCloud:EEPP_11_AES withCompletionBlock:^(CKRecord *record, NSError *error) {
        NSLog(@"%s %@", __FUNCTION__, error.userInfo);
        if (record) {
            
            // 11th Edition found on iCLoud
            NSLog(@"------- %@", record.creationDate);
            NSLog(@"------- %@", record.creatorUserRecordID.recordName);
            NSLog(@"------- %@", record.modificationDate);
            NSLog(@"------- %@", record.lastModifiedUserRecordID.recordName);
            Book *book = [self parseRecord:record];
            
            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.lastPathComponent == %@", book.bookAESName];
            NSArray *matchingPaths = [[[NSFileManager defaultManager] subpathsAtPath:documentsDirectory] filteredArrayUsingPredicate:predicate];
            NSLog(@"%@", matchingPaths);
            
            NSURL * encryptedPDFURL;
            
            if ([matchingPaths lastObject]) {
                NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[matchingPaths lastObject]];
                encryptedPDFURL = [NSURL fileURLWithPath:filePath];
                
            } else {
                // not found in Documents directory
                CKAsset *pdf = [book.editionRecord valueForKey:CLOUD_RECORD_EDITION_PDF];
                NSLog(@"fethcd record pdf file url = %@", pdf.fileURL);
                
                NSString *docsFolder = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
                NSString *newPath = [docsFolder stringByAppendingPathComponent:book.bookAESName];
                
                encryptedPDFURL = [NSURL fileURLWithPath:newPath];
                const BOOL needsCopy = ![NSFileManager.defaultManager fileExistsAtPath:newPath];
                
                if (needsCopy) {
                    NSLog(@"---- Needs copy to documents directory");
                } else {
                    NSLog(@"---- File already exists in the documents directory");
                }
                
                NSError *error;
                if ((needsCopy) && ![NSFileManager.defaultManager copyItemAtURL:pdf.fileURL toURL:encryptedPDFURL error:&error]) {
                    NSLog(@"Error while copying %@: %@", pdf.fileURL.path, error.localizedDescription);
                } else {
                    NSLog(@"FILE COPIED at url = %@", encryptedPDFURL);
                }
            }
            
            CKAsset *pdfAsset = [[CKAsset alloc] initWithFileURL:encryptedPDFURL];
            [record setObject:pdfAsset forKey:CLOUD_RECORD_EDITION_PDF];
            [book.database saveRecord:record completionHandler:^(CKRecord * _Nullable record, NSError * _Nullable error) {
                if (error != nil) {
                    NSLog(@"%s - %@", __FUNCTION__, error);
                    cloudError(error);
                } else {
                    Book *book = [self parseRecord:record];
                    NSLog(@"documents url = %@", book.documentsURL);
                    [self saveBookToCoreData:book];
                    fetchedBook(book);
                }
            }];
            
        } else {
            // TRy again if network failure

            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.lastPathComponent == %@", EEPP_11_AES];
            NSArray *matchingPaths = [[[NSFileManager defaultManager] subpathsAtPath:documentsDirectory] filteredArrayUsingPredicate:predicate];
            NSLog(@"%@", matchingPaths);
            
            NSURL * encryptedPDFURL;
            
            if ([matchingPaths lastObject]) {
                NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[matchingPaths lastObject]];
                encryptedPDFURL = [NSURL fileURLWithPath:filePath];
                
                [self save11thEdiPDFtoiCloudWithDocumentURL:encryptedPDFURL withCompletionBlock:^(CKRecord *record, NSError *error) {
                    if (record) {
                        Book *book = [self parseRecord:record];
                        [self saveBookToCoreData:book];
                        fetchedBook(book);
                    } else {
                        NSLog(@"%s --- Unable to render PDF with eror %@", __FUNCTION__, error.userInfo);
                        cloudError(error);
                    }
                } withSaveError:^(NSError *error) {
                    cloudError(error);
                }];
            } else {
                cloudError(error);
            }
#warning new User
            /*[self save11thEdiPDFtoiCloud:EEPP_11_AES withCompletionBlock:^(CKRecord *record, NSError *error) {
                if (record) {
                    Book *book = [self parseRecord:record];
                    [self saveBookToCoreData:book];
                    fetchedBook(book);
                } else {
                    NSLog(@"%s --- Unable to render PDF with eror %@", __FUNCTION__, error.userInfo);
                    // record exists can be the error as in else block bcoz of network failure
                    cloudError(error);
                }
            } withSaveError:^(NSError *error) {
                cloudError(error);
            }]; */
        }
    }];
}

- (void)save11thEdiPDFtoiCloudWithDocumentURL:(NSURL *)documentSamplesURL withCompletionBlock:(recordSaved) recordSaved withSaveError:(cloudError) cloudError {
    
    NSString *timestampAsString = [NSString stringWithFormat:@"%f", [NSDate timeIntervalSinceReferenceDate]];
    NSArray *timestampParts = [timestampAsString componentsSeparatedByString:@"."];
    NSLog(@"-----record id = %@", timestampParts[0]);
    
    NSLog(@"Saved to documents dir with uRL = %@", documentSamplesURL);
    
    CKRecordID *recordEditionId = [[CKRecordID alloc] initWithRecordName:EEPP_11_AES];
    CKRecord *recordEdition = [[CKRecord alloc] initWithRecordType:CLOUD_RECORD_TYPE recordID:recordEditionId];
    //0
    [recordEdition setObject:timestampParts[0] forKey:CLOUD_RECORD_EDITION_TIME_INTERVAL];
    
    //1
    [recordEdition setObject:[NSDate date] forKey:CLOUD_RECORD_EDITION_DATE];
    
    CKAsset *pdfAsset = [[CKAsset alloc] initWithFileURL:documentSamplesURL];
    //2
    [recordEdition setObject:pdfAsset forKey:CLOUD_RECORD_EDITION_PDF];
    
    NSURL *coverPageImageURL = [[NSBundle bundleForClass:[self class]] URLForResource:EEPP_11_COVER_IMAGE_NAME withExtension:@"png"];
    CKAsset *pdfCoverPage = [[CKAsset alloc] initWithFileURL:coverPageImageURL];
    //3
    [recordEdition setObject:pdfCoverPage forKey:CLOUD_RECORD_EDITION_COVER_IMAGE];
    //4
    [recordEdition setObject:EEPP_11_EDITION_NAME forKey:CLOUD_RECORD_EDITION_NUMBER];
    //5
    [recordEdition setObject:EEPP_11_BOOK_NAME forKey:CLOUD_RECORD_EDITION_NAME];
    //6
    [recordEdition setObject:EEPP_11_AES forKey:CLOUD_RECORD_PDF_AES_NAME];
    
    
    CKContainer *container = [CKContainer defaultContainer];
    CKDatabase *privateDatabase = container.privateCloudDatabase;
    
    [privateDatabase saveRecord:recordEdition completionHandler:^(CKRecord * _Nullable record, NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"%s - %@", __FUNCTION__, error);
            recordSaved(nil, error);
        } else {
            NSLog(@"record saved to iCloud successfully");
            recordSaved(record, error);
        }
    }];
}

- (void) save_12th_EditionToiCloudAndLocallyIfDoesNotExistWithCompletionBlock:(bookViewModel) fetchedBook withError: (cloudError) cloudError {
    
    [self fetchRecordfromiCloud:EEPP_12_AES withCompletionBlock:^(CKRecord *record, NSError *error) {
        if (record) {
            
            // 12th Edition found on iCLoud
            NSLog(@"------- %@", record.creationDate);
            NSLog(@"------- %@", record.creatorUserRecordID.recordName);
            NSLog(@"------- %@", record.modificationDate);
            NSLog(@"------- %@", record.lastModifiedUserRecordID.recordName);
            Book *book = [self parseRecord:record];
            
            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.lastPathComponent == %@", book.bookAESName];
            NSArray *matchingPaths = [[[NSFileManager defaultManager] subpathsAtPath:documentsDirectory] filteredArrayUsingPredicate:predicate];
            NSLog(@"%@", matchingPaths);
            
            NSURL * encryptedPDFURL;
            
            if ([matchingPaths lastObject]) {
                NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[matchingPaths lastObject]];
                encryptedPDFURL = [NSURL fileURLWithPath:filePath];
            } else {
                // not found in Documents directory
                CKAsset *pdf = [book.editionRecord valueForKey:CLOUD_RECORD_EDITION_PDF];
                NSLog(@"fethcd record pdf file url = %@", pdf.fileURL);
                
                NSString *docsFolder = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
                NSString *newPath = [docsFolder stringByAppendingPathComponent:book.bookAESName];
                
                encryptedPDFURL = [NSURL fileURLWithPath:newPath];
                const BOOL needsCopy = ![NSFileManager.defaultManager fileExistsAtPath:newPath];
                
                if (needsCopy) {
                    NSLog(@"---- Needs copy to documents directory");
                } else {
                    NSLog(@"---- File already exists in the documents directory");
                }
                
                NSError *error;
                if ((needsCopy) && ![NSFileManager.defaultManager copyItemAtURL:pdf.fileURL toURL:encryptedPDFURL error:&error]) {
                    NSLog(@"Error while copying %@: %@", pdf.fileURL.path, error.localizedDescription);
                } else {
                    NSLog(@"FILE COPIED at url = %@", encryptedPDFURL);
                }
            }
            
            CKAsset *pdfAsset = [[CKAsset alloc] initWithFileURL:encryptedPDFURL];
            [record setObject:pdfAsset forKey:CLOUD_RECORD_EDITION_PDF];
            [book.database saveRecord:record completionHandler:^(CKRecord * _Nullable record, NSError * _Nullable error) {
                if (error != nil) {
                    NSLog(@"%s - %@", __FUNCTION__, error);
#warning Here the Record exists on iCLoud. So we need to retry saving the record / updating the record to iCloud
                    cloudError(error);
                } else {
                    Book *book = [self parseRecord:record];
                    NSLog(@"documents url = %@", book.documentsURL);
                    [self saveBookToCoreData:book];
                    fetchedBook(book);
                }
            }];
        } else {
            [self savePDFtoiCloud:EEPP_12_AES withCompletionBlock:^(CKRecord *record, NSError *error) {
                if (record) {
                    Book *book = [self parseRecord:record];
                    [self saveBookToCoreData:book];
                    fetchedBook(book);
                } else {
                    NSLog(@"%s --- Unable to render PDF with eror %@", __FUNCTION__, error);
                    cloudError(error);
                }
            } withSaveError:^(NSError *error) {
                cloudError(error);
            }];
        }
    }];
}

- (Book *) parseRecord:(CKRecord *)record {
    Book *book = [[Book alloc] init];
    
    book.editionRecord = record;
    book.privateContainer = self.privateContainer;
    book.database = self.privateDatabase;
    
    CKAsset *pdf = [book.editionRecord valueForKey:CLOUD_RECORD_EDITION_PDF];
    book.documentsURL = pdf.fileURL;//PSCCopyFileURLToDocumentFolderAndOverride(pdf.fileURL, NO);
    NSLog(@"FROM iCLOUD RECORD TO DOCUMENTS URL ****** %s ****** ~~~~~~~~~~~~~~~~~~~~~~ %@ ****** ~~~~~~~~~~~~~~~~~~~~~~ ", __FUNCTION__, pdf.fileURL);
    CKAsset *coverPage = [book.editionRecord valueForKey:CLOUD_RECORD_EDITION_COVER_IMAGE];
    book.coverPageURL = coverPage.fileURL;
    
    book.bookName = [book.editionRecord valueForKey:CLOUD_RECORD_EDITION_NAME];
    book.bookDesc = [book.editionRecord valueForKey:CLOUD_RECORD_EDITION_NUMBER];
    book.bookAESName = [book.editionRecord valueForKey:CLOUD_RECORD_PDF_AES_NAME];
    book.timestampString = [book.editionRecord valueForKey:CLOUD_RECORD_EDITION_TIME_INTERVAL];
    book.savedDate = [book.editionRecord valueForKey:CLOUD_RECORD_EDITION_DATE];
    
    //    if (!self.allEEPPEditions) {
    //        self.allEEPPEditions = [NSMutableArray array];
    //    }
    //    [self.allEEPPEditions removeAllObjects];
    //    [self.allEEPPEditions addObject:book];
    return book;
}

- (AviationPressLandingViewController *) getInitializedVCForURL:(NSURL *) documentSamplesURL forBookEditionNumber:(NSString *) bookEditionName andBookObj:(Book *)bookObj {
    
    NSLog(@"Rendering frm ----- %@", documentSamplesURL.path);
    NSString *passphrase;
    NSString *salt;
    
    if ([bookEditionName isEqualToString:EEPP_11_EDITION_NAME]) {
        passphrase = EEPP_11_PASSPHRASE;
        salt = EEPP_11_SALT;
    } else if ([bookEditionName isEqualToString:EEPP_12_SAMPLE_EDITION_NAME]) {
        passphrase = EEPP_12_SAMPLE_PASSPHRASE;
        salt = EEPP_12_SAMPLE_SALT;
    } else if ([bookEditionName isEqualToString:EEPP_12_EDITION_NAME]) {
        passphrase = EEPP_12_PASSPHRASE;
        salt = EEPP_12_SALT;
    }else if ([bookEditionName isEqualToString:EEPP_13_EDITION_NAME]) {
        passphrase = EEPP_13_PASSPHRASE;
        salt = nil;
    }else if ([bookObj.bookDesc isEqualToString:@"12"])
    {
        passphrase = EEPP_12_PASSPHRASE;
        salt = EEPP_12_SALT;
    }else if ([bookObj.bookDesc isEqualToString:@"13"])
    {
        passphrase = EEPP_13_PASSPHRASE;
        salt = EEPP_13_SALT;
    }else
    {
        passphrase = EEPP_12_PASSPHRASE;
        salt = EEPP_12_SALT;

//        passphrase =  bookObj.passphrase;
//        salt = bookObj.salt;
    }

    
    // New Logic
    passphrase =  [AviationPressIAPHelper getpassPhrase:bookObj];
    salt = [AviationPressIAPHelper getSalt:bookObj];

    NSString *(^const passphraseProvider)(void) = ^() {
        return passphrase;
    };
    
    PSPDFAESCryptoDataProvider *cryptoWrapper = [[PSPDFAESCryptoDataProvider alloc] initWithURL:documentSamplesURL passphraseProvider:passphraseProvider salt:salt rounds:PSPDFDefaultPBKDFNumberOfRounds];
//    PSPDFAESCryptoDataProvider *cryptoWrapper = [[PSPDFAESCryptoDataProvider alloc] initWithURL:documentSamplesURL passphraseProvider:passphraseProvider];

  //  PSPDFDocument *document = [PSPDFDocument documentWithURL:documentSamplesURL];
    
   PSPDFDocument *document = [PSPDFDocument documentWithDataProvider:cryptoWrapper];
    
    document.UID = documentSamplesURL.lastPathComponent; // manually set an UID for encrypted documents.
    document.title = [NSString stringWithFormat:@"%@", bookObj.bookName];
    
    AviationPressLandingViewController *vc = [[AviationPressLandingViewController alloc] initWithDocument:document configuration:[PSPDFConfiguration configurationWithBuilder:^(PSPDFConfigurationBuilder * _Nonnull builder) {
        builder.backgroundColor = [UIColor whiteColor];
        builder.firstPageAlwaysSingle = YES;
        [builder setMargin:UIEdgeInsetsMake(44, 0, 0, 0)];
        [builder setThumbnailGrouping:PSPDFThumbnailGroupingNever];
        [builder setMaximumZoomScale:20.0f];
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        if (orientation == UIDeviceOrientationLandscapeLeft ||
            orientation == UIDeviceOrientationLandscapeRight ||
            orientation == UIInterfaceOrientationLandscapeLeft ||
            orientation == UIInterfaceOrientationLandscapeRight) {
            builder.pageMode = PSPDFPageModeDouble;
        } else {
            builder.pageMode = PSPDFPageModeSingle;
        }
    }] withBookEditionName:bookEditionName];
    return vc;
}

- (void)savePDFtoiCloud:(NSString *)pdfName withCompletionBlock:(recordSaved) recordSaved withSaveError:(cloudError) cloudError {
    
    NSString *timestampAsString = [NSString stringWithFormat:@"%f", [NSDate timeIntervalSinceReferenceDate]];
    NSArray *timestampParts = [timestampAsString componentsSeparatedByString:@"."];
    NSLog(@"-----record id = %@", timestampParts[0]);
    
    NSURL *const encryptedPDFURL = [[[NSBundle bundleForClass:[self class]] resourceURL] URLByAppendingPathComponent:pdfName];
    NSURL *documentSamplesURL = PSCCopyFileURLToDocumentFolderAndOverride(encryptedPDFURL, NO);
    
    NSLog(@"Saved to documents dir with uRL = %@", documentSamplesURL);
    
    CKRecordID *recordEditionId = [[CKRecordID alloc] initWithRecordName:pdfName];
    CKRecord *recordEdition = [[CKRecord alloc] initWithRecordType:CLOUD_RECORD_TYPE recordID:recordEditionId];
    //0
    [recordEdition setObject:timestampParts[0] forKey:CLOUD_RECORD_EDITION_TIME_INTERVAL];
    
    //1
    [recordEdition setObject:[NSDate date] forKey:CLOUD_RECORD_EDITION_DATE];
    
    CKAsset *pdfAsset = [[CKAsset alloc] initWithFileURL:documentSamplesURL];
    //2
    [recordEdition setObject:pdfAsset forKey:CLOUD_RECORD_EDITION_PDF];
    
    NSURL *coverPageImageURL = [[NSBundle bundleForClass:[self class]] URLForResource:EEPP_12_COVER_IMAGE_NAME withExtension:@"png"];
    CKAsset *pdfCoverPage = [[CKAsset alloc] initWithFileURL:coverPageImageURL];
    //3
    [recordEdition setObject:pdfCoverPage forKey:CLOUD_RECORD_EDITION_COVER_IMAGE];
    //4
    [recordEdition setObject:EEPP_12_EDITION_NAME forKey:CLOUD_RECORD_EDITION_NUMBER];
    //5
    [recordEdition setObject:EEPP_12_BOOK_NAME forKey:CLOUD_RECORD_EDITION_NAME];
    //6
    [recordEdition setObject:pdfName forKey:CLOUD_RECORD_PDF_AES_NAME];
    
    
    CKContainer *container = [CKContainer defaultContainer];
    CKDatabase *privateDatabase = container.privateCloudDatabase;
    
    [privateDatabase saveRecord:recordEdition completionHandler:^(CKRecord * _Nullable record, NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"%s - %@", __FUNCTION__, error);
            recordSaved(nil, error);
        } else {
            NSLog(@"record saved to iCloud successfully");
            recordSaved(record, error);
        }
    }];
}

#warning to be removed

- (void)save11thEdiPDFtoiCloud:(NSString *)pdfName withCompletionBlock:(recordSaved) recordSaved withSaveError:(cloudError) cloudError {
    
    NSString *timestampAsString = [NSString stringWithFormat:@"%f", [NSDate timeIntervalSinceReferenceDate]];
    NSArray *timestampParts = [timestampAsString componentsSeparatedByString:@"."];
    NSLog(@"-----record id = %@", timestampParts[0]);
    
    NSURL *const encryptedPDFURL = [[[NSBundle bundleForClass:[self class]] resourceURL] URLByAppendingPathComponent:pdfName];
    NSURL *documentSamplesURL = PSCCopyFileURLToDocumentFolderAndOverride(encryptedPDFURL, NO);
    
    NSLog(@"Saved to documents dir with uRL = %@", documentSamplesURL);
    
    CKRecordID *recordEditionId = [[CKRecordID alloc] initWithRecordName:pdfName];
    CKRecord *recordEdition = [[CKRecord alloc] initWithRecordType:CLOUD_RECORD_TYPE recordID:recordEditionId];
    //0
    [recordEdition setObject:timestampParts[0] forKey:CLOUD_RECORD_EDITION_TIME_INTERVAL];
    
    //1
    [recordEdition setObject:[NSDate date] forKey:CLOUD_RECORD_EDITION_DATE];
    
    CKAsset *pdfAsset = [[CKAsset alloc] initWithFileURL:documentSamplesURL];
    //2
    [recordEdition setObject:pdfAsset forKey:CLOUD_RECORD_EDITION_PDF];
    
    NSURL *coverPageImageURL = [[NSBundle bundleForClass:[self class]] URLForResource:EEPP_11_COVER_IMAGE_NAME withExtension:@"png"];
    CKAsset *pdfCoverPage = [[CKAsset alloc] initWithFileURL:coverPageImageURL];
    //3
    [recordEdition setObject:pdfCoverPage forKey:CLOUD_RECORD_EDITION_COVER_IMAGE];
    //4
    [recordEdition setObject:EEPP_11_EDITION_NAME forKey:CLOUD_RECORD_EDITION_NUMBER];
    //5
    [recordEdition setObject:EEPP_11_BOOK_NAME forKey:CLOUD_RECORD_EDITION_NAME];
    //6
    [recordEdition setObject:pdfName forKey:CLOUD_RECORD_PDF_AES_NAME];
    
    
    CKContainer *container = [CKContainer defaultContainer];
    CKDatabase *privateDatabase = container.privateCloudDatabase;
    
    [privateDatabase saveRecord:recordEdition completionHandler:^(CKRecord * _Nullable record, NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"%s - %@", __FUNCTION__, error);
            recordSaved(nil, error);
        } else {
            NSLog(@"record saved to iCloud successfully");
            recordSaved(record, error);
        }
    }];
}

- (void) fetchRecordfromiCloud:(NSString *)pdfName withCompletionBlock:(recordSaved) recordFetched {
    CKContainer *container = [CKContainer defaultContainer];
    CKDatabase *privateDatabase = container.privateCloudDatabase;
    
    CKRecordID *recordEditionId = [[CKRecordID alloc] initWithRecordName:pdfName];
    [privateDatabase fetchRecordWithID:recordEditionId completionHandler:^(CKRecord * _Nullable record, NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"%s - %@", __FUNCTION__, error.userInfo);
            //            cloudError(error);
            NSLog(@"Error in fetching record from icloud");
            recordFetched(nil, error);
        } else {
            NSLog(@"REcord fetched from icloud to be upadted wid local copy");
            recordFetched(record, error);
        }
    }];
    /*[container fetchUserRecordIDWithCompletionHandler:^(CKRecordID * _Nullable recordID, NSError * _Nullable error) {
     if (error != nil) {
     NSLog(@"%s - %@", __FUNCTION__, error);
     //            cloudError(error);
     NSLog(@"Error in fetching USER ID from icloud");
     recordFetched(nil, error);
     } else {
     NSLog(@"REcord fetched USER ID from icloud = %@", recordID.recordName);
     [privateDatabase fetchRecordWithID:recordEditionId completionHandler:^(CKRecord * _Nullable record, NSError * _Nullable error) {
     if (error != nil) {
     NSLog(@"%s - %@", __FUNCTION__, error);
     //            cloudError(error);
     NSLog(@"Error in fetching record from icloud");
     recordFetched(nil, error);
     } else {
     NSLog(@"REcord fetched from icloud to be upadted wid local copy");
     recordFetched(record, error);
     }
     }];
     }
     }];*/
    
}
-(void)saveProperBookToCoreData:(Book *) book
{
    NSError *error;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    
    //Book
//    NSURL *const encryptedPDFURL = [[[NSBundle bundleForClass:[self class]] resourceURL] URLByAppendingPathComponent:EEPP_11_AES];
    NSURL *encryptedPDFURL = book.documentsURL;

    NSURL *documentSamplesURL = PSCCopyFileURLToDocumentFolderAndOverride(encryptedPDFURL, NO);
    
    AviationBook *bookObject = [NSEntityDescription insertNewObjectForEntityForName:@"AviationBook"
                                                             inManagedObjectContext:self.managedObjectContext];
    [bookObject setValue:book.bookName forKey:@"bookName"];
    [bookObject setValue:[NSNumber numberWithBool:NO] forKey:@"isBookSynced"];
    [bookObject setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
    [bookObject setValue:documentSamplesURL.lastPathComponent forKey:@"bookDocumentsURL"];
    [bookObject setValue:book.bookEditionNo forKey:@"bookEditionNumber"];
    NSString *imgName = EEPP_12_COVER_IMAGE_NAME;
    if ([book.bookEditionNo isEqualToString:@"11"])
    {
        imgName = EEPP_11_COVER_IMAGE_NAME;
        
    }else if ([book.bookEditionNo isEqualToString:@"13"])
    {
        imgName = EEPP_13_COVER_IMAGE_NAME;
    }
    NSURL *coverPageURL = [[NSBundle bundleForClass:[self class]] URLForResource:imgName withExtension:@"png"];
    [bookObject setValue:coverPageURL.lastPathComponent forKey:@"bookCoverImageName"];
    
    NSLog(@"SONIAS CHECK  *** 1st Launch encryptedPDFURL - %@", documentSamplesURL);
    NSLog(@"SONIAS CHECK  *** 1st Launch coverPageURL - %@", coverPageURL);
    
    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    
#warning EDITION
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Edition"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name contains[cd] %@", @"All Editions"];
    [fetchRequest setPredicate:predicate];
    NSArray *allEditions = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    if (allEditions.count > 0) {
        // do nothings
        /// update hasBOOK
        for (Edition *edi in allEditions) {
            [edi addHasBook:[NSSet setWithObjects:bookObject, nil]];
        }
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    } else {
        Edition *edi1 = [NSEntityDescription insertNewObjectForEntityForName:@"Edition" inManagedObjectContext:self.managedObjectContext];
        edi1.name = @"All Editions";
        edi1.selected = YES;
        [edi1 addHasBook:[NSSet setWithObjects:bookObject, nil]];
        
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    }
    
    
    NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc] initWithEntityName:@"Edition"];
    NSArray *allEditions1 = [[managedObjectContext executeFetchRequest:fetchRequest1 error:nil] mutableCopy];
    
    for (Edition *ediCD in allEditions1) {
        NSSet<AviationBook *> *books = ediCD.hasBook;
        for (AviationBook *bk in books) {
            NSLog(@"$$$$$$$$$ %@ --- %@", ediCD.name, bk.bookEditionNumber);
        }
    }
    
    NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:@"AviationBook"];
    NSArray *allBooks = [managedObjectContext executeFetchRequest:req error:nil];
    for (AviationBook *bk in allBooks) {
        NSSet<Edition *> *belongsToEdi = bk.belongsTo;
        for (Edition *ed in belongsToEdi) {
            NSLog(@"&&&&&&&& %@ ---- %@", bk.bookName, ed.name);
        }
    }
}
- (void) saveBookToCoreData:(Book *) book
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"AviationBook"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bookEditionNumber == %@", book.bookDesc];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *tempAviationBooks = [context executeFetchRequest:request error:&error];
    
    if (tempAviationBooks.count > 0)
    {
        for (AviationBook *av in tempAviationBooks) {
            [av setValue:book.documentsURL.lastPathComponent forKey:@"bookDocumentsURL"];
            if (![context save:&error]) {
                NSLog(@"Can't Delete! %@ %@", error, [error localizedDescription]);
                return;
            } else {
                NSLog(@"DELETION DONE");
            }
        }
    } else {
        // Insert
        AviationBook *bookObject = [NSEntityDescription insertNewObjectForEntityForName:@"AviationBook"
                                                                    inManagedObjectContext:context];
        [bookObject setValue:book.bookName forKey:@"bookName"];
        [bookObject setValue:[NSNumber numberWithBool:NO] forKey:@"isBookSynced"];
        [bookObject setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
        [bookObject setValue:book.documentsURL.lastPathComponent forKey:@"bookDocumentsURL"];
        [bookObject setValue:book.bookDesc forKey:@"bookEditionNumber"];
        //[[NSBundle bundleForClass:[self class]] URLForResource:EEPP_11_COVER_IMAGE_NAME withExtension:@"png"]
        [bookObject setValue:book.coverPageURL.lastPathComponent forKey:@"bookCoverImageName"];
        
        if (![context save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
        
        [self assignBookToAllEditions:bookObject];
    }
}

- (BOOL) assignBookToAllEditions:(AviationBook *) bookObject {
    NSManagedObjectContext *context = [self managedObjectContext];

    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Edition"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name contains[cd] %@", @"All Editions"];
    [fetchRequest setPredicate:predicate];
    NSArray *allEditions = [[context executeFetchRequest:fetchRequest error:&error] mutableCopy];
    
    if (allEditions.count > 0) {
        for (Edition *edi in allEditions) {
            [edi addHasBook:[NSSet setWithObjects:bookObject, nil]];
        }
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    } else {
        Edition *edi1 = [NSEntityDescription insertNewObjectForEntityForName:@"Edition" inManagedObjectContext:self.managedObjectContext];
        edi1.name = @"All Editions";
        edi1.selected = YES;
        [edi1 addHasBook:[NSSet setWithObjects:bookObject, nil]];
        
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    }
    return YES;
}

- (BOOL) save_12th_EditionBookToCoreData {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"AviationBook"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bookEditionNumber == %@", EEPP_12_EDITION_NAME];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *tempAviationBooks = [context executeFetchRequest:request error:&error];
    
    if (tempAviationBooks.count > 0) {
        return YES;
    } else {
        // Insert Book Edition 12th
        NSURL *const encryptedPDFURL_12 = [[[NSBundle bundleForClass:[self class]] resourceURL] URLByAppendingPathComponent:EEPP_12_AES];
        NSURL *documentSamplesURL_12 = PSCCopyFileURLToDocumentFolderAndOverride(encryptedPDFURL_12, NO);
        
        AviationBook *bookObject_12 = [NSEntityDescription insertNewObjectForEntityForName:@"AviationBook"
                                                                    inManagedObjectContext:self.managedObjectContext];
        [bookObject_12 setValue:EEPP_12_BOOK_NAME forKey:@"bookName"];
        [bookObject_12 setValue:[NSNumber numberWithBool:NO] forKey:@"isBookSynced"];
        [bookObject_12 setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
        [bookObject_12 setValue:documentSamplesURL_12.lastPathComponent forKey:@"bookDocumentsURL"];
        [bookObject_12 setValue:EEPP_12_EDITION_NAME forKey:@"bookEditionNumber"];

        NSURL *coverPageURL_12 = [[NSBundle bundleForClass:[self class]] URLForResource:EEPP_12_COVER_IMAGE_NAME withExtension:@"png"];
        [bookObject_12 setValue:coverPageURL_12.lastPathComponent forKey:@"bookCoverImageName"];

        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
        [self assignBookToAllEditions:bookObject_12];
        
        return YES;
    }
    return NO;
}

#pragma mark - fetch for categories

- (NSArray *) fetchForCategory:(NSString *)categoryName {
    NSMutableArray *allEdi = [NSMutableArray array];
    NSError *error;
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Edition"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name contains %@", categoryName];
    [fetchRequest setPredicate:predicate];
    NSArray *editions = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (editions && [editions lastObject]) {
        Edition *editionCD = [editions lastObject];
        NSSet *aviationBooks = [editionCD hasBook];
        
        for (AviationBook *ab in aviationBooks) {
            Book *book = [[Book alloc] init];
            NSLog(@"SONIAS CHECK  *** GET SAVED encryptedPDFURL - %@", [NSURL URLWithString:ab.bookDocumentsURL]);
            NSLog(@"SONIAS CHECK  *** GET SAVED coverPageURL - %@", ab.bookCoverImageName);
            
            NSString *docsFolder = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
            NSString *newPath = [docsFolder stringByAppendingPathComponent:(NSString *)ab.bookDocumentsURL];
            
            book.documentsURL = [NSURL fileURLWithPath:newPath];//[NSURL URLWithString:ab.bookDocumentsURL];
            
            book.coverPageURL = [[NSBundle bundleForClass:[self class]] URLForResource:ab.bookCoverImageName withExtension:@"png"];
            
            book.bookDesc = ab.bookEditionNumber;
            book.bookName = ab.bookName;
            book.category = editionCD.name;
            [allEdi addObject:book];
        }
        return allEdi;
    }
    return nil;
}
@end
