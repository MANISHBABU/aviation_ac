//
//  PSCAESCryptoDataProviderExample.m
//  AviationPress
//
//  Created by Sonia Mane on 29/08/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import "PSCAESCryptoDataProviderExample.h"
#import "AviationPressLandingViewController.h"
#import "PSCFileHelper.h"

@implementation PSCAESCryptoDataProviderExample

+ (instancetype)sharedInstance {
    static PSCAESCryptoDataProviderExample *_sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[PSCAESCryptoDataProviderExample alloc] init];
    });
    return _sharedInstance;
}

/*
- (void)invokeWithDelegate:(id<PSCExampleRunnerDelegate>) delegate forPDF:(NSString *) pdfName withCompletionBlock:(initializedVC) initializedVC {

    [self fetchRecordfromiCloud:pdfName withCompletionBlock:^(CKRecord *record) {
        if (record) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                initializedVC([self getInitializedVCForRecord:record]);
            });
        } else {
            [self savePDFtoiCloud:pdfName withCompletionBlock:^(CKRecord *record) {
                if (record) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        initializedVC([self getInitializedVCForRecord:record]);
                    });
                } else {
                    NSLog(@"%s --- Unable to render PDF", __FUNCTION__);
                }
            }];
        }
    }];
*/
//    NSString *filename = @"every_11.pdf.aes";
    
//    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentDir = [documentPaths objectAtIndex:0];
//    NSString *filePath = [documentDir stringByAppendingPathComponent:filename];
//    
//    [self copyPDFToDocumentsDirectoryWithDestinationPath:filePath withFileName:filename];
    
    
//    @"every_11.pdf.aes"
    
    /*
    NSURL *const encryptedPDFURL = [[[NSBundle mainBundle] resourceURL] URLByAppendingPathComponent:pdfName];
    NSURL *documentSamplesURL = PSCCopyFileURLToDocumentFolderAndOverride(encryptedPDFURL, NO);
    
    
    NSString *passphrase = @"aBfMjDTJVjUf3FApA6gtim0e61LeSGWV9sTxBdsfadsHGFHGbasjdjaDSNKCUHGYGghgsguGDHDiuHSDIUHASchsid";
    NSString *salt = @"öhapvuenröaoeruhföaeiruaerubYTFYTFVbjncJKcnijsdhcuHcIUTUYGDBHJsaSUHAUIJBvodsijvireiowrewBUYEuuasdas";
    
    NSString *(^const passphraseProvider)(void) = ^() {
        return passphrase;
    };
    
//    NSURL *const encryptedPDF = [NSURL fileURLWithPath:filePath];

//    NSURL *const encryptedPDF = [[[NSBundle mainBundle] resourceURL] URLByAppendingPathComponent:@"every_11.pdf.aes"];

    PSPDFAESCryptoDataProvider *cryptoWrapper = [[PSPDFAESCryptoDataProvider alloc] initWithURL:documentSamplesURL passphraseProvider:passphraseProvider salt:salt rounds:PSPDFDefaultPBKDFNumberOfRounds];
    
    PSPDFDocument *document = [PSPDFDocument documentWithDataProvider:cryptoWrapper];
    document.UID = encryptedPDFURL.lastPathComponent; // manually set an UID for encrypted documents.
    document.title = @"EVERYTHING EXPLAINED";
    
    AviationPressLandingViewController *vc = [[AviationPressLandingViewController alloc] initWithDocument:document
                                                                                            configuration:[PSPDFConfiguration configurationWithBuilder:^(PSPDFConfigurationBuilder * _Nonnull builder) {
        builder.backgroundColor = [UIColor whiteColor];
        builder.doublePageModeOnFirstPage = NO;
        [builder setThumbnailGrouping:PSPDFThumbnailGroupingNever];
        [builder setMaximumZoomScale:20.0f];
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        
        if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeLeft) {
            builder.pageMode = PSPDFPageModeDouble;
        } else {
            builder.pageMode = PSPDFPageModeSingle;
        }
    }]];
    return vc; */
//}

//- (void) fetchAllEditionsWithCompletionBlock:(allEditionsFromCloud) editions {
//    CKContainer *container = [CKContainer defaultContainer];
//    CKDatabase *privateDatabase = container.privateCloudDatabase;
//    NSPredicate *predicate = [NSPredicate predicateWithValue:YES];
//    CKQuery *query = [[CKQuery alloc] initWithRecordType:@"EEPP_Editions" predicate:predicate];
//    
//    [privateDatabase performQuery:query inZoneWithID:nil completionHandler:^(NSArray<CKRecord *> * _Nullable results, NSError * _Nullable error) {
//        if (error != nil) {
//            NSLog(@"%s - %@", __FUNCTION__, error);
//            editions(nil);
//        } else {
//            editions(results);
//        }
//    }];
//}

- (AviationPressLandingViewController *) getInitializedVCForURL:(NSURL *) documentSamplesURL {
//    CKAsset *pdf = [record valueForKey:@"PDF"];
//    NSURL *documentSamplesURL = pdf.fileURL;
    NSString *passphrase = @"aBfMjDTJVjUf3FApA6gtim0e61LeSGWV9sTxBdsfadsHGFHGbasjdjaDSNKCUHGYGghgsguGDHDiuHSDIUHASchsid";
    NSString *salt = @"öhapvuenröaoeruhföaeiruaerubYTFYTFVbjncJKcnijsdhcuHcIUTUYGDBHJsaSUHAUIJBvodsijvireiowrewBUYEuuasdas";
    
    NSString *(^const passphraseProvider)(void) = ^() {
        return passphrase;
    };
    
    PSPDFAESCryptoDataProvider *cryptoWrapper = [[PSPDFAESCryptoDataProvider alloc] initWithURL:documentSamplesURL passphraseProvider:passphraseProvider salt:salt rounds:PSPDFDefaultPBKDFNumberOfRounds];
    
    PSPDFDocument *document = [PSPDFDocument documentWithDataProvider:cryptoWrapper];
    document.UID = documentSamplesURL.lastPathComponent; // manually set an UID for encrypted documents.
    document.title = @"EVERYTHING EXPLAINED";
    
    AviationPressLandingViewController *vc = [[AviationPressLandingViewController alloc] initWithDocument:document configuration:[PSPDFConfiguration configurationWithBuilder:^(PSPDFConfigurationBuilder * _Nonnull builder) {
        builder.backgroundColor = [UIColor whiteColor];
        builder.doublePageModeOnFirstPage = NO;
        [builder setThumbnailGrouping:PSPDFThumbnailGroupingNever];
        [builder setMaximumZoomScale:20.0f];
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeLeft) {
            builder.pageMode = PSPDFPageModeDouble;
        } else {
            builder.pageMode = PSPDFPageModeSingle;
        }
    }]];
    return vc;
}

- (void)savePDFtoiCloud:(NSString *)pdfName withCompletionBlock:(recordSaved) recordSaved {

    NSURL *const encryptedPDFURL = [[[NSBundle mainBundle] resourceURL] URLByAppendingPathComponent:pdfName];
    NSURL *documentSamplesURL = PSCCopyFileURLToDocumentFolderAndOverride(encryptedPDFURL, NO);

    NSString *timestampAsString = [NSString stringWithFormat:@"%f", [NSDate timeIntervalSinceReferenceDate]];
    NSArray *timestampParts = [timestampAsString componentsSeparatedByString:@"."];
    
    CKRecordID *recordEditionId = [[CKRecordID alloc] initWithRecordName:pdfName];
    CKRecord *recordEdition = [[CKRecord alloc] initWithRecordType:@"EEPP_Editions" recordID:recordEditionId];
    //1
    [recordEdition setObject:[NSDate date] forKey:@"editionDate"];

    CKAsset *pdfAsset = [[CKAsset alloc] initWithFileURL:documentSamplesURL];
    //2
    [recordEdition setObject:pdfAsset forKey:@"PDF"];
    
    NSURL *coverPageImageURL = [[NSBundle mainBundle] URLForResource:@"EEPP" withExtension:@"png"];
    CKAsset *pdfCoverPage = [[CKAsset alloc] initWithFileURL:coverPageImageURL];
    //3
    [recordEdition setObject:pdfCoverPage forKey:@"CoverImage"];
    
    CKContainer *container = [CKContainer defaultContainer];
    CKDatabase *privateDatabase = container.privateCloudDatabase;
    
    [privateDatabase saveRecord:recordEdition completionHandler:^(CKRecord * _Nullable record, NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"%s - %@", __FUNCTION__, error);
            recordSaved(nil);
        } else {
            recordSaved(record);
        }
    }];
}

- (void) fetchRecordfromiCloud:(NSString *)pdfName withCompletionBlock:(recordSaved) recordFetched {
    
    CKContainer *container = [CKContainer defaultContainer];
    CKDatabase *privateDatabase = container.privateCloudDatabase;

    CKRecordID *recordEditionId = [[CKRecordID alloc] initWithRecordName:pdfName];
    [privateDatabase fetchRecordWithID:recordEditionId completionHandler:^(CKRecord * _Nullable record, NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"%s - %@", __FUNCTION__, error);
            recordFetched(nil);
        } else {
            recordFetched(record);
        }
    }];
}

- (void)copyPDFToDocumentsDirectoryWithDestinationPath:(NSString *) filePath withFileName:(NSString *)fileName {
    BOOL success;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    success = [fileManager fileExistsAtPath:filePath];
    
    NSString *filePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:fileName];

    NSError *error;
    
    if (success) {
        [fileManager removeItemAtPath:filePathFromApp error:&error];
       return;
    }
    
    [fileManager copyItemAtPath:filePathFromApp toPath:filePath error:nil];
}

#pragma mark - UBiquity Container

- (void)rootDirectoryForICloud:(void (^)(NSURL *))completionHandler {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *rootDirectory = [[[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil] URLByAppendingPathComponent:@"Documents"];
        
        if (rootDirectory) {
            if (![[NSFileManager defaultManager] fileExistsAtPath:rootDirectory.path isDirectory:nil]) {
                NSLog(@"Create directory");
                [[NSFileManager defaultManager] createDirectoryAtURL:rootDirectory withIntermediateDirectories:YES attributes:nil error:nil];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completionHandler(rootDirectory);
        });
    });
}

/*
 - (nullable UIViewController *)invokeWithDelegate:(id<PSCExampleRunnerDelegate>)delegate {
 
 NSURL *const encryptedPDF = [NSBundle.mainBundle.resourceURL URLByAppendingPathComponent:@"every_11_working.pdf"];
 
 PSPDFDocument *document = [PSPDFDocument documentWithURL:encryptedPDF];
 document.title = @"EVERYTHING EXPLAINED";
 
 AviationPressLandingViewController *vc = [[AviationPressLandingViewController alloc] initWithDocument:document
 configuration:[PSPDFConfiguration configurationWithBuilder:^(PSPDFConfigurationBuilder * _Nonnull builder) {
 builder.backgroundColor = [UIColor whiteColor];
 builder.doublePageModeOnFirstPage = NO;
 [builder setThumbnailGrouping:PSPDFThumbnailGroupingNever];
 [builder setMaximumZoomScale:20.0f];
 UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
 
 if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeLeft) {
 builder.pageMode = PSPDFPageModeDouble;
 } else {
 builder.pageMode = PSPDFPageModeSingle;
 }
 }]];
 return vc;
 } */

@end
