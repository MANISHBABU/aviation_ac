//
//  PSCExampleRunnerDelegate.h
//  AviationPress
//
//  Created by Sonia Mane on 29/08/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PSCExampleRunnerDelegate <NSObject>
@property (nonatomic, readonly, nullable) UIViewController *currentViewController;
@end
