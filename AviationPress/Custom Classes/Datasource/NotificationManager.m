//
//  NotificationManager.m
//  AviationPress
//
//  Created by Sonia Mane on 24/01/17.
//  Copyright © 2017 Aptara. All rights reserved.
//

#import "NotificationManager.h"
#import "AppDelegate.h"
#import "DataUpdater.h"

@interface NotificationManager ()
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
@end

@implementation NotificationManager
+ (instancetype)sharedInstance {
    static NotificationManager *_sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        _sharedInstance = [[NotificationManager alloc] init];
    });
    return _sharedInstance;
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (BOOL)saveNotificationsInDB:(NSString *)notifMessage withNotifId:(NSString *)notifID {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Notification"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"notificationkey = %@", notifID];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *notificationsArr = [context executeFetchRequest:request error:&error];
    
    if (notificationsArr.count == 0) {
        
        NSManagedObject *notifObject = [NSEntityDescription insertNewObjectForEntityForName:@"Notification"     inManagedObjectContext:context];
        [notifObject setValue:notifMessage forKey:@"message"];
        [notifObject setValue:[NSNumber numberWithBool:NO] forKey:@"readStatus"];
        [notifObject setValue:[NSDate date] forKey:@"dateTime"];
        [notifObject setValue:notifID forKey:@"notificationkey"];
        NSError *error;
        if (![context save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
            return NO;
        } else {
            return YES;
        }
    } else {
        NSLog(@"ALREADY SAVED");
        return YES;
    }
}

- (void)setNotificationAsRead:(Notification *)notification {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [self updateBadgeCountOnServer];
    NSManagedObjectContext *context = [self managedObjectContext];
    notification.readStatus = YES;
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}

- (void)updateBadgeCountOnServer {
    [DataUpdater sendResetBadgeCount:^(BOOL didFinish) {
        
    }];
}

- (NSArray *)getUnreadNotifications {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Notification"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"readStatus = %@", [NSNumber numberWithBool:NO]];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *unreadNotifications = [context executeFetchRequest:request error:&error];

    return unreadNotifications;
}

- (BOOL) deleteUnreadNotifications {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Notification"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"readStatus = %@", [NSNumber numberWithBool:NO]];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *unreadNotifications = [context executeFetchRequest:request error:&error];
    
    if (unreadNotifications != nil) {
        for (NSManagedObject* object in unreadNotifications) {
            [context deleteObject:object];
        }
        return [context save:&error];
    }
    else {
        return NO;
    }
}

- (BOOL) deleteAllNotifications
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Notification"];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"readStatus = %@", [NSNumber numberWithBool:NO]];
//    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *unreadNotifications = [context executeFetchRequest:request error:&error];
    
    if (unreadNotifications != nil) {
        for (NSManagedObject* object in unreadNotifications) {
            [context deleteObject:object];
        }
        return [context save:&error];
    }
    else {
        return NO;
    }
}

- (NSArray *)getAllNotifications {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Notification"];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"dateTime" ascending:NO];
    request.sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    NSError *error = nil;
    NSArray *allNotifications = [context executeFetchRequest:request error:&error];
    return allNotifications;
}

- (NSUInteger)count {
    return [self countWithPredicate:nil];
}

- (NSUInteger)unreadCount {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"readStatus = %@", [NSNumber numberWithBool:NO]];
    return [self countWithPredicate:predicate];
}

- (NSUInteger)countWithPredicate:(NSPredicate *)predicate {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Notification"];
    if (predicate) {
        request.predicate = predicate;
    }
    return [context countForFetchRequest:request error:nil];
}

@end
