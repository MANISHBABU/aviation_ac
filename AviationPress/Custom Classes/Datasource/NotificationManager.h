//
//  NotificationManager.h
//  AviationPress
//
//  Created by Sonia Mane on 24/01/17.
//  Copyright © 2017 Aptara. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Notification+CoreDataProperties.h"

@interface NotificationManager : NSObject
+ (instancetype)sharedInstance;
- (BOOL)saveNotificationsInDB:(NSString *)notifMessage withNotifId:(NSString *)notifID;
- (void)setNotificationAsRead:(Notification *)notification;
- (NSArray *)getUnreadNotifications;
- (BOOL) deleteUnreadNotifications;
- (BOOL) deleteAllNotifications;
- (NSArray *)getAllNotifications;
- (NSUInteger)count;
- (NSUInteger)unreadCount;
@end
