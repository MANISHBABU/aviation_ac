//
//  PSCAESCryptoDataProviderExample.h
//  AviationPress
//
//  Created by Sonia Mane on 29/08/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PSPDFKit/PSPDFKit.h>
#import "PSCExampleRunnerDelegate.h"
#import <CloudKit/CloudKit.h>
#import "AviationPressLandingViewController.h"

typedef void(^initializedVC)(UIViewController * viewcontroller);
typedef void(^recordSaved)(CKRecord *record);
//typedef void (^localBook)(Books *book);

@interface PSCAESCryptoDataProviderExample : NSObject
@property (nonatomic, strong) NSArray *editions;
@property (nonatomic, strong) CKContainer *privateContainer;
@property (nonatomic, strong) CKDatabase *database;

+ (instancetype)sharedInstance;
//- (void) fetchRecordfromiCloud:(NSString *)pdfName withCompletionBlock:(recordSaved) recordFetched;
//- (void)savePDFtoiCloud:(NSString *)pdfName withCompletionBlock:(recordSaved) recordSaved;
//- (AviationPressLandingViewController *) getInitializedVCForURL:(NSURL *) documentSamplesURL;

//- (void) fetchBookInOfflineMode:(NSString *)pdfName withCompletionBlock:(Books *)
//- (void) fetchAllEditionsWithCompletionBlock:(allEditionsFromCloud) editions;
//- (AviationPressLandingViewController *) getInitializedVCForRecord:(CKRecord *) record;
//- (void)invokeWithDelegate:(id<PSCExampleRunnerDelegate>) delegate forPDF:(NSString *) pdfName withCompletionBlock:(initializedVC) initializedVC;
@end
