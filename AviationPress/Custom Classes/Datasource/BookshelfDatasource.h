//
//  BookshelfDatasource.h
//  AviationPress
//
//  Created by Sonia Mane on 06/12/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CloudKit/CloudKit.h>

@class AviationPressLandingViewController, Book;

typedef void(^bookViewModel)(Book * fetchedBook);
typedef void(^allEditions)(NSArray *editions, NSError *error);
typedef void(^cloudError)(NSError *error);
typedef void(^initializedVC)(UIViewController * viewcontroller);
typedef void(^recordSaved)(CKRecord *record, NSError *error);

@interface BookshelfDatasource : NSObject

+ (instancetype)sharedInstance;
- (void) fetchEditionsFromLocalStorageWithCompletionBlock:(allEditions)editions;
- (void) fetchAllEditionsFromiCloudWithCompletionBlock:(allEditions) editions withError: (cloudError) cloudError;
- (AviationPressLandingViewController *) getInitializedVCForURL:(NSURL *) documentSamplesURL forBookEditionNumber:(NSString *) bookEditionName andBookObj:(Book *)bookObj;
- (void) save_12th_EditionToiCloudAndLocallyIfDoesNotExistWithCompletionBlock:(bookViewModel) fetchedBook withError: (cloudError) cloudError;
- (BOOL) save_12th_EditionBookToCoreData;
- (NSArray *) fetchForCategory:(NSString *)categoryName;
- (void) saveBookToCoreData:(Book *) book;
-(void)saveProperBookToCoreData:(Book *) book;
- (void)deleteAllEntities:(NSString *)nameEntity;
@end
