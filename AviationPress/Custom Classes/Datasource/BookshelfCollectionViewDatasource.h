//
//  BookshelfCollectionViewDatasource.h
//  AviationPress
//
//  Created by Sonia Mane on 23/11/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CloudKit/CloudKit.h>

@class AviationPressLandingViewController;

typedef void(^allEditions)(NSArray *editions);
typedef void(^initializedVC)(UIViewController * viewcontroller);
typedef void(^recordSaved)(CKRecord *record);

@interface BookshelfCollectionViewDatasource : NSObject

+ (instancetype)sharedInstance;
- (void) fetchEditionsFromLocalStorageWithCompletionBlock:(allEditions)editions;
- (void) fetchAllEditionsFromiCloudWithCompletionBlock:(allEditions) editions;
- (AviationPressLandingViewController *) getInitializedVCForURL:(NSURL *) documentSamplesURL;
//- (void)savePDFtoiCloud:(NSString *)pdfName withCompletionBlock:(recordSaved) recordSaved;
- (void) fetchRecordfromiCloud:(NSString *)pdfName withCompletionBlock:(recordSaved) recordFetched;

- (void) getAllEditionsFromCloudWithCompletionBlock:(allEditions) editions;
- (void) getAllLocalEditionsWithCompletionBlock:(allEditions)editions;
- (AviationPressLandingViewController *) getInitializedVCForURL:(NSURL *) documentSamplesURL;

//- (void) fetchRecordfromiCloud:(NSString *)pdfName withCompletionBlock:(recordSaved) recordFetched;
//- (void)savePDFtoiCloud:(NSString *)pdfName withCompletionBlock:(recordSaved) recordSaved;
//- (AviationPressLandingViewController *) getInitializedVCForURL:(NSURL *) documentSamplesURL;
@end
