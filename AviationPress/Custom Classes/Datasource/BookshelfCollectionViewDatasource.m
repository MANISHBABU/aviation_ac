 //
//  BookshelfCollectionViewDatasource.m
//  AviationPress
//
//  Created by Sonia Mane on 23/11/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import "BookshelfCollectionViewDatasource.h"
#import "Books.h"
#import <PSPDFKit/PSPDFKit.h>
#import "PSCFileHelper.h"
#import "AviationPressLandingViewController.h"
#import "APConstants.h"

NSString *kEditionCellID = @"EditionCell";

static NSMutableArray *allEEPPEditions;
@interface BookshelfCollectionViewDatasource () 
@property (nonatomic, strong) NSMutableArray *allEEPPEditions;
@property (nonatomic, strong) CKContainer *privateContainer;
@property (nonatomic, strong) CKDatabase *privateDatabase;
@end

@implementation BookshelfCollectionViewDatasource

+ (instancetype)sharedInstance {
    static BookshelfCollectionViewDatasource *_sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[BookshelfCollectionViewDatasource alloc] init];
    });
    return _sharedInstance;
}

- (instancetype)init {
    if (self = [super init]) {
        self.privateContainer = [CKContainer defaultContainer];
        self.privateDatabase = self.privateContainer.privateCloudDatabase;
    }
    return self;
}

- (void) fetchEditionsFromLocalStorageWithCompletionBlock:(allEditions)editions {

    NSURL *const encryptedPDFURL = [[[NSBundle mainBundle] resourceURL] URLByAppendingPathComponent:@"every_11.pdf.aes"];
    NSURL *documentSamplesURL = PSCCopyFileURLToDocumentFolderAndOverride(encryptedPDFURL, NO);
    
    Books *book = [[Books alloc] init];
    book.coverPageURL = [[NSBundle mainBundle] URLForResource:@"EEPP" withExtension:@"png"];
    book.documentsURL = documentSamplesURL;
    book.bookDesc = @"11th Edition";
    book.bookName = @"Everything Explained for the Professional Pilot";
    
    if (!self.allEEPPEditions) {
        self.allEEPPEditions = [NSMutableArray array];
    }
    [self.allEEPPEditions addObject:book];
    editions(self.allEEPPEditions);
    
//    NSString *passphrase = @"aBfMjDTJVjUf3FApA6gtim0e61LeSGWV9sTxBdsfadsHGFHGbasjdjaDSNKCUHGYGghgsguGDHDiuHSDIUHASchsid";
//    NSString *salt = @"öhapvuenröaoeruhföaeiruaerubYTFYTFVbjncJKcnijsdhcuHcIUTUYGDBHJsaSUHAUIJBvodsijvireiowrewBUYEuuasdas";
//    
//    NSString *(^const passphraseProvider)(void) = ^() {
//        return passphrase;
//    };
//    
//    PSPDFAESCryptoDataProvider *cryptoWrapper = [[PSPDFAESCryptoDataProvider alloc] initWithURL:documentSamplesURL passphraseProvider:passphraseProvider salt:salt rounds:PSPDFDefaultPBKDFNumberOfRounds];
//    
//    PSPDFDocument *document = [PSPDFDocument documentWithDataProvider:cryptoWrapper];
//    document.UID = encryptedPDFURL.lastPathComponent; // manually set an UID for encrypted documents.
//    document.title = @"EVERYTHING EXPLAINED";
}

- (void) fetchAllEditionsFromiCloudWithCompletionBlock:(allEditions) editions {
    if (self.allEEPPEditions.count > 0) {
        editions(self.allEEPPEditions);
    } else {
        [self fetchRecordfromiCloud:@"every_11.pdf.aes" withCompletionBlock:^(CKRecord *record) {
            if (record) {
                Books *book = [self parseRecord:record];
                CKAsset *pdfAsset = [[CKAsset alloc] initWithFileURL:book.documentsURL];
                [record setObject:pdfAsset forKey:CLOUD_RECORD_EDITION_PDF];
                [book.database saveRecord:record completionHandler:^(CKRecord * _Nullable record, NSError * _Nullable error) {
                    if (error != nil) {
                        NSLog(@"%s - %@", __FUNCTION__, error);
                    } else {
                        editions(self.allEEPPEditions);
                    }
                }];
            } else {
                [self savePDFtoiCloud:@"every_11.pdf.aes" withCompletionBlock:^(CKRecord *record) {
                    if (record) {
                        [self parseRecord:record];
                        editions(self.allEEPPEditions);
                    } else {
                        NSLog(@"%s --- Unable to render PDF", __FUNCTION__);
                    }
                }];
            }
        }];
    }
}

- (Books *) parseRecord:(CKRecord *)record {
    Books *book = [[Books alloc] init];
    book.editionRecord = record;
    book.privateContainer = self.privateContainer;
    book.database = self.privateDatabase;
    CKAsset *pdf = [book.editionRecord valueForKey:CLOUD_RECORD_EDITION_PDF];
    book.documentsURL = PSCCopyFileURLToDocumentFolderAndOverride(pdf.fileURL, NO);
    CKAsset *coverPage = [book.editionRecord valueForKey:CLOUD_RECORD_EDITION_COVER_IMAGE];
    book.coverPageURL = coverPage.fileURL;
    book.bookName = [book.editionRecord valueForKey:CLOUD_RECORD_EDITION_NAME];
    book.bookDesc = [book.editionRecord valueForKey:CLOUD_RECORD_EDITION_NUMBER];
    
    if (!self.allEEPPEditions) {
        self.allEEPPEditions = [NSMutableArray array];
    }
    [self.allEEPPEditions addObject:book];
    return book;
}

- (AviationPressLandingViewController *) getInitializedVCForURL:(NSURL *) documentSamplesURL {
    //    CKAsset *pdf = [record valueForKey:@"PDF"];
    //    NSURL *documentSamplesURL = pdf.fileURL;
    NSString *passphrase = @"aBfMjDTJVjUf3FApA6gtim0e61LeSGWV9sTxBdsfadsHGFHGbasjdjaDSNKCUHGYGghgsguGDHDiuHSDIUHASchsid";
    NSString *salt = @"öhapvuenröaoeruhföaeiruaerubYTFYTFVbjncJKcnijsdhcuHcIUTUYGDBHJsaSUHAUIJBvodsijvireiowrewBUYEuuasdas";
    
    NSString *(^const passphraseProvider)(void) = ^() {
        return passphrase;
    };
    
    PSPDFAESCryptoDataProvider *cryptoWrapper = [[PSPDFAESCryptoDataProvider alloc] initWithURL:documentSamplesURL passphraseProvider:passphraseProvider salt:salt rounds:PSPDFDefaultPBKDFNumberOfRounds];
    
    PSPDFDocument *document = [PSPDFDocument documentWithDataProvider:cryptoWrapper];
    document.UID = documentSamplesURL.lastPathComponent; // manually set an UID for encrypted documents.
    document.title = @"EVERYTHING EXPLAINED";
    
    AviationPressLandingViewController *vc = [[AviationPressLandingViewController alloc] initWithDocument:document configuration:[PSPDFConfiguration configurationWithBuilder:^(PSPDFConfigurationBuilder * _Nonnull builder) {
        builder.backgroundColor = [UIColor whiteColor];
        builder.doublePageModeOnFirstPage = NO;
        [builder setThumbnailGrouping:PSPDFThumbnailGroupingNever];
        [builder setMaximumZoomScale:20.0f];
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeLeft) {
            builder.pageMode = PSPDFPageModeDouble;
        } else {
            builder.pageMode = PSPDFPageModeSingle;
        }
    }]];
    return vc;
}

- (void)savePDFtoiCloud:(NSString *)pdfName withCompletionBlock:(recordSaved) recordSaved {
    
    NSURL *const encryptedPDFURL = [[[NSBundle mainBundle] resourceURL] URLByAppendingPathComponent:pdfName];
    NSURL *documentSamplesURL = PSCCopyFileURLToDocumentFolderAndOverride(encryptedPDFURL, NO);
    
//    NSString *timestampAsString = [NSString stringWithFormat:@"%f", [NSDate timeIntervalSinceReferenceDate]];
//    NSArray *timestampParts = [timestampAsString componentsSeparatedByString:@"."];
    
    CKRecordID *recordEditionId = [[CKRecordID alloc] initWithRecordName:pdfName];
    CKRecord *recordEdition = [[CKRecord alloc] initWithRecordType:CLOUD_RECORD_TYPE recordID:recordEditionId];
    //1
    [recordEdition setObject:[NSDate date] forKey:CLOUD_RECORD_EDITION_DATE];
    
    CKAsset *pdfAsset = [[CKAsset alloc] initWithFileURL:documentSamplesURL];
    //2
    [recordEdition setObject:pdfAsset forKey:CLOUD_RECORD_EDITION_PDF];
    
    NSURL *coverPageImageURL = [[NSBundle mainBundle] URLForResource:@"EEPP" withExtension:@"png"];
    CKAsset *pdfCoverPage = [[CKAsset alloc] initWithFileURL:coverPageImageURL];
    //3
    [recordEdition setObject:pdfCoverPage forKey:CLOUD_RECORD_EDITION_COVER_IMAGE];
    //4
    [recordEdition setObject:@"11th Edition" forKey:CLOUD_RECORD_EDITION_NUMBER];
    //5
    [recordEdition setObject:@"Everything Explained for the Professional Pilot" forKey:CLOUD_RECORD_EDITION_NAME];
    
    CKContainer *container = [CKContainer defaultContainer];
    CKDatabase *privateDatabase = container.privateCloudDatabase;
    
    [privateDatabase saveRecord:recordEdition completionHandler:^(CKRecord * _Nullable record, NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"%s - %@", __FUNCTION__, error);
            recordSaved(nil);
        } else {
            recordSaved(record);
        }
    }];
}

- (void) fetchRecordfromiCloud:(NSString *)pdfName withCompletionBlock:(recordSaved) recordFetched {
    
    CKContainer *container = [CKContainer defaultContainer];
    CKDatabase *privateDatabase = container.privateCloudDatabase;
    
    CKRecordID *recordEditionId = [[CKRecordID alloc] initWithRecordName:pdfName];
    [privateDatabase fetchRecordWithID:recordEditionId completionHandler:^(CKRecord * _Nullable record, NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"%s - %@", __FUNCTION__, error);
            recordFetched(nil);
        } else {
            recordFetched(record);
        }
    }];
}








@end



#pragma mark - UICollectionViewDataSource
//        NSPredicate *predicate = [NSPredicate predicateWithValue:YES];
//        CKQuery *query = [[CKQuery alloc] initWithRecordType:@"EEPP_Editions" predicate:predicate];

//- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    return [self.editions count];
//}
//
//- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
//    EditionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kEditionCellID forIndexPath:indexPath];
//    cell.editionObj = self.editions[indexPath.item];
//    return cell;
//    
//}
/*
 PSCAESCryptoDataProviderExample *example = [PSCAESCryptoDataProviderExample sharedInstance];
 [example fetchRecordfromiCloud:@"every_11.pdf.aes" withCompletionBlock:^(CKRecord *record) {
 if (record) {
 [self.privateDatabase performQuery:query inZoneWithID:nil completionHandler:^(NSArray<CKRecord *> * _Nullable results, NSError * _Nullable error) {
 if (error != nil) {
 NSLog(@"%s - %@", __FUNCTION__, error);
 editions(nil);
 } else {
 for (CKRecord *edition in results) {
 Books *book = [[Books alloc] init];
 book.editionRecord = edition;
 book.privateContainer = self.privateContainer;
 book.database = self.privateDatabase;
 CKAsset *pdf = [book.editionRecord valueForKey:@"PDF"];
 book.documentsURL = PSCCopyFileURLToDocumentFolderAndOverride(pdf.fileURL, NO);
 CKAsset *coverPage = [book.editionRecord valueForKey:@"CoverImage"];
 book.coverPageURL = coverPage.fileURL;
 if (!self.allEEPPEditions) {
 self.allEEPPEditions = [NSMutableArray array];
 }
 [self.allEEPPEditions addObject:book];
 }
 editions(self.allEEPPEditions);
 }
 }];
 } else {
 [example savePDFtoiCloud:@"every_11.pdf.aes" withCompletionBlock:^(CKRecord *record) {
 if (record) {
 [self.privateDatabase performQuery:query inZoneWithID:nil completionHandler:^(NSArray<CKRecord *> * _Nullable results, NSError * _Nullable error) {
 if (error != nil) {
 NSLog(@"%s - %@", __FUNCTION__, error);
 editions(nil);
 } else {
 for (CKRecord *edition in results) {
 Books *book = [[Books alloc] init];
 book.editionRecord = edition;
 book.privateContainer = self.privateContainer;
 book.database = self.privateDatabase;
 CKAsset *pdf = [book.editionRecord valueForKey:@"PDF"];
 book.documentsURL = PSCCopyFileURLToDocumentFolderAndOverride(pdf.fileURL, NO);
 CKAsset *coverPage = [book.editionRecord valueForKey:@"CoverImage"];
 book.coverPageURL = coverPage.fileURL;
 if (!self.allEEPPEditions) {
 self.allEEPPEditions = [NSMutableArray array];
 }
 [self.allEEPPEditions addObject:book];
 }
 editions(self.allEEPPEditions);
 }
 }];
 } else {
 NSLog(@"%s --- Unable to render PDF", __FUNCTION__);
 }
 }];
 }
 }];
 */
