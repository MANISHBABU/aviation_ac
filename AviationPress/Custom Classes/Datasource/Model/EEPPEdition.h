//
//  EEPPEdition.h
//  AviationPress
//
//  Created by Sonia Mane on 23/11/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EEPPEdition : NSObject
@property (nonatomic, copy) NSString *edition;
@property (nonatomic, copy) NSString *localPath;
@property (nonatomic, copy) NSString *pdfName;
@property (nonatomic, copy) NSString *pdfCoverThumbnail;

//+ (EEPPEdition *) EEPP_edition_11;
//+ (EEPPEdition *) EEPP_edition_12;
@end
