//
//  Book.h
//  AviationPress
//
//  Created by Sonia Mane on 06/12/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CloudKit/CloudKit.h>
#import <PSPDFKit/PSPDFKit.h>

@protocol BooksDownloadDelegate <NSObject>

- (void)didStartDownloading;
- (void)didUpdateProgress:(float)progress;
- (void)didFinishDownload;
- (void)didFailDownload;

@end

@interface UserInfo : NSObject
@property (nonatomic, strong) CKContainer *container;
@property (nonatomic, strong) CKRecordID *recordId;

@end
@interface Book : NSObject

@property (nonatomic, retain) NSNumber * bookId;
@property (nonatomic, retain) NSString * bookEditionNo;
@property (nonatomic, retain) NSString * coverImageName;
@property (nonatomic, retain) NSString * bookName;
@property (nonatomic, retain) NSString * bookEditionNumber;
@property (nonatomic, retain) NSNumber * bookPrice;
@property (nonatomic, retain) NSString * passphrase;
@property (nonatomic, retain) NSString * salt;
@property (nonatomic, retain) NSString * pListName;

@property (nonatomic) BOOL IsExist; //Above New data 

@property (nonatomic, retain) NSString * bookDesc;
@property (nonatomic, retain) NSString * downloadDate;
@property (nonatomic, retain) NSNumber * numberOfPages;
@property (nonatomic, retain) NSString * thumbPath;
@property (nonatomic, retain) NSString * zipPath;
@property (nonatomic, retain) NSString * folderName;
@property (nonatomic, retain) NSNumber * bookmark;
@property (nonatomic, retain) NSString * favorites;
@property (nonatomic, retain) NSString * globalNote;
@property (nonatomic, retain) NSString * classId;
@property (nonatomic, retain) NSNumber * isSync;
@property (nonatomic, retain) NSNumber * isSelected;
@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSString * category;

@property (nonatomic, strong) id<BooksDownloadDelegate> delegate1;
@property (nonatomic, strong) id<BooksDownloadDelegate> delegate2;

@property (nonatomic, strong) PSPDFDocument *document;
@property (nonatomic, strong) CKRecord *editionRecord;
@property (nonatomic, strong) CKContainer *privateContainer;
@property (nonatomic, strong) CKDatabase *database;
@property (nonatomic, strong) NSURL *documentsURL;
@property (nonatomic, strong) NSURL *coverPageURL;
@property (nonatomic, strong) NSString *bookAESName;
@property (nonatomic, strong) NSString *timestampString;
@property (nonatomic, strong) NSString *savedDate;

- (void)downloadItem;
- (BOOL)isDownloading;
- (BOOL)isDownloaded;
- (float)progress;

- (void)updateProgress:(float)prog;
@end
