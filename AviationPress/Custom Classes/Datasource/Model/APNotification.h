//
//  APNotification.h
//  AviationPress
//
//  Created by Sonia Mane on 24/01/17.
//  Copyright © 2017 Aptara. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APNotification : NSObject
@property (nonatomic, copy) NSString *message;
@property (nonatomic) BOOL readStatus;
@property (nonatomic, strong) NSDate *notifDate;
@end
