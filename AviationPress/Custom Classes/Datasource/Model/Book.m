//
//  Book.m
//  AviationPress
//
//  Created by Sonia Mane on 06/12/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import "Book.h"
#import "AFHTTPSessionManager.h"

@implementation Book {
    BOOL _downloaded;
    BOOL _downloading;
    float _progress;
    NSTimer *_downloadTimer;
}

- (id)init {
    self = [super init];
    if (self) {
        _downloaded = NO;
        _downloading = NO;
        _progress = 0.0;
    }
    
    return self;
}

- (BOOL)isDownloaded {
    return _downloaded;
}

- (BOOL)isDownloading {
    return _downloading;
}

- (void)downloadItem {
    _downloading = YES;
    // Simulate network activity
    if (_delegate1) {
        [_delegate1 didStartDownloading];
        [_delegate2 didStartDownloading];
    }
    //    [NSTimer scheduledTimerWithTimeInterval:2.0
    //                                     target:self
    //                                   selector:@selector(updateProgress)
    //                                   userInfo:nil
    //                                    repeats:NO];
}

- (void)updateProgress:(float)prog {
    _progress = prog;
    if (_delegate1) {
        [_delegate1 didUpdateProgress:_progress];
        [_delegate2 didUpdateProgress:_progress];
    }
    if (_progress < 1.0) {
        ////        [NSTimer scheduledTimerWithTimeInterval:2.0
        //                                         target:self
        //                                       selector:@selector(updateProgress)
        //                                       userInfo:nil
        //                                        repeats:NO];
    } else {
        // Completed
        _progress = 0;
        _downloading = NO;
        _downloaded = YES;
        if (_delegate1) {
            [_delegate1 didFinishDownload];
            [_delegate2 didFinishDownload];
        }
        _downloaded = NO;
    }
}

- (float)progress {
    return _progress;
}

@end
