//
//  PSCIndexViewController.h
//  AviationPress
//
//  Created by Sonia Mane on 25/08/16.
//  Copyright (c) 2016 Aptara. All rights reserved.
//

#import <UIKit/UIKit.h>

@import PSPDFKit;

NS_ASSUME_NONNULL_BEGIN

@protocol IndexGotoPageNumber <NSObject>
- (void)moveToIndexPage:(NSInteger)page;
@end

@interface PSCIndexViewController : UITableViewController
@property (nonatomic, strong) NSArray *indexes;

- (instancetype)initWithDocument:(nullable PSPDFDocument *)document withBookEditionName:(NSString *) bookEditionName;

NS_DESIGNATED_INITIALIZER;

@property (nonatomic, nullable) PSPDFDocument *document;
@property (nonatomic, weak) id<IndexGotoPageNumber> delegate;

@end

NS_ASSUME_NONNULL_END
