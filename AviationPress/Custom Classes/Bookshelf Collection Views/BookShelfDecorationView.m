//
//  BookShelfDecorationView.m
//  CollectionViewLayoutDemo
//
//  Created by Jay Thrash on 9/13/13.
//  Copyright (c) 2013 AirDrop Apps. All rights reserved.
//

#import "BookShelfDecorationView.h"

#define isRetina ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)

#define iPadPro (UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad && UIScreen.mainScreen.nativeBounds.size.height == 2732)

//width ipad pro
@interface BookShelfDecorationView()
{
    UIImageView *shelfTexture;
    
    UIImageView *_shelfImageView;
    UIImageView *_shelfImageViewLandscape;
    UIImageView *_woodImageView;
    UIImageView *_shadingImageView;
    UIImageView *_shadingImageViewLanscape;
    
    UIImageView *_sideImageView_left;
    UIImageView *_sideImageView_right;
    
}
@end

@implementation BookShelfDecorationView

static UIImage *shadingImage = nil;
static UIImage *shadingImagePort = nil;
static UIImage *woodImage = nil;
static UIImage *shelfImageProtrait = nil;
static UIImage *shelfImageLandscape = nil;

+ (NSString *)kind
{
    return @"DecorationView";
}

+ (UIImage *)shadingImage {
    if (shadingImage == nil) {
        shadingImage = [UIImage imageNamed:@"middle_P.png"];
        
    }
    return shadingImage;
}

+ (UIImage *)shadingImagePortrait {
    if (shadingImagePort == nil) {
        shadingImagePort = [UIImage imageNamed:@"middle_L.png"];
        
    }
    return shadingImagePort;
}
+ (UIImage *)woodImage {
    if (woodImage == nil) {
        CGFloat scale = isRetina ? 2.0f : 1.0f;
        
        UIGraphicsBeginImageContext(CGSizeMake(480 * scale, 139 * scale));
        UIImage *woodImageToDraw = [UIImage imageNamed:@"WoodTile.png"];
        [woodImageToDraw drawInRect:CGRectMake(0, 0, woodImageToDraw.size.width * scale, woodImageToDraw.size.width * scale)];
        woodImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        woodImage = [UIImage imageWithCGImage:woodImage.CGImage scale:scale orientation:UIImageOrientationUp];
    }
    return woodImage;
}

+ (UIImage *)shelfImageProtrait {
    if (shelfImageProtrait == nil) {
        shelfImageProtrait = [UIImage imageNamed:@"shelf_P.png"];
        //   shelfImageProtrait = [UIImage imageNamed:@"Shelf.png"];
        
    }
    return shelfImageProtrait;
}

+ (UIImage *)shelfImageLandscape {
    if (shelfImageLandscape == nil) {
        shelfImageLandscape = [UIImage imageNamed:@"shelf_L.png"];
    }
    return shelfImageLandscape;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
//        UIImage *background = [UIImage imageNamed:@"shelf_L"];
//        shelfTexture = [[UIImageView alloc] initWithFrame:CGRectMake(0,
//                                                                     0,
//                                                                     CGRectGetWidth(frame),
//                                                                     CGRectGetHeight(frame))];
//        shelfTexture.image = background;
//        
//        shelfTexture.autoresizingMask = UIViewAutoresizingFlexibleWidth;
//        [self addSubview:shelfTexture];
        
        
//        UIImage* background = [UIImage imageNamed:@"shelf_L"];
//        
//        shelfTexture = [[UIImageView alloc] initWithFrame:CGRectMake(15,
//                                                                     CGRectGetHeight(frame)-40,
//                                                                     995,
//                                                                     68)];
//        
//        shelfTexture.image = background;
//        
//        shelfTexture.autoresizingMask = UIViewAutoresizingFlexibleWidth;
//        
//        [self addSubview:shelfTexture];

        _shelfImageView = [[UIImageView alloc] initWithImage:[BookShelfDecorationView shelfImageProtrait]];
        
        _shelfImageViewLandscape = [[UIImageView alloc] initWithImage:[BookShelfDecorationView shelfImageLandscape]];
        
        _woodImageView = [[UIImageView alloc] initWithImage:[BookShelfDecorationView woodImage]];
        
        _shadingImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"middle_P"]];
        
        _shadingImageViewLanscape = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"middle_L.png"]];
        
        [self addSubview:_woodImageView];
        [self addSubview:_shadingImageView];
        [self addSubview:_shadingImageViewLanscape];
        
        [self addSubview:_shelfImageView];
        [self addSubview:_shelfImageViewLandscape];
        

    }
    return self;
}

-(void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    [super applyLayoutAttributes:layoutAttributes];
    
//    if(layoutAttributes.frame.size.width == 768)
//    {
//        shelfTexture.frame = CGRectMake(layoutAttributes.frame.origin.x+10, shelfTexture.frame.origin.y, layoutAttributes.frame.size.width-20, shelfTexture.frame.size.height);
//    }
//    else {
//        shelfTexture.frame = CGRectMake(layoutAttributes.frame.origin.x+15, shelfTexture.frame.origin.y, layoutAttributes.frame.size.width-30, shelfTexture.frame.size.height);
//    }
    
    
    [_shadingImageView setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [_shadingImageViewLanscape setFrame:CGRectMake(0, 0, self.frame.size.width,self.frame.size.height)];
    /*
    [_shadingImageView setFrame:CGRectMake(0, 0, 768, self.frame.size.height)];
    [_shadingImageViewLanscape setFrame:CGRectMake(0, 0, 1024,self.frame.size.height)];
    
    if (iPadPro) {
        [_shadingImageView setFrame:CGRectMake(0, 0, 768, self.frame.size.height)];
        [_shadingImageViewLanscape setFrame:CGRectMake(0, 0, 1024,self.frame.size.height)];
    }
    */
    if (!UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))
    {
        [_shelfImageView setHidden:NO];
        [_shelfImageViewLandscape setHidden:YES];
        [_shadingImageViewLanscape setHidden:YES];
        [_shadingImageView setHidden:NO];
        
    }
    else {
        [_shelfImageView setHidden:YES];
        [_shelfImageViewLandscape setHidden:NO];
        [_shadingImageViewLanscape setHidden:NO];
        [_shadingImageView setHidden:YES];
        
    }
    
    [_shelfImageView setFrame:CGRectMake(9, 190, 750, 51)];
    [_shelfImageViewLandscape setFrame:CGRectMake(14, 185, 996, 68)];
    
    if (iPadPro) {
        [_shelfImageView setFrame:CGRectMake(12.5, 190, self.frame.size.width - 25, 51)];
        [_shelfImageViewLandscape setFrame:CGRectMake(19, 185, self.frame.size.width - 35, 68)];
    }

}

@end
