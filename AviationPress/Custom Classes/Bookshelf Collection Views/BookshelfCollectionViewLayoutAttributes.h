//
//  BookshelfCollectionViewLayoutAttributes.h
//  AviationPress
//
//  Created by Sonia Mane on 23/11/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookshelfCollectionViewLayoutAttributes : UICollectionViewLayoutAttributes

// whether header view (ConferenceHeader class) should align label left or center (default = left)
@property (nonatomic, assign) NSTextAlignment headerTextAlignment;
// shadow opacity for the shadow on the photo in SpeakerCell (default = 0.5)
@property (nonatomic, assign) CGFloat shadowOpacity;
@end
