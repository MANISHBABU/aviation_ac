//
//  BookshlefGridLayout.h
//  AviationPress
//
//  Created by Sonia Mane on 24/11/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookshlefGridLayout : UICollectionViewFlowLayout
@property (nonatomic, assign) NSInteger cellCount;
@property (nonatomic, assign) CGPoint center;

+ (Class)layoutAttributesClass;
@end
