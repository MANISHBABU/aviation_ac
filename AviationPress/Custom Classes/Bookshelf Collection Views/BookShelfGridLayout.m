//
//  BookShelfGridLayout.m
//  BookShelfSample
//
//  Created by Sonia Mane on 08/05/15.
//  Copyright (c) 2015 Aptara. All rights reserved.
//

#import "BookShelfGridLayout.h"
#import "BookShelfDecorationView.h"
#import "BookShelfLayoutAttributes.h"

#define iPadPro (UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad && UIScreen.mainScreen.nativeBounds.size.height == 2732)

@interface BookShelfGridLayout ()
{
    NSDictionary *_shelfFrames;
}
@property (nonatomic, strong) NSMutableSet *insertedRowSet;
@property (nonatomic, strong) NSMutableSet *deletedRowSet;

@end

@implementation BookShelfGridLayout

-(id)init {
    if (!(self = [super init])) return nil;
    
    // Must instantiate these in init or else they'll always be empty
    self.insertedRowSet = [NSMutableSet set];
    self.deletedRowSet = [NSMutableSet set];    
   
    //Register Decoration View Class
    [self registerClass:[BookShelfDecorationView class]
forDecorationViewOfKind:[BookShelfDecorationView kind]];
    
    return self;
}

+ (Class)layoutAttributesClass {
    return [BookShelfLayoutAttributes class];
}

- (BOOL)isDeletionModeOn {
    if ([[self.collectionView.delegate class] conformsToProtocol:@protocol(BookShelfGridDelegate)]) {
        return [(id)self.collectionView.delegate isDeletionModeActiveForCollectionView:self.collectionView layout:self];
    }
    return NO;
    
}

#pragma mark - Overridden Methods

-(void)prepareLayout {
    [super prepareLayout];
    
    if (self.collectionViewContentSize.width == 320 || self.collectionViewContentSize.width == 375) {
        self.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.minimumInteritemSpacing = 0.0f;
//        self.itemSize = CGSizeMake(185, 210);
    }
    else {
        self.sectionInset = UIEdgeInsetsMake(0, 40, 0, 40);
        self.minimumInteritemSpacing = 0.0f;
        self.itemSize = CGSizeMake(210, 210);
//        self.itemSize = CGSizeMake(310, 310);
    }
    self.minimumLineSpacing = 0.0f;
    self.minimumInteritemSpacing = 0.0f;

    
    self.cellCount = [[self collectionView] numberOfItemsInSection:0];
    [self prepareShelfFrameLayouts];
}

-(CGSize)collectionViewContentSize {
    
    CGRect bounds = [[self collectionView] bounds];
    
    float height = _shelfFrames.count*self.itemSize.height;
    return CGSizeMake(bounds.size.width, height);
}

-(BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return YES;
}

- (void) prepareShelfFrameLayouts {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    int sectionCount = [self.collectionView numberOfSections];
    
    CGFloat y = 0;
    CGFloat availableWidth = self.collectionViewContentSize.width - (self.sectionInset.left + self.sectionInset.right);
    int itemsAcross = floorf((availableWidth + self.minimumInteritemSpacing) / (self.itemSize.width + self.minimumInteritemSpacing));
    
//    NSLog(@"%f", self.itemSize.width);
    for (int section = 0; section < sectionCount; section++) {
        y += self.headerReferenceSize.height;
        y += self.sectionInset.top;
        
        int itemCount = [self.collectionView numberOfItemsInSection:section];
        int rows = ceilf(itemCount/(float)itemsAcross);
        int sectioncnt = 6;
        if (self.collectionViewContentSize.width == 1024) {
            sectioncnt = 5;
        }

        if (iPadPro) {
            sectioncnt = 7;
        }
        
        if(rows <= sectioncnt) {
            int remain = sectioncnt - rows;
            rows = rows + remain;
        }
        
        for (int row = 0; row < rows; row++) {
           
            dictionary[[NSIndexPath indexPathForItem:row inSection:section]] = [NSValue valueWithCGRect:CGRectMake(0, y, self.collectionViewContentSize.width, self.itemSize.height)];
            
            y += self.itemSize.height;
            
            if (row < rows - 1)
                y += self.minimumLineSpacing;
        }
        
        // End of section. Increment Y offset by the sizes of:
        //   footer reference size
        //   section bottom inset height
        y += self.sectionInset.bottom;
        y += self.footerReferenceSize.height;
    }
    _shelfFrames = [NSDictionary dictionaryWithDictionary:dictionary];
}

#pragma mark Runtime Layout Calculations

- (BookShelfLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    BookShelfLayoutAttributes *attributes = (BookShelfLayoutAttributes *)[super layoutAttributesForItemAtIndexPath:indexPath];
    attributes.showAnimation = NO;
    if ([self isDeletionModeOn])
        attributes.deleteButtonHidden = NO;
    else
        attributes.deleteButtonHidden = YES;
    return attributes;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    // call super so flow layout can return default attributes for all cells, headers, and footers
    // NOTE: Flow layout has already taken care of the Cell view layout attributes! :)
    NSArray *array = [super layoutAttributesForElementsInRect:rect];
    
    // create a mutable copy so we can add layout attributes for any shelfs that
    // have frames that intersect the rect the CollectionView is interested in
    NSMutableArray *newArray = [array mutableCopy];
    
    for (BookShelfLayoutAttributes *attribs in newArray) {
        attribs.showAnimation = NO;
        if ([self isDeletionModeOn]) attribs.deleteButtonHidden = NO;
        else attribs.deleteButtonHidden = YES;
    }

    [_shelfFrames enumerateKeysAndObjectsUsingBlock:^(id key, id shelfRect, BOOL *stop) {
        if (CGRectIntersectsRect([shelfRect CGRectValue], rect)) {
            UICollectionViewLayoutAttributes *shelfAttributes =
            [self layoutAttributesForDecorationViewOfKind:[BookShelfDecorationView kind]
                                              atIndexPath:key];
            [newArray addObject:shelfAttributes];
        }
    }];
    
    return [newArray copy];
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForDecorationViewOfKind:(NSString *)decorationViewKind atIndexPath:(NSIndexPath *)indexPath {
    id shelfRect = _shelfFrames[indexPath];
    
    // this should never happen, but just in case...
    if (!shelfRect)
        return nil;
    
    UICollectionViewLayoutAttributes *attributes =
    [UICollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:decorationViewKind
                                                                withIndexPath:indexPath];
    attributes.frame = [shelfRect CGRectValue];
    attributes.zIndex = -1; // shelves go behind other views
    
    return attributes;
}

@end
