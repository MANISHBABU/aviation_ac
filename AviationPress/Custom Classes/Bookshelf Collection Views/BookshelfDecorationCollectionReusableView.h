//
//  BookshelfDecorationCollectionReusableView.h
//  AviationPress
//
//  Created by Sonia Mane on 23/11/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookshelfDecorationCollectionReusableView : UICollectionReusableView
+ (NSString *)kind;
@end
