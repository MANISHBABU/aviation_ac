//
//  BookShelfListLayout.h
//  BookShelfSample
//
//  Created by Sonia Mane on 08/05/15.
//  Copyright (c) 2015 Aptara. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BookShelfListDelegate

@required

- (BOOL)isDeletionModeActiveForCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout;

@end

@interface BookShelfListLayout : UICollectionViewFlowLayout

@property (nonatomic, weak, readwrite) id <BookShelfListDelegate> delegate;

+ (Class)layoutAttributesClass;

@end
