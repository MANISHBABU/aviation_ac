//
//  EditionCollectionViewCell.h
//  AviationPress
//
//  Created by Sonia Mane on 23/11/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EEPPEdition.h"

@interface EditionCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) EEPPEdition *editionObj;
@end
