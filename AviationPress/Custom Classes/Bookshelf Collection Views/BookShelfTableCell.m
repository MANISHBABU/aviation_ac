//
//  BookShelfTableCell.m
//  AviationPress
//
//  Created by Sonia Mane on 24/11/16.
//  Copyright © 2016 Sonia Mane. All rights reserved.
//

#import "BookShelfTableCell.h"
#import "Book.h"
#import "APConstants.h"

@interface BookShelfTableCell () <BooksDownloadDelegate>

@end

@implementation BookShelfTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) configCell {
    self.cloudSync.hidden = YES;
    if (_book.editionRecord) {
        NSLog(@"%s ********>>>>>>>> cell from icloud entry", __FUNCTION__);
        CKAsset *coverImage = [_book.editionRecord valueForKey:CLOUD_RECORD_EDITION_COVER_IMAGE];
        [self.bookImage setImage:[UIImage imageWithContentsOfFile:coverImage.fileURL.path]];
        self.bookTitle.text = [_book.editionRecord valueForKey:CLOUD_RECORD_EDITION_NAME];
        self.bookDetail.text = [_book.editionRecord valueForKey:CLOUD_RECORD_EDITION_NUMBER];
        self.cloudSync.image = [UIImage imageNamed:@"online"];
        [self.cloudSync setTintColor:[UIColor greenColor]];
    } else {
//        [self.bookImage setImage:[UIImage imageWithContentsOfFile:_book.coverPageURL.path]];
        if ([self.book.bookDesc isEqualToString:EEPP_11_EDITION_NAME]) {
            [self.bookImage setImage:[UIImage imageNamed:EEPP_11_COVER_IMAGE_NAME]];
        } else if ([self.book.bookDesc isEqualToString:EEPP_12_SAMPLE_EDITION_NAME]) {
            [self.bookImage setImage:[UIImage imageNamed:EEPP_12_SAMPLE_COVER_IMAGE_NAME]];
        } else if ([self.book.bookDesc isEqualToString:EEPP_12_EDITION_NAME]) {
            [self.bookImage setImage:[UIImage imageNamed:EEPP_12_COVER_IMAGE_NAME]];
        }
        self.bookTitle.text = _book.bookName;
        self.bookDetail.text = _book.bookDesc;
        self.cloudSync.image = [UIImage imageNamed:@"offline"];
        [self.cloudSync setTintColor:[UIColor redColor]];
    }
    [self setupUI];
}

/////////////////////////////////////
- (void)didStartDownloading {
    [self setupUI];
}

- (void)didUpdateProgress:(float)progress {
    [self setupUI];
}

- (void)didFinishDownload {
    [self setupUI];
}

- (void)didFailDownload {
    //
}

- (void)setupUI {
    _progressView.hidden = ![_book isDownloading];
    _progressView.progress = _book.progress;
    
    if ([_book isDownloading]) {
        //        _itemLabel.text = [NSString stringWithFormat:@"Downloading %@", _book.name];
        //        [_activityIndicator startAnimating];
        //        _activityIndicator.hidden = NO;
    } else if ([_book isDownloaded]) {
        //        _itemLabel.textColor = [UIColor redColor];
        //        _itemLabel.text = [NSString stringWithFormat:@"Downloaded %@", _item.name];
        //        [_activityIndicator stopAnimating];
        //        _activityIndicator.hidden = YES;
    } else {
        // Not downloaded, not downloading (initial state)
        //        _itemLabel.textColor = [UIColor blackColor];
        //        _itemLabel.text = _item.name;
        //        _activityIndicator.hidden = YES;
    }
}


- (void)setBook:(Book *)book {
    _book = book;
    _book.delegate2 = self;
    [self setupUI];
}

- (void)prepareForReuse {
    _book.delegate2 = nil;
}

@end
