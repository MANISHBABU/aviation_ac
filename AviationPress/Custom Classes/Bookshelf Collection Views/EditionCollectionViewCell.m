//
//  EditionCollectionViewCell.m
//  AviationPress
//
//  Created by Sonia Mane on 23/11/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import "EditionCollectionViewCell.h"
#import "AppDelegate.h"
#import "BookshelfCollectionViewLayoutAttributes.h"

@interface EditionCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *bookThumbnailImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@end

@implementation EditionCollectionViewCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        /*self.selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
         self.selectedBackgroundView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
         self.selectedBackgroundView.backgroundColor = [UIColor orangeColor];*/
    }
    return self;
}

- (void) setEditionObj:(EEPPEdition *) editionObj {
    if (![_editionObj isEqual:editionObj]) {
        _editionObj = editionObj;
        self.nameLabel.text = editionObj.edition;
        UIImage *bookThumbnail = [UIImage imageNamed:editionObj.pdfCoverThumbnail];
        self.bookThumbnailImage.image = bookThumbnail;
        [[AppDelegate backgroundQueue] addOperationWithBlock:^{
            UIImage *bookThumbnail = [UIImage imageNamed:editionObj.pdfCoverThumbnail];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                self.bookThumbnailImage.image = bookThumbnail;
            }];
        }];
    }
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
    if (newSuperview) {
        self.bookThumbnailImage.layer.shadowOpacity = 0.5;
        self.bookThumbnailImage.layer.shadowOffset = CGSizeMake(0, 3);
        self.bookThumbnailImage.layer.shadowPath = [[UIBezierPath bezierPathWithRect:CGRectInset(self.bookThumbnailImage.bounds,1,1)] CGPath];
    }
}

- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    if ([layoutAttributes isKindOfClass:[BookshelfCollectionViewLayoutAttributes class]]) {
        BookshelfCollectionViewLayoutAttributes *bookshelfAttributes = (BookshelfCollectionViewLayoutAttributes *)layoutAttributes;
        self.bookThumbnailImage.layer.shadowOpacity = bookshelfAttributes.shadowOpacity;
    }
}

@end
