//
//  BookShelfTableCell.h
//  AviationPress
//
//  Created by Sonia Mane on 24/11/16.
//  Copyright © 2016 Sonia Mane. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Book;

@interface BookShelfTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *bookImage;
@property (weak, nonatomic) IBOutlet UILabel *bookTitle;
@property (weak, nonatomic) IBOutlet UILabel *bookDate;
@property (weak, nonatomic) IBOutlet UILabel *bookDetail;
@property (weak, nonatomic) IBOutlet UIImageView *cloudSync;

@property (weak, nonatomic) IBOutlet UIProgressView *progressView;

@property (nonatomic, strong) Book *book;

-(void) configCell;

@end
