
//
//  BookshelfDecorationCollectionReusableView.m
//  AviationPress
//
//  Created by Sonia Mane on 23/11/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import "BookshelfDecorationCollectionReusableView.h"
#define isRetina ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)


@interface BookshelfDecorationCollectionReusableView () {
    UIImageView *shelfTexture;
    
    UIImageView *_shelfImageView;
    UIImageView *_shelfImageViewLandscape;
    UIImageView *_shadingImageView;
    UIImageView *_shadingImageViewLanscape;
    
    UIImageView *_sideImageView_left;
    UIImageView *_sideImageView_right;
    
}

@end
@implementation BookshelfDecorationCollectionReusableView
static UIImage *shadingImage = nil;
static UIImage *shadingImagePort = nil;
static UIImage *shelfImageProtrait = nil;
static UIImage *shelfImageLandscape = nil;
const NSString *kShelfViewKind = @"ShelfView";

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _shelfImageView = [[UIImageView alloc] initWithImage:[BookshelfDecorationCollectionReusableView shelfImageProtrait]];
        
        _shelfImageViewLandscape = [[UIImageView alloc] initWithImage:[BookshelfDecorationCollectionReusableView shelfImageLandscape]];
        
        _shadingImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"middle_P"]];
        
        _shadingImageViewLanscape = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"middle_L.png"]];
        
        [self addSubview:_shadingImageView];
        [self addSubview:_shadingImageViewLanscape];
        
        [self addSubview:_shelfImageView];
        [self addSubview:_shelfImageViewLandscape];
    }
    return self;
}

-(void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    [super applyLayoutAttributes:layoutAttributes];
    [_shadingImageView setFrame:CGRectMake(0, 0, 768, self.frame.size.height)];
    [_shadingImageViewLanscape setFrame:CGRectMake(0, 0, 1024,self.frame.size.height)];
    
    if (!UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        [_shelfImageView setHidden:NO];
        [_shelfImageViewLandscape setHidden:YES];
        [_shadingImageViewLanscape setHidden:YES];
        [_shadingImageView setHidden:NO];
        
    }
    else {
        [_shelfImageView setHidden:YES];
        [_shelfImageViewLandscape setHidden:NO];
        [_shadingImageViewLanscape setHidden:NO];
        [_shadingImageView setHidden:YES];
        
    }
    
    [_shelfImageView setFrame:CGRectMake(9, 190, 749, 51)];
    [_shelfImageViewLandscape setFrame:CGRectMake(14, 185, 995, 68)];
    
}

- (void)layoutSubviews {
    CGRect shadowBounds = CGRectMake(0, -5, self.bounds.size.width, self.bounds.size.height + 5);
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:shadowBounds].CGPath;
}

+ (NSString *)kind {
    return (NSString *)kShelfViewKind;
}

+ (UIImage *)shadingImage {
    if (shadingImage == nil) {
        shadingImage = [UIImage imageNamed:@"middle_P.png"];
        
    }
    return shadingImage;
}

+ (UIImage *)shadingImagePortrait {
    if (shadingImagePort == nil) {
        shadingImagePort = [UIImage imageNamed:@"middle_L.png"];
        
    }
    return shadingImagePort;
}
+ (UIImage *)shelfImageProtrait {
    if (shelfImageProtrait == nil) {
        shelfImageProtrait = [UIImage imageNamed:@"shelf_P.png"];
        //   shelfImageProtrait = [UIImage imageNamed:@"Shelf.png"];
        
    }
    return shelfImageProtrait;
}

+ (UIImage *)shelfImageLandscape {
    if (shelfImageLandscape == nil) {
        shelfImageLandscape = [UIImage imageNamed:@"shelf_L.png"];
    }
    return shelfImageLandscape;
}

@end
