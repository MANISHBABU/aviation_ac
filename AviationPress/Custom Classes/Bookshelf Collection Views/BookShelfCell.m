//
//  BookShelfCell.m
//  BookShelfSample
//
//  Created by Sonia Mane on 08/05/15.
//  Copyright (c) 2015 Aptara. All rights reserved.
//

#import "BookShelfCell.h"
#import "Book.h"
#import "BookShelfLayoutAttributes.h"
#import "APConstants.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AviationPressIAPHelper.h"

@interface BookShelfCell () <BooksDownloadDelegate>

@end

@implementation BookShelfCell

-(void) configCell {
    self.bookTitle.numberOfLines = 2;
    self.cloudSync.hidden = YES;
    CKAsset *coverImage = [_book.editionRecord valueForKey:CLOUD_RECORD_EDITION_COVER_IMAGE];
    [self.bookImage setImage:[UIImage imageWithContentsOfFile:coverImage.fileURL.path]];

    if (_book.editionRecord) {
        NSLog(@"%s ********>>>>>>>> cell from icloud Collection cell entry", __FUNCTION__);
        CKAsset *coverImage = [_book.editionRecord valueForKey:CLOUD_RECORD_EDITION_COVER_IMAGE];
        [self.bookImage setImage:[UIImage imageWithContentsOfFile:coverImage.fileURL.path]];
        self.bookTitle.text = [_book.editionRecord valueForKey:CLOUD_RECORD_EDITION_NAME];
        self.bookDetail.text = [_book.editionRecord valueForKey:CLOUD_RECORD_EDITION_NUMBER];
        self.cloudSync.image = [UIImage imageNamed:@"online"];
        [self.cloudSync setTintColor:[UIColor greenColor]];
    } else {
        if ([self.book.bookDesc isEqualToString:EEPP_11_EDITION_NAME]) {
            [self.bookImage setImage:[UIImage imageNamed:EEPP_11_COVER_IMAGE_NAME]];
        } else if ([self.book.bookDesc isEqualToString:EEPP_12_SAMPLE_EDITION_NAME]) {
            [self.bookImage setImage:[UIImage imageNamed:EEPP_12_SAMPLE_COVER_IMAGE_NAME]];
        } else if ([self.book.bookDesc isEqualToString:EEPP_12_EDITION_NAME]) {
            [self.bookImage setImage:[UIImage imageNamed:EEPP_12_COVER_IMAGE_NAME]];
        }
        
        NSString *imgName = EEPP_12_COVER_IMAGE_NAME;
        if ([self.book.bookDesc isEqualToString:@"11"])
        {
            imgName = EEPP_11_COVER_IMAGE_NAME;
            
        }else if ([self.book.bookDesc isEqualToString:@"13"])
        {
            imgName = EEPP_13_COVER_IMAGE_NAME;
        }
        [self.bookImage setImage:[UIImage imageNamed:imgName]];
        [self.bookImage sd_setImageWithURL:[NSURL URLWithString:[AviationPressIAPHelper getCoverPage:self.book]]
                     placeholderImage:[UIImage imageNamed:@"defualt_book"]];
        self.bookTitle.text = _book.bookName;
        self.bookDetail.text = _book.bookDesc;
        self.cloudSync.image = [UIImage imageNamed:@"offline"];
        [self.cloudSync setTintColor:[UIColor redColor]];
    }

    self.deleteImage.hidden = YES;
    self.bookTitle.hidden = YES;
    self.bookDate.hidden = YES;
    self.progressView.hidden = YES;
    self.bookDetail.hidden = YES;
    self.bookDelete.hidden = YES;
    self.horizontalLine.hidden = YES;
    
    self.bookImage.layer.borderWidth = 1.0;
    self.bookImage.layer.borderColor = [UIColor darkGrayColor].CGColor;
    [self.bookImage.layer setShadowOffset:CGSizeMake(-2.0, 2.0)];
    [self.bookImage.layer setShadowRadius:2.0];
    [self.bookImage.layer setShadowOpacity:0.7];
    
    [self setupUI];
}

- (void)setProgess:(float)progress {
    [self.progressView setProgress:progress];
}

-(void)applyLayoutAttributes:(BookShelfLayoutAttributes *)layoutAttributes {
    [super applyLayoutAttributes:layoutAttributes];
    [self layoutIfNeeded];
}

- (void)startQuivering {
    CABasicAnimation *quiverAnim = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    float startAngle = (-2) * M_PI/180.0;
    float stopAngle = -startAngle;
    quiverAnim.fromValue = [NSNumber numberWithFloat:startAngle];
    quiverAnim.toValue = [NSNumber numberWithFloat:3 * stopAngle];
    quiverAnim.autoreverses = YES;
    quiverAnim.duration = 0.2;
    quiverAnim.repeatCount = HUGE_VALF;
    float timeOffset = (float)(arc4random() % 100)/100 - 0.50;
    quiverAnim.timeOffset = timeOffset;
    CALayer *layer = self.layer;
    [layer addAnimation:quiverAnim forKey:@"quivering"];
}

- (void)stopQuivering {
    CALayer *layer = self.layer;
    [layer removeAnimationForKey:@"quivering"];
}

/////////////////////////////////////
- (void)didStartDownloading {
    [self setupUI];
}

- (void)didUpdateProgress:(float)progress {
    [self setupUI];
}

- (void)didFinishDownload {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DOWNLOADBOOK" object:nil];
    [self setupUI];
}

- (void)didFailDownload {
    //
}

- (void)setupUI {
    _progressView.hidden = ![_book isDownloading];
    _progressView.progress = _book.progress;
    
    if ([_book isDownloading]) {
//        _itemLabel.text = [NSString stringWithFormat:@"Downloading %@", _book.name];
//        [_activityIndicator startAnimating];
//        _activityIndicator.hidden = NO;
    } else if ([_book isDownloaded]) {
//        _itemLabel.textColor = [UIColor redColor];
//        _itemLabel.text = [NSString stringWithFormat:@"Downloaded %@", _item.name];
//        [_activityIndicator stopAnimating];
//        _activityIndicator.hidden = YES;
    } else {
        // Not downloaded, not downloading (initial state)
//        _itemLabel.textColor = [UIColor blackColor];
//        _itemLabel.text = _item.name;
//        _activityIndicator.hidden = YES;
    }
}


- (void)setBook:(Book *)book {
    _book = book;
    _book.delegate1 = self;
    [self setupUI];
}

- (void)prepareForReuse {
    _book.delegate1 = nil;
}

@end
