//
//  BookShelfCell.h
//  BookShelfSample
//
//  Created by Sonia Mane on 08/05/15.
//  Copyright (c) 2015 Aptara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CloudKit/CloudKit.h>

@class Book;

@interface BookShelfCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *bookImage;
@property (weak, nonatomic) IBOutlet UILabel *bookTitle;
@property (weak, nonatomic) IBOutlet UILabel *bookDate;
@property (weak, nonatomic) IBOutlet UILabel *bookDetail;
@property (weak, nonatomic) IBOutlet UIButton *bookDelete;
@property (weak, nonatomic) IBOutlet UIView *horizontalLine;
@property (weak, nonatomic) IBOutlet UIImageView *deleteImage;

@property (weak, nonatomic) IBOutlet UIImageView *cloudSync;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imageWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imageHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imageBottom;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imageX;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *deleteWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *deleteX;

@property (nonatomic, strong) Book *book;

-(void)configCell;

- (void)setProgess:(float)progress;

@end






