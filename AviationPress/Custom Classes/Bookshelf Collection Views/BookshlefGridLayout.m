//
//  BookshlefGridLayout.m
//  AviationPress
//
//  Created by Sonia Mane on 24/11/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import "BookshlefGridLayout.h"
#import "BookshelfDecorationCollectionReusableView.h"
#import "BookshelfCollectionViewLayoutAttributes.h"

@interface BookshlefGridLayout ()
@property (nonatomic, strong) NSDictionary *shelfRects;
@end

@implementation BookshlefGridLayout

- (id)init {
    self = [super init];
    if (self)
    {
        self.scrollDirection = UICollectionViewScrollDirectionVertical;
        self.itemSize = (CGSize){170, 197};
        self.sectionInset = UIEdgeInsetsMake(4, 10, 14, 10);//UIEdgeInsetsMake(54, 60, 64, 60);
        self.headerReferenceSize = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad? (CGSize){50, 50} : (CGSize){43, 43}; // 100
        self.footerReferenceSize = (CGSize){44, 44}; // 88
        self.minimumInteritemSpacing = 10; // 40;
        self.minimumLineSpacing = 10;//40;
        [self registerClass:[BookshelfDecorationCollectionReusableView class] forDecorationViewOfKind:[BookshelfDecorationCollectionReusableView kind]];
    }
    return self;
}

+ (Class)layoutAttributesClass {
    return [BookshelfCollectionViewLayoutAttributes class];
}

// Do all the calculations for determining where shelves go here
- (void)prepareLayout {
    // call super so flow layout can do all the math for cells, headers, and footers
    [super prepareLayout];
    
    if (self.collectionViewContentSize.width == 320 || self.collectionViewContentSize.width == 375) {
        self.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.minimumInteritemSpacing = 0.0f;
        self.itemSize = CGSizeMake(185, 210);
    }
    else {
        self.sectionInset = UIEdgeInsetsMake(0, 40, 0, 40);
        self.minimumInteritemSpacing = 0.0f;
        self.itemSize = CGSizeMake(210, 210);
    }
    self.minimumLineSpacing = 0.0f;
    self.minimumInteritemSpacing = 0.0f;
    self.cellCount = [[self collectionView] numberOfItemsInSection:0];
    [self prepareShelfFrameLayouts];
}

- (void) prepareShelfFrameLayouts {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    int sectionCount = [self.collectionView numberOfSections];
    
    CGFloat y = 0;
    CGFloat availableWidth = self.collectionViewContentSize.width - (self.sectionInset.left + self.sectionInset.right);
    int itemsAcross = floorf((availableWidth + self.minimumInteritemSpacing) / (self.itemSize.width + self.minimumInteritemSpacing));
    
    NSLog(@"%f", self.itemSize.width);
    
    // Iterate through the sections to see how many shelves are needed for each
    for (int section = 0; section < sectionCount; section++)
    {
        // for each shelf, compute its Y offset which is determined by:
        //  header height
        //  section top inset height
        //  cell item height (minus small offset to position "behind" the cells)
        //
        y += self.headerReferenceSize.height;
        y += self.sectionInset.top;
        
        int itemCount = [self.collectionView numberOfItemsInSection:section];
        int rows = ceilf(itemCount/(float)itemsAcross);
        int sectioncnt = 6;
        if (self.collectionViewContentSize.width == 1024) {
            sectioncnt = 5;
        }
        
        if(rows <= sectioncnt) {
            int remain = sectioncnt - rows;
            rows = rows + remain;
        }
        
        for (int row = 0; row < rows; row++)
        {
            
            dictionary[[NSIndexPath indexPathForItem:row inSection:section]] = [NSValue valueWithCGRect:CGRectMake(0, y, self.collectionViewContentSize.width, self.itemSize.height)];
            
            y += self.itemSize.height;
            
            if (row < rows - 1)
                y += self.minimumLineSpacing;
        }
        
        // End of section. Increment Y offset by the sizes of:
        //   footer reference size
        //   section bottom inset height
        y += self.sectionInset.bottom;
        y += self.footerReferenceSize.height;
    }
    
    _shelfRects = [NSDictionary dictionaryWithDictionary:dictionary];
}

- (CGSize)collectionViewContentSize {
    
    CGRect bounds = [[self collectionView] bounds];
    
    float height = _shelfRects.count*self.itemSize.height;
    return CGSizeMake(bounds.size.width, height);
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return YES;
}

// Return attributes of all items (cells, supplementary views, decoration views) that appear within this rect
- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    // call super so flow layout can return default attributes for all cells, headers, and footers
    NSArray *array = [super layoutAttributesForElementsInRect:rect];
    
    // tweak the attributes slightly
    for (UICollectionViewLayoutAttributes *attributes in array) {
        attributes.zIndex = 1;
        if (attributes.representedElementCategory == UICollectionElementCategorySupplementaryView && [attributes isKindOfClass:[BookshelfCollectionViewLayoutAttributes class]]) {
            BookshelfCollectionViewLayoutAttributes *bookshelfCollectionViewLayoutAttributes = (BookshelfCollectionViewLayoutAttributes *)attributes;
            bookshelfCollectionViewLayoutAttributes.headerTextAlignment = NSTextAlignmentCenter;
        }
    }
    
    // Add our decoration views (shelves)
    NSMutableArray *newArray = [array mutableCopy];
    
    [self.shelfRects enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if (CGRectIntersectsRect([obj CGRectValue], rect)) {
            UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:[BookshelfDecorationCollectionReusableView kind] withIndexPath:key];
            attributes.frame = [obj CGRectValue];
            attributes.zIndex = 0;
            //attributes.alpha = 0.5; // screenshots
            [newArray addObject:attributes];
        }
    }];
    
    array = [NSArray arrayWithArray:newArray];
    
    return array;
}

// Layout attributes for a specific cell
- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewLayoutAttributes *attributes = [super layoutAttributesForItemAtIndexPath:indexPath];
    attributes.zIndex = 1;
    return attributes;
}

// layout attributes for a specific header or footer
- (UICollectionViewLayoutAttributes *)layoutAttributesForSupplementaryViewOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
   
    UICollectionViewLayoutAttributes *attributes = [super layoutAttributesForSupplementaryViewOfKind:kind atIndexPath:indexPath];
    attributes.zIndex = 1;
    if ([attributes isKindOfClass:[BookshelfCollectionViewLayoutAttributes class]]) {
        BookshelfCollectionViewLayoutAttributes *bookshelfCollectionViewLayoutAttributes = (BookshelfCollectionViewLayoutAttributes *)attributes;
        bookshelfCollectionViewLayoutAttributes.headerTextAlignment = NSTextAlignmentCenter;
    }
    
    return attributes;
}

// layout attributes for a specific decoration view
- (UICollectionViewLayoutAttributes *)layoutAttributesForDecorationViewOfKind:(NSString *)decorationViewKind atIndexPath:(NSIndexPath *)indexPath {
    id shelfRect = self.shelfRects[indexPath];
    if (!shelfRect)
        return nil; // no shelf at this index (this is probably an error)
    
    UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:[BookshelfDecorationCollectionReusableView kind] withIndexPath:indexPath];
    attributes.frame = [shelfRect CGRectValue];
    attributes.zIndex = 0; // shelves go behind other views
    return attributes;
}


@end
