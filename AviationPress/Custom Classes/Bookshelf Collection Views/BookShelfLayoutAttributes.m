//
//  BookShelfLayoutAttributes.m
//  ALL
//
//  Created by Sonia Mane on 25/05/15.
//  Copyright (c) 2015 Aptara. All rights reserved.
//

#import "BookShelfLayoutAttributes.h"

@implementation BookShelfLayoutAttributes

- (id)copyWithZone:(NSZone *)zone
{
    BookShelfLayoutAttributes *attributes = [super copyWithZone:zone];
    attributes.deleteButtonHidden = _deleteButtonHidden;
    attributes.showAnimation = _showAnimation;
    return attributes;
}

- (BOOL) isEqual:(id)object
{
    return NO;
}

@end
