//
//  BookshelfCollectionViewLayoutAttributes.m
//  AviationPress
//
//  Created by Sonia Mane on 23/11/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import "BookshelfCollectionViewLayoutAttributes.h"

@implementation BookshelfCollectionViewLayoutAttributes

- (id)init {
    self = [super init];
    if (self) {
        _headerTextAlignment = NSTextAlignmentCenter;
        _shadowOpacity = 0.5;
    }
    
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    BookshelfCollectionViewLayoutAttributes *newAttributes = [super copyWithZone:zone];
    newAttributes.headerTextAlignment = self.headerTextAlignment;
    newAttributes.shadowOpacity = self.shadowOpacity;
    return newAttributes;
}
@end
