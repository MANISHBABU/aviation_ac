//
//  BookShelfGridLayout.h
//  BookShelfSample
//
//  Created by Sonia Mane on 08/05/15.
//  Copyright (c) 2015 Aptara. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol BookShelfGridDelegate

@required

- (BOOL)isDeletionModeActiveForCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout;

@end

@interface BookShelfGridLayout : UICollectionViewFlowLayout 

@property (nonatomic, weak) id <BookShelfGridDelegate> delegate;

@property (nonatomic, assign) NSInteger cellCount;
@property (nonatomic, assign) CGPoint center;

+ (Class)layoutAttributesClass;

@end
