//
//  BookShelfLayoutAttributes.h
//  ALL
//
//  Created by Sonia Mane on 25/05/15.
//  Copyright (c) 2015 Aptara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookShelfLayoutAttributes : UICollectionViewLayoutAttributes

@property (nonatomic, getter = isDeleteButtonHidden) BOOL deleteButtonHidden;

@property (nonatomic, getter = isShowAnimation) BOOL showAnimation;

@end
