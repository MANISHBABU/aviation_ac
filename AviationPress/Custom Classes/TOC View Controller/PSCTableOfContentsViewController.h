//
//  PSCTableOfContentsViewController.h
//  AviationPress
//
//  Created by Sonia Mane on 25/08/16.
//  Copyright (c) 2016 Aptara. All rights reserved.
//

#import <UIKit/UIKit.h>

@import PSPDFKit;

NS_ASSUME_NONNULL_BEGIN

@protocol TOCGotoPageNumber <NSObject>
- (void)moveToPage:(NSInteger)page;
@end

@interface PSCTableOfContentsViewController : UITableViewController
@property (nonatomic, strong) NSArray *chapters;

- (instancetype)initWithDocument:(nullable PSPDFDocument *)document withBookEditionName:(NSString *)bookEditionName;

NS_DESIGNATED_INITIALIZER;

//@property (nonatomic, nullable) PSPDFDocument *document;
@property (nonatomic, copy) NSString *bookEdiName;
@property (nonatomic, weak) id<TOCGotoPageNumber> delegate;

@end

NS_ASSUME_NONNULL_END
