//
//  PSTOCTableViewCell.m
//  Aviation
//
//  Created by Sonia Mane on 25/08/14.
//  Copyright (c) 2014 Sonia Mane. All rights reserved.
//

#import "PSTOCTableViewCell.h"

@implementation PSTOCTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:reuseIdentifier];
    if (self) {

    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGSize size = self.bounds.size;
    CGRect frame = CGRectMake(10.0f, 3.0f, 300, size.height);
    self.textLabel.frame =  frame;
    self.textLabel.contentMode = UIViewContentModeScaleAspectFit;
    
    CGRect frameDetail = CGRectMake(290.0f, 3.0f, 100, size.height);
    self.detailTextLabel.frame =  frameDetail;

}
- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
