//
//  PSCTableOfContentsViewController.m
//  AviationPress
//
//  Created by Sonia Mane on 25/08/16.
//  Copyright (c) 2016 Aptara. All rights reserved.
//

#import "PSCTableOfContentsViewController.h"
#import "PSTOCTableViewCell.h"
#import "AppContext.h"

@interface PSCTableOfContentsViewController ()
@end

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-designated-initializers"

@implementation PSCTableOfContentsViewController

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Lifecycle

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInitializer];
    }
    return self;
}

- (instancetype) init {
    if (self = [super init]) {
        [self commonInitializer];
    }
    return self;
}

- (instancetype)initWithDocument:(nullable PSPDFDocument *)document withBookEditionName:(NSString *)bookEditionName {
    if ((self = [super initWithStyle:UITableViewStylePlain])) {
//        _document = document;
        _bookEdiName = bookEditionName;
        [self commonInitializer];
    }
    return self;
}

- (void) commonInitializer {
    self.title = NSLocalizedString(@"Chapters", @"");
    self.preferredContentSize = CGSizeMake(400.f, self.chapters.count * 44.f);
    self.tableView.rowHeight = 75;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableViewDataSource

- (NSString *)metadataForRow:(NSUInteger)row {
    return [[self.chapters objectAtIndex:row] objectForKey:@"ChapterName"];
}

- (NSString *)metadataKeyForRow:(NSUInteger)row {
    return [[self.chapters objectAtIndex:row] objectForKey:@"PageNumber"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.chapters.count > 0 ? 1 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.chapters.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"PSChaptersCell";
    PSTOCTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[PSTOCTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    cell.textLabel.font = [UIFont systemFontOfSize:17.0];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:16.0];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.text = [self metadataForRow:indexPath.row];
    cell.detailTextLabel.text = [self metadataKeyForRow:indexPath.row];
   
    return cell;
}

///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *index = [[self.chapters objectAtIndex:indexPath.row] objectForKey:@"PageIndex"];
    [self.delegate moveToPage:[index integerValue]];
}

- (void)tableView:(UITableView*)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath*)indexPath withSender:(id)sender {
    if (action == @selector(copy:)) {
        [UIPasteboard generalPasteboard].string = [self metadataForRow:indexPath.row] ?: @"";
    }
}

- (BOOL)tableView:(UITableView*)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath*)indexPath withSender:(id)sender {
    return action == @selector(copy:);
}

- (BOOL)tableView:(UITableView*)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath*)indexPath {
    return YES;
}

@end

#pragma clang diagnostic pop
