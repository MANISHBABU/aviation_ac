//
//  DataUpdater.m
//  AviationPress
//
//  Created by Santosh Khandare on 10/11/15.
//  Copyright (c) 2015 Aptara. All rights reserved.
//

#import "DataUpdater.h"
#import <AFNetworking/AFURLSessionManager.h>
#import <AFNetworking/AFURLResponseSerialization.h>
#import <AFNetworking/AFHTTPSessionManager.h>
#import "NotificationManager.h"
#import "APConstants.h"

@implementation DataUpdater

+ (void)sendUserToken {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"apnsTokenSentSuccessfully"]) {
        NSLog(@"apnsTokenSentSuccessfully already");
        return;
    }

    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.requestCachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SAVE_TOKEN_PROD, [[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"]]]; //set here your URL
    
    //NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SAVE_TOKEN_DEV, [[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"]]]; //set here your URL

    NSURLRequest *request = [NSURLRequest requestWithURL:url];

    AFHTTPSessionManager *_manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
    _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSURLSessionDataTask *dataTask = [_manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                 options:kNilOptions
                                                                   error:&error];
            if (json) {
                
                BOOL val = [[json objectForKey:@"successresponse"] boolValue];
                if (val) {
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"apnsTokenSentSuccessfully"];
                    NSLog(@"Token is being sent successfully");
                    //you can check server response here if you need
                }
            }
        }
    }];
    [dataTask resume];
}

+ (void)sendResetBadgeCount:(completionBlock) complete {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.requestCachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", RESET_MSG_PROD, [[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"]]]; //set here your URL
    
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", RESET_MSG_DEV, [[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"]]]; //set here your URL

    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPSessionManager *_manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
    _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSURLSessionDataTask *dataTask = [_manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"sendResetBadgeCount Error: %@", error);
            complete(NO);
        } else {
            complete(YES);
        }
    }];
    [dataTask resume];
}

+ (void)getNotificationsFromServer:(completionBlock) complete {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.requestCachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", GET_UNREAD_MSG_PROD, [[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"]]]; //set here your URL

//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", GET_UNREAD_MSG_DEV, [[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"]]]; //set here your URL

    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPSessionManager *_manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
    _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSURLSessionDataTask *dataTask = [_manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            
            NSArray* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                 options:kNilOptions
                                                                   error:&error];
            NSLog(@"JSON arr = %@", json);
            if (json) {
                [[NotificationManager sharedInstance] deleteUnreadNotifications];
                for (NSDictionary *pushDict in json) {
                    complete([[NotificationManager sharedInstance] saveNotificationsInDB:pushDict[@"aps"][@"alert"] withNotifId:pushDict[@"aps"][@"notificationkey"]]);
                }
            }
        }
    }];
    [dataTask resume];
}

@end
