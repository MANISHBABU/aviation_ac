//
//  AppDelegate.h
//  AviationPress
//
//  Created by Sonia Mane on 16/08/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Book.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic)  UIWindow * _Nonnull window;
@property (nonatomic, nonnull) UINavigationController *navigationController;

@property (readonly, strong, nonatomic) NSManagedObjectContext * _Nonnull managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel * _Nonnull managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator * _Nonnull persistentStoreCoordinator;
@property (readonly, strong) NSPersistentContainer * _Nonnull persistentContainer;

- (void)saveContext;
- ( NSURL * _Nonnull )applicationDocumentsDirectory;

+ (NSOperationQueue * _Nonnull)backgroundQueue;
//@property (nonatomic, strong) NSMutableArray *publicationsArray;
- (void) saveBookToCoreData:(Book *)bookObj;

@end

