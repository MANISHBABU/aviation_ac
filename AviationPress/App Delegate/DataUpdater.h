//
//  DataUpdater.h
//  AviationPress
//
//  Created by Santosh Khandare on 10/11/15.
//  Copyright (c) 2015 Aptara. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^completionBlock)(BOOL didFinish);
@interface DataUpdater : NSObject
+ (void)sendUserToken;
+ (void)sendResetBadgeCount:(completionBlock) complete;
+ (void)getNotificationsFromServer:(completionBlock) complete;
@end
