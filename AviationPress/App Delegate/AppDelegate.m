//
//  AppDelegate.m
//  AviationPress
//
//  Created by Sonia Mane on 16/08/16.
//  Copyright © 2016 Aptara. All rights reserved.
//

#import "AppDelegate.h"
#import <PSPDFKit/PSPDFKit.h>
#import "AviationPressLandingViewController.h"
#import "BookshelfDatasource.h"
#import <CloudKit/CloudKit.h>
#import "BookshelfViewController.h"
#import "PSCFileHelper.h"
#import <AFNetworking/AFNetworking.h>
#import "APConstants.h"
#import "DataUpdater.h"
#import "NotificationManager.h"
#import "Edition+CoreDataClass.h"
#import "AviationBook+CoreDataClass.h"
#import "AviationPressIAPHelper.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(nullable NSDictionary *)launchOptions {
//    [PSPDFKit setLicenseKey:@"rJ0YmMJMQNVEDpZokRtyWY6jvArz97WADb9w00IPOH3XegPWXmZ15Rqjgfqa"
//     "fgWJ6xsareTt3l8kFGJGLccTtiqcLLXb3Rm95G7/y2+/YMXWPldU8oys1r9J"
//     "HBSC6d/ovQ46xltSXCQlCoybCFX1lxy2Dc0ls8b6zOfy/dzfgpzRpNch1/+e"
//     "gayYReHXX5Fut7AwsL/PEU77ebm57RUwJBxt3+q5Al8v3hMbRViY6gUihyHB"
//     "o/G7iQ9iGppbxsr0L0IjbWDND2xu8QoXDzWLyU3EAa4ZFZWvLhzPpGF+GLvX"
//     "uJPohNi2XWpRZXneP3M7axx6FqBj8pvbygD0xna4uJuzVMhbN8B8K0StUsVr"
//     "w8DLz7iS0xaSoxGog1hZzQx9Pm1OFok/JcJz5x1F3zla4jkLUOvjJpX0s/k6"
//     "wvXQgIIEHqE8bVhX4ILgYNMtR8Bj8k/z2tauOO401fVzpvp86tMO172uzBYJ"
//     "rI7OkpWzZuD/uuWZNUHOJpOhcyiH8UK48fl44TTNdSazdPLvXuO3yFv+6rzy"
//     "yIUVCAZM0XRpMCLw90ea8u5oRAzPkvrfb4rt"];
    
    [PSPDFKit setLicenseKey:@"iwDkZPJIfUTnkVKgpUT2idv+PxYLv0OajaQLPWuMa2NxzwwEb2RbqW56UKfl"

     "Dr3AqC4YIWlwOw1r6CsDMcW+XYLlopjkJwswJGwYbfM+qCpsO3RR1I4hGAnn"

     "FOMVKOQ1pp+oLqflsC9dboisN7D1WyB81aPtDIlRdiOfWEsTWL8PKYCd1gfi"

     "TgDCkk/AC+hY0FR/p3fs/PcR5OOg2OFHURJ8+yladijgs2YZysL+qLh3WZyL"

     "ZLJAji6nA/K+M7gmVbHm2Aq+tNm7fgx8tZsUJcpOlEzlMP0gNkm+RthZhiMR"

     "rzwYBiTgw0d0zlN4Wy8f4rxoo/iePnMDuwg4/IP09SGDSZnve8NDFqmY34SB"

     "6MMo17Q9+LPzjtFtw/oMWZhaOrYmtyb2wR0xbUWA5/37PE1B1vWAnwNeTDG2"

     "hrpQd+Iwk+Ez3hZBpE5mGkoxqJTPQlgqh4AWXpl5riDAhIS94QcXJqVZwl/C"

     "Isd8YTxy5nnY1tZQajo0yI1fpCcdrB/i2XthaHafYKqVZwTR/9FhIyiCDXvs"

     "9NCg2+/qox6a9+jyd8DDe1SGKgvJmFa4Vuv7o92HA1DvYE1cHK+VX6KsUQ=="];
    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:37.0/255.0 green:79.0/255.0 blue:192.0/255.0 alpha:0.85]];
    [[UINavigationBar appearance] setTranslucent:YES];

    
    UILocalNotification *notification = launchOptions[UIApplicationLaunchOptionsLocalNotificationKey];
    
    if (notification) {
        // launched from notification
        NSLog(@"Launched from notification");
    } else {
        // from the springboard
        NSLog(@"Launched from springboard");
        [DataUpdater getNotificationsFromServer:^(BOOL didFinish) {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_SAVED_TO_DB object:nil];
        }];
    }

    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }

    #ifdef DEBUG
    PSPDFKit.sharedInstance.logLevel = PSPDFLogLevelMaskInfo|PSPDFLogLevelMaskWarning|PSPDFLogLevelMaskError;
    #endif
    
 //   [self previous2_2_released];
   // [self current3_0_released];
    [self setInitialVC];
    return YES;
}

-(void)setInitialVC
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    [self.window makeKeyAndVisible];

    if ([AviationPressIAPHelper checkAutoLogin])
    {
        UINavigationController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"BookshelfNavigationViewController"];
        rootViewController.navigationBar.tintColor = nil;
        rootViewController.navigationBar.backIndicatorImage = nil;
        self.window.rootViewController = rootViewController;
    }else
    {
        UINavigationController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"loginVCNAVID"];
//        rootViewController.navigationBar.tintColor = nil;
//        rootViewController.navigationBar.backIndicatorImage = nil;
        self.window.rootViewController = rootViewController;
    }
    
    
}
- (void) current3_0_released {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:FIRST_LAUNCH_VERSION_3]) {
        [self add12thEditionBookAndCategory];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:FIRST_LAUNCH_VERSION_3];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void) add12thEditionBookAndCategory {
    NSError *error;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    
    //Book 12th EDition Sample
    NSURL *const encryptedPDFURL = [[[NSBundle bundleForClass:[self class]] resourceURL] URLByAppendingPathComponent:EEPP_12_SAMPLE_AES];
    NSURL *documentSamplesURL = PSCCopyFileURLToDocumentFolderAndOverride(encryptedPDFURL, NO);
    
    AviationBook *bookObject = [NSEntityDescription insertNewObjectForEntityForName:@"AviationBook"
                                                             inManagedObjectContext:self.managedObjectContext];
    [bookObject setValue:EEPP_12_SAMPLE_BOOK_NAME forKey:@"bookName"];
    [bookObject setValue:[NSNumber numberWithBool:NO] forKey:@"isBookSynced"];
    [bookObject setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
    [bookObject setValue:documentSamplesURL.lastPathComponent forKey:@"bookDocumentsURL"];
    [bookObject setValue:EEPP_12_SAMPLE_EDITION_NAME forKey:@"bookEditionNumber"];
    
    NSURL *coverPageURL = [[NSBundle bundleForClass:[self class]] URLForResource:EEPP_12_SAMPLE_COVER_IMAGE_NAME withExtension:@"png"];
    [bookObject setValue:coverPageURL.lastPathComponent forKey:@"bookCoverImageName"];
    
    NSLog(@"SONIAS CHECK  *** 1st Launch encryptedPDFURL - %@", documentSamplesURL);
    NSLog(@"SONIAS CHECK  *** 1st Launch coverPageURL - %@", coverPageURL);

    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    
#warning EDITION
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Edition"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name contains[cd] %@", @"All Editions"];
    [fetchRequest setPredicate:predicate];
    NSArray *allEditions = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    if (allEditions.count > 0) {
        // do nothings
        /// update hasBOOK
        for (Edition *edi in allEditions) {
//            [edi addHasBook:[NSSet setWithObjects:bookObject, bookObject_12, nil]];
            [edi addHasBook:[NSSet setWithObjects:bookObject, nil]];
        }
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    } else {
        Edition *edi1 = [NSEntityDescription insertNewObjectForEntityForName:@"Edition" inManagedObjectContext:self.managedObjectContext];
        edi1.name = @"All Editions";
        edi1.selected = YES;
//        [edi1 addHasBook:[NSSet setWithObjects:bookObject, bookObject_12, nil]];
        [edi1 addHasBook:[NSSet setWithObjects:bookObject, nil]];

        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    }
    
    
    NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc] initWithEntityName:@"Edition"];
    NSArray *allEditions1 = [[managedObjectContext executeFetchRequest:fetchRequest1 error:nil] mutableCopy];
    
    for (Edition *ediCD in allEditions1) {
        NSSet<AviationBook *> *books = ediCD.hasBook;
        for (AviationBook *bk in books) {
            NSLog(@"$$$$$$$$$ %@ --- %@", ediCD.name, bk.bookEditionNumber);
        }
    }
    
    NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:@"AviationBook"];
    NSArray *allBooks = [managedObjectContext executeFetchRequest:req error:nil];
    for (AviationBook *bk in allBooks) {
        NSSet<Edition *> *belongsToEdi = bk.belongsTo;
        for (Edition *ed in belongsToEdi) {
            NSLog(@"&&&&&&&& %@ ---- %@", bk.bookName, ed.name);
        }
    }
}

- (void) saveBookToCoreData:(Book *)bookObj
{
        NSError *error;
        NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"AviationBook"];
        NSArray *aviationBooks = [[managedObjectContext executeFetchRequest:fetchRequest error:&error] mutableCopy];
        
        if (aviationBooks.count > 0) {
            // do nothings
            // Has book - now assign it to only All Editions
            NSError *error1;
            NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc] initWithEntityName:@"AviationBook"];
            NSArray *aviationBooks = [[managedObjectContext executeFetchRequest:fetchRequest1 error:&error1] mutableCopy];

            
            NSError *error;
            NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Edition"];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name contains[cd] %@", @"All Editions"];
            [fetchRequest setPredicate:predicate];
            NSArray *allEditions = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
            
            if (allEditions.count > 0) {
                // do nothings
                /// update hasBOOK
                for (Edition *edi in allEditions) {
                    [edi addHasBook:[NSSet setWithObjects:aviationBooks.lastObject, nil]];
                }
                if (![self.managedObjectContext save:&error]) {
                    NSLog(@"Failed to save - error: %@", [error localizedDescription]);
                }
            } else {
                Edition *edi1 = [NSEntityDescription insertNewObjectForEntityForName:@"Edition" inManagedObjectContext:self.managedObjectContext];
                edi1.name = @"All Editions";
                edi1.selected = YES;
                [edi1 addHasBook:[NSSet setWithObjects:aviationBooks.lastObject, nil]];
                
                if (![self.managedObjectContext save:&error]) {
                    NSLog(@"Failed to save - error: %@", [error localizedDescription]);
                }
            }
        } else
        {
            // else now add for core data crash fix
            [self addInitialBookAndCategory];
            NSError *error;
            NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
            
            //Book
            NSURL *const encryptedPDFURL = [[[NSBundle bundleForClass:[self class]] resourceURL] URLByAppendingPathComponent:EEPP_11_AES];
            NSURL *documentSamplesURL = PSCCopyFileURLToDocumentFolderAndOverride(encryptedPDFURL, NO);
            
            AviationBook *bookObject = [NSEntityDescription insertNewObjectForEntityForName:@"AviationBook"
                                                                     inManagedObjectContext:self.managedObjectContext];
            [bookObject setValue:bookObj.bookName forKey:@"bookName"];
            [bookObject setValue:[NSNumber numberWithBool:NO] forKey:@"isBookSynced"];
            [bookObject setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
            [bookObject setValue:documentSamplesURL.lastPathComponent forKey:@"bookDocumentsURL"];
            [bookObject setValue:bookObj.bookEditionNumber forKey:@"bookEditionNumber"];
            
            NSURL *coverPageURL = [[NSBundle bundleForClass:[self class]] URLForResource:EEPP_11_COVER_IMAGE_NAME withExtension:@"png"];
            [bookObject setValue:coverPageURL.lastPathComponent forKey:@"bookCoverImageName"];
            
            NSLog(@"SONIAS CHECK  *** 1st Launch encryptedPDFURL - %@", documentSamplesURL);
            NSLog(@"SONIAS CHECK  *** 1st Launch coverPageURL - %@", coverPageURL);
            
            
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"Failed to save - error: %@", [error localizedDescription]);
            }
            
#warning EDITION
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Edition"];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name contains[cd] %@", @"All Editions"];
            [fetchRequest setPredicate:predicate];
            NSArray *allEditions = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
            
            if (allEditions.count > 0) {
                // do nothings
                /// update hasBOOK
                for (Edition *edi in allEditions) {
                    [edi addHasBook:[NSSet setWithObjects:bookObject, nil]];
                }
                if (![self.managedObjectContext save:&error]) {
                    NSLog(@"Failed to save - error: %@", [error localizedDescription]);
                }
            } else {
                Edition *edi1 = [NSEntityDescription insertNewObjectForEntityForName:@"Edition" inManagedObjectContext:self.managedObjectContext];
                edi1.name = @"All Editions";
                edi1.selected = YES;
                [edi1 addHasBook:[NSSet setWithObjects:bookObject, nil]];
                
                if (![self.managedObjectContext save:&error]) {
                    NSLog(@"Failed to save - error: %@", [error localizedDescription]);
                }
            }
            
            
            NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc] initWithEntityName:@"Edition"];
            NSArray *allEditions1 = [[managedObjectContext executeFetchRequest:fetchRequest1 error:nil] mutableCopy];
            
            for (Edition *ediCD in allEditions1) {
                NSSet<AviationBook *> *books = ediCD.hasBook;
                for (AviationBook *bk in books) {
                    NSLog(@"$$$$$$$$$ %@ --- %@", ediCD.name, bk.bookEditionNumber);
                }
            }
            
            NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:@"AviationBook"];
            NSArray *allBooks = [managedObjectContext executeFetchRequest:req error:nil];
            for (AviationBook *bk in allBooks) {
                NSSet<Edition *> *belongsToEdi = bk.belongsTo;
                for (Edition *ed in belongsToEdi) {
                    NSLog(@"&&&&&&&& %@ ---- %@", bk.bookName, ed.name);
                }
            }

        }
}


- (void) previous2_2_released {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:FIRST_LAUNCH]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:FIRST_LAUNCH];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self addInitialBookAndCategory];
    } else {
        // check if already added
        //        if (![[NSUserDefaults standardUserDefaults] boolForKey:CORE_DATA_CRASH_FIX]) {
        NSError *error;
        NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"AviationBook"];
        NSArray *aviationBooks = [[managedObjectContext executeFetchRequest:fetchRequest error:&error] mutableCopy];
        
        if (aviationBooks.count > 0) {
            // do nothings
            // Has book - now assign it to only All Editions
            [self assignBookToAllEditions:[aviationBooks lastObject]];
        } else {
            // else now add for core data crash fix
            [self addInitialBookAndCategory];
        }
    }
}

- (void) addInitialBookAndCategory {
    NSError *error;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    
    //Book
    NSURL *const encryptedPDFURL = [[[NSBundle bundleForClass:[self class]] resourceURL] URLByAppendingPathComponent:EEPP_11_AES];
    NSURL *documentSamplesURL = PSCCopyFileURLToDocumentFolderAndOverride(encryptedPDFURL, NO);
    
    AviationBook *bookObject = [NSEntityDescription insertNewObjectForEntityForName:@"AviationBook"
                                                             inManagedObjectContext:self.managedObjectContext];
    [bookObject setValue:EEPP_11_BOOK_NAME forKey:@"bookName"];
    [bookObject setValue:[NSNumber numberWithBool:NO] forKey:@"isBookSynced"];
    [bookObject setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
    [bookObject setValue:documentSamplesURL.lastPathComponent forKey:@"bookDocumentsURL"];
    [bookObject setValue:EEPP_11_EDITION_NAME forKey:@"bookEditionNumber"];

    NSURL *coverPageURL = [[NSBundle bundleForClass:[self class]] URLForResource:EEPP_11_COVER_IMAGE_NAME withExtension:@"png"];
    [bookObject setValue:coverPageURL.lastPathComponent forKey:@"bookCoverImageName"];
    
    NSLog(@"SONIAS CHECK  *** 1st Launch encryptedPDFURL - %@", documentSamplesURL);
    NSLog(@"SONIAS CHECK  *** 1st Launch coverPageURL - %@", coverPageURL);

    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    
#warning EDITION
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Edition"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name contains[cd] %@", @"All Editions"];
    [fetchRequest setPredicate:predicate];
    NSArray *allEditions = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    if (allEditions.count > 0) {
        // do nothings
        /// update hasBOOK
        for (Edition *edi in allEditions) {
            [edi addHasBook:[NSSet setWithObjects:bookObject, nil]];
        }
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    } else {
        Edition *edi1 = [NSEntityDescription insertNewObjectForEntityForName:@"Edition" inManagedObjectContext:self.managedObjectContext];
        edi1.name = @"All Editions";
        edi1.selected = YES;
        [edi1 addHasBook:[NSSet setWithObjects:bookObject, nil]];
        
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    }

        
    NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc] initWithEntityName:@"Edition"];
    NSArray *allEditions1 = [[managedObjectContext executeFetchRequest:fetchRequest1 error:nil] mutableCopy];
    
    for (Edition *ediCD in allEditions1) {
         NSSet<AviationBook *> *books = ediCD.hasBook;
        for (AviationBook *bk in books) {
            NSLog(@"$$$$$$$$$ %@ --- %@", ediCD.name, bk.bookEditionNumber);
        }
    }

    NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:@"AviationBook"];
    NSArray *allBooks = [managedObjectContext executeFetchRequest:req error:nil];
    for (AviationBook *bk in allBooks) {
        NSSet<Edition *> *belongsToEdi = bk.belongsTo;
        for (Edition *ed in belongsToEdi) {
            NSLog(@"&&&&&&&& %@ ---- %@", bk.bookName, ed.name);
        }
    }
}

- (void) assignBookToAllEditions:(AviationBook *) aviationBook {
    NSError *error;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Edition"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name contains[cd] %@", @"All Editions"];
    [fetchRequest setPredicate:predicate];
    NSArray *allEditions = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    if (allEditions.count > 0) {
        // do nothings
        /// update hasBOOK
        for (Edition *edi in allEditions) {
            [edi addHasBook:[NSSet setWithObjects:aviationBook, nil]];
        }
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    } else {
        Edition *edi1 = [NSEntityDescription insertNewObjectForEntityForName:@"Edition" inManagedObjectContext:self.managedObjectContext];
        edi1.name = @"All Editions";
        edi1.selected = YES;
        [edi1 addHasBook:[NSSet setWithObjects:aviationBook, nil]];
        
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    UINavigationController *nav = (UINavigationController*)self.window.rootViewController;
    
    for (UIViewController *ctllr in nav.viewControllers) {
        if ([ctllr isKindOfClass:[AviationPressLandingViewController class]]) {
            AviationPressLandingViewController *vc = (AviationPressLandingViewController *) ctllr;
            [[NSUserDefaults standardUserDefaults] setInteger:vc.pageIndex forKey:@"currentPageNumber"];
            [[NSUserDefaults standardUserDefaults] setInteger:vc.pageIndex forKey:vc.bookEditionName];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"Saved page number = %lu", (unsigned long)vc.pageIndex);
        } else {
            
        }
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize persistentContainer = _persistentContainer;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "AP.AviationPress" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle bundleForClass:[self class]] URLForResource:@"AviationPress" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"AviationPress.sqlite"];
    NSLog(@"--- storeURL ----%@-----", storeURL);
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption : @YES,
                              NSInferMappingModelAutomaticallyOption : @YES};
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        // Report any error we got.
        if(error) {
            //Erase old sqlite
            
            NSURL  *url = [self.applicationDocumentsDirectory URLByAppendingPathComponent:@"AviationPress.sqlite"];
            NSURL *walUrl = [[url URLByDeletingPathExtension] URLByAppendingPathExtension:@"sqlite-wal"];
            NSURL *shmUrl = [[url URLByDeletingPathExtension] URLByAppendingPathExtension:@"sqlite-shm"];
            if ([[NSFileManager defaultManager] fileExistsAtPath:url.path]) {
                BOOL didremove = [[NSFileManager defaultManager] removeItemAtURL:url error:&error];
                NSLog(@"DID remove - %d", didremove);
            }
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:walUrl.path]) {
                BOOL didremove = [[NSFileManager defaultManager] removeItemAtURL:walUrl error:&error];
                NSLog(@"DID remove - %d", didremove);
            }
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:shmUrl.path]) {
                BOOL didremove = [[NSFileManager defaultManager] removeItemAtURL:shmUrl error:&error];
                NSLog(@"DID remove - %d", didremove);
            }
            
            //Make new persistent store for future saves
            if (![self.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
                // do something with the error
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
                dict[NSLocalizedFailureReasonErrorKey] = failureReason;
                dict[NSUnderlyingErrorKey] = error;
                error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
                // Replace this with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                [self sendCrashLog:error];
            }
        }
    }
    
    return _persistentStoreCoordinator;
}

- (NSPersistentContainer *)persistentContainer {
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"AviationPress"];
//            managedObjectModel:[self managedObjectModel]
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    [self sendCrashLog:error];
                }
            }];
        }
    }
    
    return _persistentContainer;
}

- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSInteger version = [[NSProcessInfo processInfo] operatingSystemVersion].majorVersion;
//    if (version >= 10) {
//        _managedObjectContext = self.persistentContainer.viewContext;
//    } else {
        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
        if (!coordinator) {
            return nil;
        }
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
//    }
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//            abort();
        }
    }
}

#pragma mark - Background Queue

+ (NSOperationQueue *)backgroundQueue {
    static dispatch_once_t once;
    static id _backgroundQueue;
    
    dispatch_once(&once, ^{
        _backgroundQueue = [[NSOperationQueue alloc] init];
        [_backgroundQueue setMaxConcurrentOperationCount:4];
    });
    
    return _backgroundQueue;
}

#pragma mark - Remote Notifications

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    
    const char *data = [deviceToken bytes];
    NSMutableString *token = [NSMutableString string];
    
    for (NSUInteger i = 0; i < [deviceToken length]; i++) {
        [token appendFormat:@"%02.2hhX", data[i]];
    }
    NSLog(@"2 - My token is: %@", token);

    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"apnsToken"]; //save token to resend it if request fails
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"apnsTokenSentSuccessfully"]; // set flag for request status
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [DataUpdater sendUserToken]; //send token
    
    /*
     // Device token
     $deviceToken = '9d217540db70217721d7779d8c94cf96fce27d12021a08e62fc6bc2ad6cdeb29';
     
     // Put your private key's passphrase here:
     $passphrase = 'AviationPress';
     */
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error {
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    NSLog(@"Remote Notification Dictionary %@", userInfo);
    [self processNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"Remote Notification Dictionary %@", userInfo);
    [self processNotification:userInfo];
}

- (void)processNotification:(NSDictionary *)userInfo {
    NSString *notifciationString = userInfo[@"aps"][@"alert"];
    NSString *badgeCount = userInfo[@"aps"][@"badge"];
    NSString *notificationkey = userInfo[@"aps"][@"notificationkey"];
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground) {
        NSLog(@"*******UIApplicationStateBackground********");
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = [NSDate date];
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.repeatInterval = 0;
        NSString *body = notifciationString;
        localNotification.alertBody = [NSString stringWithFormat:@"--%@--", body];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.userInfo = @{@"Notification_message":notifciationString};
        localNotification.applicationIconBadgeNumber = [badgeCount integerValue];
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    } else if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
        NSLog(@"*******UIApplicationStateActive********");
    } else if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateInactive) {
        NSLog(@"*******UIApplicationStateInactive********");
    }
    [UIApplication sharedApplication].applicationIconBadgeNumber = [badgeCount integerValue];

    NSDictionary *notifDictionary = @{@"notificationkey" : notificationkey,
                                     @"notifciationString" : notifciationString};
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_HANDLE_FOREGROUND_PUSH object:notifDictionary];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_PUSH_RECEIVED object:notifDictionary];
}

- (void)sendCrashLog:(NSError *) error {
    if (error != nil) {
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
    }
}

@end



//EEPP_Editions
//Created:
//Dec 8 2016 5:05 PM
//Modified:
//Apr 18 2017 6:12 PM
//Security:
//Custom
//Indexes:
//0
//Metadata Indexes:
//0
//Records:
//12
//Field Name
//Field Type
//Index
//EditionCoverImage
//Asset
//None
//EditionDate
//Date/Time
//Sort
//Query
//EditionName
//String
//Sort
//Query
//Search
//EditionNumber
//String
//Sort
//Query
//Search
//EditionPDF
//Asset
//None
//EditionTimeInterval
//String
//Sort
//Query
//Search
//PDF_AES_Name
//String
