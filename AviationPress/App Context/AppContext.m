//
//  AppContext.m
//  AviationPress
//
//  Created by Sonia Mane on 25/08/16.
//  Copyright (c) 2016 Aptara. All rights reserved.
//

#import "AppContext.h"
#import "APConstants.h"
#import "AviationPressIAPHelper.h"

@implementation AppContext

+ (instancetype)sharedInstance {
    static AppContext *_sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[AppContext alloc] init];
    });
    return _sharedInstance;
}

- (id)init {
    self = [super init];
    if (self) {
        NSInteger val = [[NSUserDefaults standardUserDefaults] integerForKey:@"Version"];
        NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:@"EEPP_11" ofType:@"plist"];
//        self.bookdata = [NSDictionary dictionaryWithContentsOfFile:path];
    }
    return self;
}

- (NSDictionary *) getBookdataFor:(NSString *) bookEditionNumber {
    if ([AviationPressIAPHelper getpListPathDictionary:bookEditionNumber] != nil)
    {
            return [AviationPressIAPHelper getpListPathDictionary:bookEditionNumber];
    }
    
    
    if ([bookEditionNumber isEqualToString:EEPP_11_EDITION_NAME]) {
        NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:EEPP_11_PLIST ofType:@"plist"];
        return [NSDictionary dictionaryWithContentsOfFile:path];

    } else if ([bookEditionNumber isEqualToString:EEPP_12_EDITION_NAME]) {
        NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:EEPP_12_PLIST ofType:@"plist"];
        return [NSDictionary dictionaryWithContentsOfFile:path];
    }else if ([bookEditionNumber isEqualToString:@"11"])
    {
        NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:EEPP_11_PLIST ofType:@"plist"];
        return [NSDictionary dictionaryWithContentsOfFile:path];
    }else if ([bookEditionNumber isEqualToString:@"12"])
    {
        NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:EEPP_12_PLIST ofType:@"plist"];
        return [NSDictionary dictionaryWithContentsOfFile:path];
    }else if ([bookEditionNumber isEqualToString:@"13"])
    {
        NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:EEPP_13_PLIST ofType:@"plist"];
        return [NSDictionary dictionaryWithContentsOfFile:path];
    }
    else {
        NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:EEPP_12_SAMPLE_PLIST ofType:@"plist"];
        return [NSDictionary dictionaryWithContentsOfFile:path];
    }
}
@end
