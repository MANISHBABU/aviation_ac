//
//  AppContext.h
//  AviationPress
//
//  Created by Sonia Mane on 25/08/16.
//  Copyright (c) 2016 Aptara. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppContext : NSObject
//@property (nonatomic, strong) NSDictionary* bookdata;
+ (instancetype)sharedInstance;
- (NSDictionary *) getBookdataFor:(NSString *) bookEditionNumber;
@end
