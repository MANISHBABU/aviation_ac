//
//  AviationBook+CoreDataProperties.m
//  
//
//  Created by Sonia Mane on 29/03/17.
//
//  This file was automatically generated and should not be edited.
//

#import "AviationBook+CoreDataProperties.h"

@implementation AviationBook (CoreDataProperties)

+ (NSFetchRequest<AviationBook *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"AviationBook"];
}

@dynamic bookCoverImageName;
@dynamic bookDocumentsURL;
@dynamic bookEditionNumber;
@dynamic bookName;
@dynamic isBookSynced;
@dynamic isSelected;
@dynamic belongsTo;

@end
