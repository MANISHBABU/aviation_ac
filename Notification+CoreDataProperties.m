//
//  Notification+CoreDataProperties.m
//  
//
//  Created by Sonia Mane on 06/02/17.
//
//  This file was automatically generated and should not be edited.
//

#import "Notification+CoreDataProperties.h"

@implementation Notification (CoreDataProperties)

+ (NSFetchRequest<Notification *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Notification"];
}

@dynamic dateTime;
@dynamic message;
@dynamic readStatus;
@dynamic notificationkey;

@end
