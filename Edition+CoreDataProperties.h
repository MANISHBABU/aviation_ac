//
//  Edition+CoreDataProperties.h
//  
//
//  Created by Sonia Mane on 30/03/17.
//
//  This file was automatically generated and should not be edited.
//

#import "Edition+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Edition (CoreDataProperties)

+ (NSFetchRequest<Edition *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *name;
@property (nonatomic) BOOL selected;
@property (nullable, nonatomic, retain) NSSet<AviationBook *> *hasBook;

@end

@interface Edition (CoreDataGeneratedAccessors)

- (void)addHasBookObject:(AviationBook *)value;
- (void)removeHasBookObject:(AviationBook *)value;
- (void)addHasBook:(NSSet<AviationBook *> *)values;
- (void)removeHasBook:(NSSet<AviationBook *> *)values;

@end

NS_ASSUME_NONNULL_END
