//
//  AviationBook+CoreDataClass.h
//  
//
//  Created by Sonia Mane on 29/03/17.
//
//  This file was automatically generated and should not be edited.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Edition;

NS_ASSUME_NONNULL_BEGIN

@interface AviationBook : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "AviationBook+CoreDataProperties.h"
