//
//  Notification+CoreDataClass.h
//  
//
//  Created by Sonia Mane on 06/02/17.
//
//  This file was automatically generated and should not be edited.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Notification : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Notification+CoreDataProperties.h"
