//
//  Edition+CoreDataClass.h
//  
//
//  Created by Sonia Mane on 30/03/17.
//
//  This file was automatically generated and should not be edited.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AviationBook;

NS_ASSUME_NONNULL_BEGIN

@interface Edition : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Edition+CoreDataProperties.h"
