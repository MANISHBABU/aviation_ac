//
//  Edition+CoreDataProperties.m
//  
//
//  Created by Sonia Mane on 30/03/17.
//
//  This file was automatically generated and should not be edited.
//

#import "Edition+CoreDataProperties.h"

@implementation Edition (CoreDataProperties)

+ (NSFetchRequest<Edition *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Edition"];
}

@dynamic name;
@dynamic selected;
@dynamic hasBook;

@end
