//
//  AviationBook+CoreDataProperties.h
//  
//
//  Created by Sonia Mane on 29/03/17.
//
//  This file was automatically generated and should not be edited.
//

#import "AviationBook+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface AviationBook (CoreDataProperties)

+ (NSFetchRequest<AviationBook *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *bookCoverImageName;
@property (nullable, nonatomic, copy) NSString *bookDocumentsURL;
@property (nullable, nonatomic, copy) NSString *bookEditionNumber;
@property (nullable, nonatomic, copy) NSString *bookName;
@property (nonatomic) BOOL isBookSynced;
@property (nonatomic) BOOL isSelected;
@property (nullable, nonatomic, retain) NSSet<Edition *> *belongsTo;

@end

@interface AviationBook (CoreDataGeneratedAccessors)

- (void)addBelongsToObject:(Edition *)value;
- (void)removeBelongsToObject:(Edition *)value;
- (void)addBelongsTo:(NSSet<Edition *> *)values;
- (void)removeBelongsTo:(NSSet<Edition *> *)values;

@end

NS_ASSUME_NONNULL_END
